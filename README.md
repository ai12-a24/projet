# Connect4

## Development

Please follow the [guide for setup your project](https://gitlab.utc.fr/ai12-a24/Projet/-/blob/main/docs/development.md).

For use [git follow the guide](https://gitlab.utc.fr/ai12-a24/Projet/-/blob/main/docs/gitTutorial.md)

## Useful tools

- [Scene Builder](https://gluonhq.com/products/scene-builder)
