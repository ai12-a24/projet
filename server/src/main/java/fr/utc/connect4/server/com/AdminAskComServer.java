package fr.utc.connect4.server.com;

import fr.utc.connect4.server.admin.AdminCore;
import fr.utc.connect4.server.data.DataServerCore;
import fr.utc.connect4.server.interfaces.IAdminAskComServer;

/**
 * Represents a interface for communicate between {@link DataServerCore} and
 * {@link AminCore}. It provides methods to control the server, such
 * as starting and stopping it.
 * 
 * @see IAdminAskComServer
 * @see ComServerCore
 * @see AdminCore
 */
public class AdminAskComServer implements IAdminAskComServer {

    /**
     * Communication core on server-side.
     */
    private final ComServerCore comServerCore;

    /**
     * Constructs an {@code AdminAskComServer} instance with the specified
     * {@code ComServerCore}.
     * 
     * @param comServerCore the {@ComServerCore} to be used by this admin
     *                      communication server
     */
    public AdminAskComServer(ComServerCore comServerCore) {
        this.comServerCore = comServerCore;
    }

    /**
     * Stops the server by delegating the action to the {@code ComServerCore}
     * instance.
     * 
     * @see ComServerCore#stopServer()
     */
    @Override
    public void stopServer() {
        this.comServerCore.stopServer();
    }

    /**
     * Launches the server on the specified port by delegating the action to the
     * {@code ComServerCore} instance.
     * 
     * @param port the port number on which the server should listen
     * @throws Exception
     * @see ComServerCore#launchServer(int)
     */
    @Override
    public void launchServer(int port) throws Exception {
        this.comServerCore.launchServer(port);
    }
}
