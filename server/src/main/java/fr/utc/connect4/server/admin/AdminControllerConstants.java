package fr.utc.connect4.server.admin;

import javafx.scene.input.KeyCode;
import static javafx.scene.input.KeyCode.ESCAPE;

/**
 * The {@code AdminControllerConstants} interface holds constants used by the
 * {@link AdminController}. It defines a key constant for triggering specific
 * actions in the application, such as closing the server.
 */
public interface AdminControllerConstants {

        /**
         * The {@link KeyCode} that represents the "Escape" key, which is used to close
         * the server.
         */
        KeyCode EXIT_GAME = ESCAPE;

}