package fr.utc.connect4.server.admin;

import fr.utc.connect4.server.interfaces.IComServerAskAdmin;

/**
 * The {@code ComServerAskAdmin} class implements the {@link IComServerAskAdmin}
 * interface and provides functionality for interacting with the admin core in a
 * server environment.
 * 
 * @see IComServerAskAdmin
 * @see AdminCore
 */
public class ComServerAskAdmin implements IComServerAskAdmin {

    /**
     * The {@code adminCore} is a reference to an instance of {@link AdminCore}.
     * It is used to perform various ui tasks within the server.
     * This field is private and can only be accessed within this class.
     * 
     * @see AdminCore
     */
    @SuppressWarnings("unused")
    private final AdminCore adminCore;

    /**
     * Constructs a new instance of {@code ComServerAskAdmin}.
     * 
     * <p>
     * This constructor initializes the {@code adminCore} field with the specified
     * {@code AdminCore} object.
     * </p>
     *
     * @param adminCore The {@link AdminCore} object that is used to perform
     *                  ui tasks. This parameter cannot be {@code null}.
     */
    public ComServerAskAdmin(AdminCore adminCore) {
        this.adminCore = adminCore;
    }
}
