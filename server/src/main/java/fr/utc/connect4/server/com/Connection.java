package fr.utc.connect4.server.com;

import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.java_websocket.WebSocket;

import com.fasterxml.jackson.core.JsonProcessingException;

import fr.utc.connect4.core.Json;
import fr.utc.connect4.core.com.IServerMessage;
import fr.utc.connect4.core.data.LightProfile;

/**
 * The {@code Connection} class represents a connection between the server and a
 * client. It maintains the WebSocket connection and the associated profile for
 * the client. This class provides methods for retrieving the profile, socket,
 * and sending messages to the client.
 */
public class Connection {

    /**
     * Logger instance for logging information, warnings, and errors related to the
     * {@code Connection} class.
     */
    private static final Logger logger = LogManager.getLogger(Connection.class);

    /**
     * The profile associated with the connection. This represents the client's user
     * profile.
     */
    private final LightProfile profile;

    /**
     * The WebSocket object representing the connection between the server and the
     * client.
     */
    private final WebSocket socket;

    /**
     * Constructs a new {@code Connection} object with the specified WebSocket and
     * LightProfile.
     * 
     * @param socket  the WebSocket object representing the connection.
     * @param profile the LightProfile object representing the client's profile.
     * @throws NullPointerException if either {@code socket} or {@code profile} is
     *                              {@code null}.
     */
    public Connection(WebSocket socket, LightProfile profile) {
        this.socket = Objects.requireNonNull(socket, "Socket cannot be null");
        this.profile = Objects.requireNonNull(profile, "Profile cannot be null");
    }

    /**
     * Retrieves the profile associated with this connection.
     * 
     * @return the {@code LightProfile} associated with this connection.
     */
    public LightProfile getProfile() {
        return this.profile;
    }

    /**
     * Retrieves the WebSocket object representing this connection.
     * 
     * @return the {@code WebSocket} object for this connection.
     */
    public WebSocket getSocket() {
        return this.socket;
    }

    /**
     * Sends a message to the client associated with this connection.
     * The message is serialized into a JSON string before being sent over the
     * WebSocket.
     * 
     * @param payload the message to send, which must implement
     *                {@code IServerMessage}.
     * @throws JsonProcessingException if an error occurs during the JSON
     *                                 serialization.
     */
    public void send(IServerMessage payload) {
        try {
            String message = Json.dumps(payload);
            logger.info("Send to {} ({}): {}", this.getProfile().getUuid(), this.getSocket().getRemoteSocketAddress(),
                    message);
            this.socket.send(message);
        } catch (JsonProcessingException e) {
            logger.error("Invalid JSON", e);
        }
    }
}
