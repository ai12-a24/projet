package fr.utc.connect4.server.admin;

import fr.utc.connect4.core.ResourceImage;

/**
 * The {@code ServerImages} class holds the static resources used for
 * server-related images. It contains constants for key images such as the error
 * image and the icon image. These images are represented as
 * {@link ResourceImage} objects.
 * 
 * @see ResourceImage
 */
public class ServerImages {

    /**
     * A static constant representing the error image. This image is used to display
     * error-related information within the server.
     * The resource is located at the path {@code /server/image/error.png}.
     */
    public static final ResourceImage ERROR = new ResourceImage("/server/image/error.png");

    /**
     * A static constant representing the icon image. This image is used for general
     * purposes within the server's interface.
     * The resource is located at the path {@code /server/image/icon.png}.
     */
    public static final ResourceImage ICON = new ResourceImage("/server/image/icon.png");

}
