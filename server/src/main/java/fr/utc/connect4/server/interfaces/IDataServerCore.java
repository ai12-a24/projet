package fr.utc.connect4.server.interfaces;

public interface IDataServerCore {

    public abstract void setDataServerAskComServer(IDataServerAskComServer dataServerAskComServer);

    public abstract IComServerAskDataServer getComServerAskDataServer();

    public abstract void setDataServerAskAdmin(IDataServerAskAdmin dataServerAskAdmin);

    public abstract IAdminAskDataServer getAdminAskDataServer();

}
