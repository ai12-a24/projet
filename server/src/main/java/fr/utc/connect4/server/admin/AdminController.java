package fr.utc.connect4.server.admin;

import java.net.BindException;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.utc.connect4.core.KeyListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * Controller for the Admin interface responsible for managing server
 * interactions. Handles user interactions such as starting and stopping the
 * server, and managing port input. This class implements {@link Initializable}
 * to initialize key listener functionality when the controller is loaded and
 * initialized by the JavaFX framework.
 * <p>
 * The {@link AdminController} interacts with {@link AdminCore} to control
 * server operations and utilizes a {@link KeyListener} to listen for key press
 * events (e.g., close server on escape).
 * </p>
 */
public class AdminController implements Initializable {

    /**
     * Logger instance for logging events and messages.
     */
    private static final Logger logger = LogManager.getLogger(AdminController.class);

    /**
     * Root element for the JavaFX UI scene.
     */
    @FXML
    private Parent root;

    /**
     * Text field for user input specifying the server's port number.
     */
    @FXML
    private TextField port;

    /**
     * Button used for triggering actions like starting and stopping the server.
     */
    @FXML
    private Button action;

    /**
     * TextArea to display logs.
     */
    @FXML
    @SuppressWarnings("unused")
    private TextArea logs;

    /**
     * Key listener instance used to detect key events.
     */
    private KeyListener keys;

    /**
     * Core logic of the admin interface, responsible for server management.
     */
    private AdminCore adminCore;

    /**
     * Util class for redirect log from a file to a TextArea.
     */
    private LogToTextArea logToTextArea;

    /**
     * Flag indicating whether the server is currently running or stopped.
     */
    private Boolean running = false;

    /**
     * Sets the {@link AdminCore} instance, which is required for server management
     * actions. Also initializes the key listener to monitor the exit game key press
     * (e.g., ESC key).
     *
     * @param adminCore the {@link AdminCore} instance responsible for handling
     *                  server operations
     * @throws NullPointerException if the {@code adminCore} is null
     */
    public void setAdminCore(AdminCore adminCore) {
        this.adminCore = Objects.requireNonNull(adminCore, "No admin core provided");
        keys.listenPressed(AdminControllerConstants.EXIT_GAME, this.adminCore::close);
    }

    /**
     * Initializes the controller after the root element is loaded.
     * Sets up the key listener to listen for key events.
     *
     * @param location  the location used to resolve relative paths for the root
     *                  object, or null if not available
     * @param resources the resources used to localize the root object, or null if
     *                  not available
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logger.info("Initializing Admin Controller ...");
        this.logToTextArea = new LogToTextArea("logs/server.log", logs);
        this.logToTextArea.start();
        logger.info("All loggers bind to textArea");
        keys = new KeyListener(root);
        logger.info("Press escape to close the server");

        // Add a listener to the TextField's text property
        port.textProperty().addListener((_, _, newValue) -> {
            String filteredText = newValue.replaceAll("[^\\d]", "");
            Integer value;
            if (filteredText.length() > 9) {
                value = 65535;
            } else if ("".equals(filteredText)) {
                value = 0;
            } else {
                value = Integer.valueOf("0" + filteredText);
                if (value > 65535) {
                    value = 65535;
                }
            }
            action.getStyleClass().add("start");
            action.getStyleClass().remove("used");
            action.getStyleClass().remove("stop");
            port.setText(value.toString());
        });
    }

    /**
     * Stops the log redirection from the log file to the TextArea.
     * This method is invoked when the controller is no longer needed.
     */
    public void stop() {
        if (this.logToTextArea != null) {
            this.logToTextArea.stop();
        }
    }

    /**
     * Event handler method for the action button (e.g., Start/Stop button). Starts
     * or stops the server based on the current state. It retrieves the port number
     * from the {@link port} text field and attempts to start the server on that
     * port. If the server is already running, it stops the server instead. Logs an
     * error message if the port number is invalid.
     *
     * @param event the action event triggered by the user clicking the action
     *              button
     */
    @FXML
    @SuppressWarnings("unused")
    private void actionPressed(ActionEvent event) {
        synchronized (this) {
            action.setDisable(true);
            if (running) {
                this.adminCore.getAdminAskComServer().stopServer();
                action.setText("Start");
                action.getStyleClass().add("start");
                action.getStyleClass().remove("stop");
                action.getStyleClass().remove("used");
                this.port.setEditable(true);
                running = false;
            } else {
                String portString = this.port.getText();
                try {
                    int portValue = Integer.parseInt(portString);
                    this.adminCore.getAdminAskComServer().launchServer(portValue);
                    action.setText("Stop");
                    action.getStyleClass().add("stop");
                    action.getStyleClass().remove("start");
                    action.getStyleClass().remove("error");
                    this.port.setEditable(false);
                    running = true;
                } catch (BindException e) {
                    logger.warn("Port already in use");
                    action.setText("Error");
                    if (!action.getStyleClass().contains("error")) {
                        action.getStyleClass().add("error");
                    }
                    action.getStyleClass().remove("start");
                    action.getStyleClass().remove("stop");
                } catch (Exception err) {
                    logger.error("Error during ", err);
                    action.setText("Error");
                    if (!action.getStyleClass().contains("error")) {
                        action.getStyleClass().add("error");
                    }
                    action.getStyleClass().remove("start");
                    action.getStyleClass().remove("stop");
                }
            }
            action.setDisable(false);
        }
    }
}