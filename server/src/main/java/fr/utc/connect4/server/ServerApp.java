package fr.utc.connect4.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.utc.connect4.server.admin.AdminCore;
import fr.utc.connect4.server.com.ComServerCore;
import fr.utc.connect4.server.data.DataServerCore;
import fr.utc.connect4.server.interfaces.IComServerCore;
import fr.utc.connect4.server.interfaces.IDataServerCore;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * The {@code ServerApp} class represents the main application for the
 * server-side of the Connect 4 game. This class is responsible for initializing
 * the necessary server modules and starting the application. It extends the
 * {@link javafx.application.Application} class to run as a JavaFX application.
 */
public class ServerApp extends Application {

    /**
     * Logger instance for logging application events and errors.
     * Uses {@link LogManager#getLogger} to create a logger for this class.
     */
    private static final Logger logger = LogManager.getLogger(ServerApp.class);

    /**
     * Launches the JavaFX application. This method is called to start the
     * application and invoke the {@link #start(Stage)} method.
     */
    public static void run() {
        launch();
    }

    /**
     * Initializes and starts the server application by setting up the necessary
     * components. This method is invoked by the JavaFX runtime after
     * {@link #launch()} is
     * called.
     * 
     * @param primaryStage The primary stage for this application, used to
     *                     initialize the UI.
     */
    @Override
    public void start(Stage primaryStage) {
        try {
            // Initialize the core components of the server application.
            AdminCore adminCore = new AdminCore();
            IComServerCore comCore = new ComServerCore();
            IDataServerCore dataCore = new DataServerCore();

            // Set communication between the modules.
            comCore.setComServerAskDataServer(dataCore.getComServerAskDataServer());
            comCore.setComServerAskAdmin(adminCore.getComServerAskAdmin());
            dataCore.setDataServerAskComServer(comCore.getDataServerAskComServer());
            dataCore.setDataServerAskAdmin(adminCore.getDataServerAskAdmin());
            adminCore.setAdminAskComServer(comCore.getAdminAskComServer());
            adminCore.setAdminAskDataServer(dataCore.getAdminAskDataServer());

            // Start the AdminCore module, passing the primary stage to it.
            adminCore.start(primaryStage);
        } catch (Exception err) {
            // Log any errors encountered during the setup.
            logger.error("An error occurred: ", err);
        }
    }
}
