package fr.utc.connect4.server.com;

import fr.utc.connect4.core.data.Game;
import fr.utc.connect4.server.data.DataServerCore;
import fr.utc.connect4.server.interfaces.IDataServerAskComServer;

/**
 * Represents a interface for communicate between {@link DataServerCore} and
 * {@link ComServerCore}.
 * 
 * @see IDataServerAskComServer
 * @see ComServerCore
 * @see DataServerCore
 */
public class DataServerAskComServer implements IDataServerAskComServer {

    /**
     * The {@code comServerCore} represents the core of the communication server.
     * It is used to handle the low-level communication between the server
     * and clients. This field is private and should only be accessed within this
     * class.
     * 
     * @see ComServerCore
     */
    @SuppressWarnings("unused")
    private final ComServerCore comServerCore;

    /**
     * Constructs a {@code DataServerAskComServer} with the specified
     * {@code ComServerCore}.
     * 
     * @param comServerCore the core of the communication module.
     */
    public DataServerAskComServer(ComServerCore comServerCore) {
        this.comServerCore = comServerCore;
    }

    @Override
    public void gameResult(Game game) {
        this.comServerCore.gameResult(game);
    }

}
