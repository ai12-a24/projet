package fr.utc.connect4.server.interfaces;

import fr.utc.connect4.core.data.Game;

public interface IDataServerAskComServer {

    void gameResult(Game game);

}
