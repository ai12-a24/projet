package fr.utc.connect4.server.com;

import java.time.Duration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;

import fr.utc.connect4.core.ProfileNotFound;
import fr.utc.connect4.core.com.HandleCreateGame;
import fr.utc.connect4.core.com.HandleGameEnd;
import fr.utc.connect4.core.com.HandleLaunchGame;
import fr.utc.connect4.core.com.HandleUpdateGame;
import fr.utc.connect4.core.com.HandleUpdateMove;
import fr.utc.connect4.core.com.IPayload;
import fr.utc.connect4.core.com.RequestCreateGame;
import fr.utc.connect4.core.com.RequestDelPlayer;
import fr.utc.connect4.core.com.RequestFetchProfile;
import fr.utc.connect4.core.com.RequestJoinGame;
import fr.utc.connect4.core.com.RequestListGame;
import fr.utc.connect4.core.com.RequestListPlayer;
import fr.utc.connect4.core.com.RequestNewMove;
import fr.utc.connect4.core.com.ResponseFetchProfile;
import fr.utc.connect4.core.com.ResponseListGame;
import fr.utc.connect4.core.com.ResponseListPlayer;
import fr.utc.connect4.core.com.ResponsePlayerJoinsFullGame;
import fr.utc.connect4.core.com.SendText;
import fr.utc.connect4.core.data.Game;
import fr.utc.connect4.core.data.GameResult;
import fr.utc.connect4.core.data.LightGame;
import fr.utc.connect4.core.data.PublicProfile;
import fr.utc.connect4.server.interfaces.IAdminAskComServer;
import fr.utc.connect4.server.interfaces.IComServerAskAdmin;
import fr.utc.connect4.server.interfaces.IComServerAskDataServer;
import fr.utc.connect4.server.interfaces.IComServerCore;
import fr.utc.connect4.server.interfaces.IDataServerAskComServer;

/**
 * The {@code ComServerCore} class implements the {@link IComServerCore}
 * interface and manages the core functionality of the communication core
 * server. It handles connections, server management, and message routing
 * between different components. This class is responsible for launching and
 * stopping the server, processing incoming payloads, and interacting with
 * various server components.
 */
public class ComServerCore implements IComServerCore {

    /**
     * Logger instance for logging events related to the server's operations.
     */
    private static final Logger logger = LogManager.getLogger(ComServerCore.class);

    /**
     * Static reference to the server instance.
     */
    private Server server;

    /**
     * Interface provided to AdminCore.
     */
    private final IAdminAskComServer adminAskComServer;

    /**
     * Interface provided to DataServerCore.
     */
    private final IDataServerAskComServer dataServerAskComServer;

    /**
     * Interface used for communicate with DataServerCore.
     */
    private IComServerAskDataServer comServerAskDataServer;

    /**
     * Interface used for communicate with AdminCore.
     */
    @SuppressWarnings("unused")
    private IComServerAskAdmin comServerAskAdmin;

    /**
     * Constructs a new {@code ComServerCore} instance.
     * Initializes the {@link IAdminAskComServer} and
     * {@link IDataServerAskComServer} components and establishes communication
     * links between them.
     */
    public ComServerCore() {
        this.dataServerAskComServer = new DataServerAskComServer(this);
        this.adminAskComServer = new AdminAskComServer(this);
    }

    @Override
    public void setComServerAskDataServer(IComServerAskDataServer comServerAskDataServer) {
        this.comServerAskDataServer = comServerAskDataServer;
    }

    @Override
    public IDataServerAskComServer getDataServerAskComServer() {
        return this.dataServerAskComServer;
    }

    @Override
    public void setComServerAskAdmin(IComServerAskAdmin comServerAskAdmin) {
        this.comServerAskAdmin = comServerAskAdmin;
    }

    @Override
    public IAdminAskComServer getAdminAskComServer() {
        return this.adminAskComServer;
    }

    /**
     * Launches the server on the specified port. If the server is already running,
     * it is stopped and restarted.
     *
     * @param port The port number on which the server should listen.
     * @throws Exception
     */
    public void launchServer(int port) throws Exception {
        this.stopServer();
        server = new Server(port, this, this.comServerAskDataServer);
        server.start();
        server.waitException(Duration.ofMillis(500));
    }

    /**
     * Stops the running server, if any, and sets the server reference to
     * {@code null}.
     */
    public void stopServer() {
        if (server != null) {
            server.close();
            server = null;
        }
    }

    public void gameResult(Game game) {
        GameResult result2 = game.isFinished();
        if (result2 == GameResult.LOSE) {
            result2 = GameResult.WIN;
        } else if (result2 == GameResult.WIN) {
            result2 = GameResult.LOSE;
        }
        try {
            server.send(game.getPlayerCreator(), new HandleGameEnd(game.isFinished()));
        } catch (JsonProcessingException | ProfileNotFound e) {
            logger.error("Error occured for player 1", e);
        }
        try {
            server.send(game.getPlayer2(), new HandleGameEnd(result2));
        } catch (JsonProcessingException | ProfileNotFound e) {
            logger.error("Error occured for player 2", e);
        }
    }

    /**
     * Handles an incoming payload message and routes it to the appropriate handler.
     * Based on the type of the message, the server processes different commands
     * such as sending text, listing players, creating games, and listing games.
     *
     * @param abstractMessage The payload message to handle.
     * @param conn            The connection object representing the communication
     *                        channel.
     */
    public void handlePayload(IPayload abstractMessage, Connection conn) {
        switch (abstractMessage) {
            case SendText textMessage -> {
                logger.info("{}", textMessage.content());
            }
            case RequestListPlayer _ -> {
                conn.send(new ResponseListPlayer(comServerAskDataServer.getUserList()));
            }
            case RequestCreateGame createGame -> {

                LightGame game = comServerAskDataServer.createGame(createGame.game());
                server.broadcast(new HandleCreateGame(game));
            }
            case RequestListGame _ -> {
                conn.send(new ResponseListGame(comServerAskDataServer.getGameList()));
            }
            case RequestJoinGame joinGame -> {
                // TODO(COM/fincerti): check if user try to join his game
                // On success, the player is added to the game, if the game is full, an
                // exception is thrown and the client is notified
                try {
                    comServerAskDataServer.playerJoinsGame(joinGame.game(), conn.getProfile());

                    Game game = comServerAskDataServer.fetchGame(joinGame.game());
                    LightGame lightGame = game.getLightGame();

                    server.broadcastExpect(new HandleUpdateGame(lightGame), game.getPlayerCreator(), game.getPlayer2());
                    server.broadcastOnly(new HandleLaunchGame(game), game.getPlayerCreator(), game.getPlayer2());
                } catch (Exception e) {
                    conn.send(new ResponsePlayerJoinsFullGame(joinGame.game()));
                }
            }
            case RequestNewMove newMove -> {
                comServerAskDataServer.processMove(newMove.move(), newMove.idGame());
                Game game = comServerAskDataServer.fetchGame(newMove.idGame());

                server.broadcastOnly(new HandleUpdateMove(game), game.getPlayerCreator(), game.getPlayer2());

            }
            case RequestFetchProfile fetchProfile -> {
                PublicProfile profile = comServerAskDataServer.fetchProfileInfo(fetchProfile.player());
                conn.send(new ResponseFetchProfile(profile));
            }
            case RequestDelPlayer oldProfile -> {
                logger.info("Suppression du profile !");
                comServerAskDataServer.deleteProfile(oldProfile.oldProfile());
                // conn.send(new HandleDelPlayer(oldProfile.oldProfile()));
            }
            default -> logger.error("Unknown message type: " + abstractMessage.getClass().getName());
        }
    }
}
