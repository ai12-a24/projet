package fr.utc.connect4.server.admin;

import fr.utc.connect4.server.interfaces.IDataServerAskAdmin;

/**
 * Represents a interface for communicate between {@link DataServerCore} and
 * {@link AminCore}.
 * 
 * @see IDataServerAskAdmin
 * @see AdminCore
 * @see DataServerCore
 */
public class DataServerAskAdmin implements IDataServerAskAdmin {

    /**
     * The core ui logic of the server.
     */
    public final AdminCore adminCore;

    /**
     * Constructs a new {@link DataServerAskAdmin} with the specified
     * {@link AdminCore} instance.
     * 
     * @param adminCore The core ui instance.
     */
    public DataServerAskAdmin(AdminCore adminCore) {
        this.adminCore = adminCore;
    }
}