package fr.utc.connect4.server.admin;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.utc.connect4.core.ResourceUtils;
import fr.utc.connect4.server.interfaces.IAdminAskComServer;
import fr.utc.connect4.server.interfaces.IAdminAskDataServer;
import fr.utc.connect4.server.interfaces.IAdminCore;
import fr.utc.connect4.server.interfaces.IComServerAskAdmin;
import fr.utc.connect4.server.interfaces.IDataServerAskAdmin;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * The {@code AdminCore} class implements the {@link IAdminCore} interface
 * and serves as the admin interface for the server administration,
 * managing communication with the data and communication core.
 * It is responsible for starting and closing the server interface,
 * as well as handling server-related actions.
 * 
 * <p>
 * This class provides methods for initializing the UI, managing server
 * shutdown procedures, and interacting with the communication and
 * data servers via appropriate interfaces.
 * </p>
 */
public class AdminCore implements IAdminCore {

    /**
     * Logger for logging events and errors in the {@code AdminCore} class.
     */
    private static final Logger logger = LogManager.getLogger(AdminCore.class);

    /**
     * Interface provided to DataServerCoe.
     */
    private final IDataServerAskAdmin dataServerAskAdmin;

    /**
     * Interface provided to ComServerCore.
     */
    private final IComServerAskAdmin comServerAskAdmin;

    /**
     * Interface used for communicate with ComServerCore.
     */
    private IAdminAskComServer adminAskComServer;

    /**
     * Interface used for communicate with DataServerCore.
     */
    private IAdminAskDataServer adminAskDataServer;

    /**
     * The controller used for handling UI-related actions.
     */
    private AdminController controller;

    /**
     * Constructs an {@code AdminCore} instance, initializing the communication
     * and data server interfaces.
     */
    public AdminCore() {
        this.dataServerAskAdmin = new DataServerAskAdmin(this);
        this.comServerAskAdmin = new ComServerAskAdmin(this);
    }

    /**
     * Starts the administration UI by loading the corresponding FXML file and
     * displaying it in the given stage. It also sets up the necessary
     * resources such as stylesheets and fonts.
     *
     * @param primaryStage The primary stage for the UI.
     */
    @Override
    public void start(Stage primaryStage) {
        try {
            URL fxml = ResourceUtils.getResourceURL("/server/view/view.fxml");
            FXMLLoader loader = new FXMLLoader(fxml);
            Parent root = Objects.requireNonNull(loader.load(), "Cannot load FXML");
            controller = loader.getController();
            controller.setAdminCore(this);
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.setTitle("Connect4 - Server");
            scene.getStylesheets().add(ResourceUtils.getResourceURL("/server/style/stylesheet.css").toExternalForm());
            ResourceUtils.loadCoreFont(24);
            primaryStage.getIcons().add(ServerImages.ICON.get());
            primaryStage.centerOnScreen();
            primaryStage.toFront();
            primaryStage.setResizable(true);
            primaryStage.setOnCloseRequest(_ -> {
                close();
            });
            primaryStage.setMinWidth(300);
            primaryStage.setMinHeight(200);
            primaryStage.show();
        } catch (IOException err) {
            logger.error("An error occurred: ", err);
        }
    }

    /**
     * Closes the server, stopping the communication and close data and
     * terminating the application.
     */
    public void close() {
        logger.info("Closing server");
        this.adminAskComServer.stopServer();
        this.adminAskDataServer.close();
        this.controller.stop();
        Platform.exit();
        logger.info("Closed !");
        System.exit(0);
    }

    @Override
    public void setAdminAskDataServer(IAdminAskDataServer adminAskDataServer) {
        this.adminAskDataServer = adminAskDataServer;
    }

    @Override
    public IDataServerAskAdmin getDataServerAskAdmin() {
        return this.dataServerAskAdmin;
    }

    public IAdminAskComServer getAdminAskComServer() {
        return this.adminAskComServer;

    }

    @Override
    public void setAdminAskComServer(IAdminAskComServer adminAskComServer) {
        this.adminAskComServer = adminAskComServer;

    }

    @Override
    public IComServerAskAdmin getComServerAskAdmin() {
        return this.comServerAskAdmin;
    }

}
