package fr.utc.connect4.server.interfaces;

public interface IAdminAskComServer {

    void stopServer();

    void launchServer(int port) throws Exception;

}
