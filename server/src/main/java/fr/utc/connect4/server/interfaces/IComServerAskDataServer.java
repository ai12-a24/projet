package fr.utc.connect4.server.interfaces;

import java.util.List;
import java.util.UUID;

import fr.utc.connect4.core.data.Game;
import fr.utc.connect4.core.data.LightGame;
import fr.utc.connect4.core.data.LightProfile;
import fr.utc.connect4.core.data.Move;
import fr.utc.connect4.core.data.PublicProfile;

public interface IComServerAskDataServer {

    // Retourne une liste d'objets LightProfile
    List<LightProfile> getUserList();

    // Récupère les informations d'un profil donné
    PublicProfile fetchProfileInfo(UUID uuid);

    // Retourne une liste d'objets LightGame
    List<LightGame> getGameList();

    // Récupère un objet Game correspondant à un LightGame
    Game fetchGame(LightGame p);

    // Récupère un objet Game correspondant à un LightGame
    Game fetchGame(UUID p);

    LightGame createGame(LightGame lightGame);

    // Ajoute un profil public
    void addProfile(PublicProfile p);

    // Supprime un profil public
    void removeProfile(PublicProfile p);

    // Traite un mouvement dans le jeu
    void processMove(Move move, UUID uuidGame);

    /**
     * 
     * @param user user to find as player
     * @return UUID of the game
     */
    UUID findGameWhereUserIsPlayer(LightProfile user);

    /**
     * 
     * @param user user to find as spectator
     * @return UUID of the game
     */
    UUID findGameWhereUserIsSpectator(LightProfile user);

    /**
     * 
     * @param oldProfile profile to delete
     */
    void deleteProfile(LightProfile oldProfile);

    /**
     *
     * Add a player to a game
     *
     * @param uuidGame
     * @param player
     * @throws Exception if the player cannot join the game (game is full)
     */
    void playerJoinsGame(UUID uuidGame, LightProfile player) throws Exception;

}
