package fr.utc.connect4.server.data;

import java.util.List;
import java.util.UUID;

import fr.utc.connect4.core.data.Color;
import fr.utc.connect4.core.data.Game;
import fr.utc.connect4.core.data.GridFormat;
import fr.utc.connect4.core.data.LightGame;
import fr.utc.connect4.core.data.LightProfile;
import fr.utc.connect4.core.data.Move;
import fr.utc.connect4.core.data.PublicProfile;
import fr.utc.connect4.server.interfaces.IComServerAskDataServer;

public class ComServerAskDataServer implements IComServerAskDataServer {

    private final DataServerCore dataCore;

    public ComServerAskDataServer(DataServerCore dataCore) {
        this.dataCore = dataCore;
    }

    // Retourne une liste d'objets LightProfile
    @Override
    public List<LightProfile> getUserList() {
        return dataCore.getUserList();
    }

    @Override
    // Récupère les informations d'un profil donné
    public PublicProfile fetchProfileInfo(UUID uuid) {
        return dataCore.fetchProfileInfo(uuid);
    }

    // Récupère un objet Game correspondant à un LightGame
    @Override
    public Game fetchGame(LightGame game) {
        return dataCore.fetchGame(game);
    }

    // Récupère un objet Game correspondant à un LightGame
    @Override
    public Game fetchGame(UUID game) {
        return dataCore.fetchGame(game);
    }

    // Retourne une liste d'objets LightGame
    @Override
    public List<LightGame> getGameList() {
        return dataCore.getGameList();
    }

    // Ajoute un profil public
    @Override
    public void addProfile(PublicProfile p) {
        dataCore.addProfile(p);
    }

    // Supprime un profil public
    @Override
    public void removeProfile(PublicProfile p) {
        // TODO: Use light profile or UUID
        dataCore.removeProfile(p);
    }

    // Traite un mouvement dans le jeu
    @Override
    public void processMove(Move move, UUID uuidGame) {
        dataCore.processMove(move, uuidGame);
    }

    @Override
    public void playerJoinsGame(UUID uuidGame, LightProfile player) throws Exception {
        Game myGame = this.dataCore.fetchGame(uuidGame);
        if (myGame.getPlayer2() == null) {
            myGame.setPlayer2(player);
        } else {
            throw new UnsupportedOperationException("Unimplemented method 'addPlayerToGame'");
        }
    }

    // @Override
    // public Game createGame(GridFormat format, Color colorCreator, LightProfile
    // lightProfile) {
    // if (colorCreator == Color.EMPTY) {
    // colorCreator = Math.random() > 0.5 ? Color.RED : Color.YELLOW;
    // }
    //
    // Game game = new Game(
    // lightProfile,
    // null,
    // UUID.randomUUID(),
    // format,
    // colorCreator,
    // colorCreator == Color.RED ? Color.YELLOW : Color.RED);
    // dataCore.createGame(game, lightProfile);
    //
    // return game;
    // }

    @Override
    public LightGame createGame(LightGame lightGame) {
        return dataCore.createGame(lightGame);
    }

    /**
     * 
     * @param user user to find as player
     * @return UUID of the game
     */
    @Override
    public UUID findGameWhereUserIsPlayer(LightProfile user) {

        Game g = dataCore.findGameWhereUserIsPlayer(user);
        return (g != null) ? g.getUuid() : null;
    }

    /**
     * 
     * @param user user to find as spectator
     * @return UUID of the game
     */
    @Override
    public UUID findGameWhereUserIsSpectator(LightProfile user) {
        Game g = dataCore.findGameWhereUserIsSpectator(user);
        return (g != null) ? g.getUuid() : null;
    }

    /**
     * 
     * @param oldProfile profile to delete
     */
    public void deleteProfile(LightProfile oldProfile) {
        dataCore.removeProfile(oldProfile);
    }
}
