
package fr.utc.connect4.server.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.utc.connect4.core.data.Color;
import fr.utc.connect4.core.data.Game;
import fr.utc.connect4.core.data.GameResult;
import fr.utc.connect4.core.data.LightGame;
import fr.utc.connect4.core.data.LightProfile;
import fr.utc.connect4.core.data.Move;
import fr.utc.connect4.core.data.PublicProfile;
import fr.utc.connect4.server.interfaces.IAdminAskDataServer;
import fr.utc.connect4.server.interfaces.IComServerAskDataServer;
import fr.utc.connect4.server.interfaces.IDataServerAskAdmin;
import fr.utc.connect4.server.interfaces.IDataServerAskComServer;
import fr.utc.connect4.server.interfaces.IDataServerCore;

public class DataServerCore implements IDataServerCore {

    ArrayList<PublicProfile> connectedUsers;
    ArrayList<Game> onGoingGames;

    private final IComServerAskDataServer comServerAskDataServer;
    private final IAdminAskDataServer adminAskDataServer;
    private IDataServerAskComServer dataServerAskComServer;
    private IDataServerAskAdmin dataServerAskAdmin;

    /**
     * Logger instance for logging events related to the DataServerCore operations.
     */
    private static final Logger logger = LogManager.getLogger(DataServerCore.class);

    public DataServerCore() {
        this(new ArrayList<>(), new ArrayList<>());
    }

    public DataServerCore(List<PublicProfile> myConnectedUsers, List<Game> myOnGoingGames) {
        this.connectedUsers = new ArrayList<>(myConnectedUsers);
        this.onGoingGames = new ArrayList<>(myOnGoingGames);
        this.comServerAskDataServer = new ComServerAskDataServer(this);
        this.adminAskDataServer = new AdminAskDataServer(this);
    }

    /**
     * Send Exception if a settings is not correct
     * 
     * @param game    Game to validate
     * @param profile Creator of the game
     * 
     * @throws IllegalArgumentException if the player is not the creator of the game
     */
    private void validateGameSettings(LightGame game, LightProfile profile) {

        if (game.getPlayerCreator().getUuid() != profile.getUuid()) {
            throw new IllegalArgumentException("The game is not created by the player " + profile.getUuid());
        }
    }

    public List<LightProfile> getUserList() {
        List<LightProfile> list_p = new ArrayList<>();
        Iterator<PublicProfile> it = connectedUsers.iterator();
        while (it.hasNext()) {
            list_p.add(it.next().getLightProfile());
        }
        return list_p;
    }

    public PublicProfile fetchProfileInfo(UUID uuid) {
        Iterator<PublicProfile> it = connectedUsers.iterator();

        while (it.hasNext()) {
            PublicProfile pp = it.next();
            if (pp.getUuid().equals(pp.getUuid())) {
                return pp;
            }
        }

        return null;
    }

    public List<LightGame> getGameList() {
        Iterator<Game> it = onGoingGames.iterator();

        ArrayList<LightGame> listLightGame = new ArrayList<>();

        while (it.hasNext()) {
            listLightGame.add(it.next().getLightGame());
        }

        return listLightGame;
    }

    public Game fetchGame(LightGame game) {
        return fetchGame(game.getUuid());
    }

    public Game fetchGame(UUID game) {
        Iterator<Game> it = onGoingGames.iterator();

        while (it.hasNext()) {
            Game g = it.next();
            if (g.getUuid().equals(game)) {
                return g;
            }
        }
        return null;
    }

    public void addProfile(PublicProfile profile) {
        connectedUsers.add(profile);
    }

    public void removeProfile(LightProfile profile) {

        Game g = findGameWhereUserIsSpectator(profile);

        if (g != null) {
            g.removeSpectator(profile);
        }

        Game g2 = findGameWhereUserIsPlayer(profile);

        if (g2 != null) {
            g2.removePlayer(profile);
        }
        // PH : Ajout de cette ligne pour supprimer un PublicProfile via un LightProfile
        connectedUsers.removeIf(p -> Objects.equals(p.getUsername(), profile.getUsername()));

    }

    public LightGame createGame(LightGame lightGame) {
        validateGameSettings(lightGame, lightGame.getPlayerCreator());
        Color colorCreator = lightGame.getColorCreator();
        if (colorCreator == Color.EMPTY) {
            colorCreator = Math.random() > 0.5 ? Color.RED : Color.YELLOW;
        }

        Game game = new Game(
                null,
                lightGame.getPlayerCreator(),
                null,
                UUID.randomUUID(),
                lightGame.getGrid(),
                colorCreator,
                colorCreator == Color.RED ? Color.YELLOW : Color.RED,
                lightGame.isChatEnabled(),
                new ArrayList<>(),
                new ArrayList<>(),
                null,
                // createBoard(pGridFormat)
                null);

        onGoingGames.add(game);
        return game.getLightGame();
    }

    public void deleteGame(UUID uuid) {
        onGoingGames.removeIf(game -> (game.getUuid() == uuid));
    }

    public void processMove(Move move, UUID uuidGame) {
        Iterator<Game> it = onGoingGames.iterator();

        while (it.hasNext()) {
            Game game = it.next();

            if (game.getUuid().equals(uuidGame)) {
                try {
                    game.addMove(move);
                    game.calculResult();
                } catch (IllegalStateException | IllegalArgumentException err) {
                    logger.error("Error during handling move", err);
                }

                if (game.isFinished() != GameResult.NO_RESULT) {
                    dataServerAskComServer.gameResult(game);
                }

                break;
            }
        }
    }

    public Game findGameWhereUserIsPlayer(LightProfile profile) {
        Iterator<Game> it = onGoingGames.iterator();

        while (it.hasNext()) {
            Game g = it.next();

            if (g.getPlayer2() == profile || g.getPlayerCreator() == profile) {
                return g;
            }
        }

        return null;
    }

    public Game findGameWhereUserIsSpectator(LightProfile profile) {
        Iterator<Game> it = onGoingGames.iterator();

        while (it.hasNext()) {
            Game g = it.next();

            Iterator<LightProfile> itSpectator = g.getSpectators().iterator();

            while (itSpectator.hasNext()) {
                LightProfile spec = itSpectator.next();

                if (spec == profile) {
                    return g;
                }
            }
        }

        return null;

    }

    @Override
    public void setDataServerAskComServer(IDataServerAskComServer dataServerAskComServer) {
        this.dataServerAskComServer = dataServerAskComServer;
    }

    @Override
    public IComServerAskDataServer getComServerAskDataServer() {
        return this.comServerAskDataServer;
    }

    @Override
    public void setDataServerAskAdmin(IDataServerAskAdmin dataServerAskAdmin) {
        this.dataServerAskAdmin = dataServerAskAdmin;
    }

    @Override
    public IAdminAskDataServer getAdminAskDataServer() {
        return this.adminAskDataServer;
    }

}
