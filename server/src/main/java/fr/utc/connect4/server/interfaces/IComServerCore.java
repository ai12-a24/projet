package fr.utc.connect4.server.interfaces;

public interface IComServerCore {

    public abstract void setComServerAskDataServer(IComServerAskDataServer comServerAskDataServer);

    public abstract IDataServerAskComServer getDataServerAskComServer();

    public abstract void setComServerAskAdmin(IComServerAskAdmin comServerAskAdmin);

    public abstract IAdminAskComServer getAdminAskComServer();

}
