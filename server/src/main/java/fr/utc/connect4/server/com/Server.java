package fr.utc.connect4.server.com;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import com.fasterxml.jackson.core.JsonProcessingException;

import fr.utc.connect4.core.Json;
import fr.utc.connect4.core.ProfileNotFound;
import fr.utc.connect4.core.com.HandleAddPlayer;
import fr.utc.connect4.core.com.HandleDelPlayer;
import fr.utc.connect4.core.com.HandleDisconnected;
import fr.utc.connect4.core.com.IClientMessage;
import fr.utc.connect4.core.com.IPayload;
import fr.utc.connect4.core.com.IServerMessage;
import fr.utc.connect4.core.com.ResponseListGame;
import fr.utc.connect4.core.com.ResponseListPlayer;
import fr.utc.connect4.core.com.SendLogin;
import fr.utc.connect4.core.data.LightProfile;
import fr.utc.connect4.core.data.PublicProfile;
import fr.utc.connect4.server.interfaces.IComServerAskDataServer;

/**
 * Represents the server handling WebSocket communication for the Connect 4
 * game. Extends {@link WebSocketServer} to handle incoming connections and
 * manage client interactions.
 */
public class Server extends WebSocketServer {

    /**
     * Logger instance for logging information, warnings, and errors related to the
     * {@code Server} class.
     */
    private static final Logger logger = LogManager.getLogger(Server.class);

    /**
     * A map storing connections by UUID.
     */
    private final ConcurrentHashMap<UUID, Connection> connectionByUUID;

    /**
     * A map storing connections by WebSocket.
     */
    private final ConcurrentHashMap<WebSocket, Connection> connectionBySocket;

    /**
     * Interface for requesting server-side data.
     */
    private final IComServerAskDataServer comServerAskDataServer;

    /**
     * Core server functionality for handling communication.
     */
    private final ComServerCore comServerCore;

    /**
     * Core server functionality for handling communication.
     */
    private Exception exception;

    /**
     * Constructs a new Server instance.
     * 
     * @param port                   the port on which the server will listen for
     *                               incoming WebSocket connections.
     * @param comServerCore          the core server component for handling
     *                               communication logic.
     * @param comServerAskDataServer the server component for requesting data from
     *                               the server.
     */
    public Server(int port, ComServerCore comServerCore, IComServerAskDataServer comServerAskDataServer) {
        super(new InetSocketAddress(port));
        this.comServerCore = comServerCore;
        this.comServerAskDataServer = comServerAskDataServer;
        this.connectionByUUID = new ConcurrentHashMap<>();
        this.connectionBySocket = new ConcurrentHashMap<>();
        this.exception = null;
    }

    /**
     * Handles the opening of a new WebSocket connection.
     * 
     * @param socket    the WebSocket that was opened.
     * @param handshake the handshake associated with the WebSocket connection.
     */
    @Override
    public void onOpen(WebSocket socket, ClientHandshake handshake) {
        logger.info("New connection from {}", socket.getRemoteSocketAddress());
    }

    /**
     * Handles incoming messages from clients.
     * 
     * @param socket  the WebSocket from which the message was received.
     * @param message the message received from the client.
     */
    @Override
    public void onMessage(WebSocket socket, String message) {
        IPayload abstractMessage;
        try {
            abstractMessage = Json.loads(message, IClientMessage.class);
            if (abstractMessage instanceof SendLogin loginUser) {
                handleSendLogin(socket, loginUser);
            } else {
                Connection conn = connectionBySocket.get(socket);
                if (conn == null) {
                    logger.warn("Unauthenticated request from {}: {}", socket.getRemoteSocketAddress(),
                            abstractMessage);
                } else {
                    logger.info("Recv from {} ({}): {}",
                            conn.getProfile().getUuid(),
                            conn.getSocket().getRemoteSocketAddress(),
                            message);
                    comServerCore.handlePayload(abstractMessage, conn);
                }
            }
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * Handles a login request by adding the profile to the server and broadcasting
     * player addition.
     * 
     * @param socket    the WebSocket from which the login request was received.
     * @param loginUser the login request containing the player's credentials.
     */
    private void handleSendLogin(WebSocket socket, SendLogin loginUser) {
        PublicProfile profile = loginUser.login();
        comServerAskDataServer.addProfile(profile);

        LightProfile lightProfile = profile.getLightProfile();
        this.register(new Connection(socket, lightProfile));
        this.broadcastExpect(new HandleAddPlayer(lightProfile), profile);
        try {
            this.send(profile, new ResponseListGame(comServerAskDataServer.getGameList()));
            this.send(profile, new ResponseListPlayer(comServerAskDataServer.getUserList()));
        } catch (JsonProcessingException | ProfileNotFound e) {
            logger.error("Error during login: {}", e.getMessage());
        }
    }

    /**
     * Handles closing of a WebSocket connection.
     * 
     * @param socket the WebSocket that was closed.
     * @param code   the closure code indicating the reason for the closure.
     * @param reason the reason for the closure.
     * @param remote true if the closure was initiated by the remote peer, false
     *               otherwise.
     */
    @Override
    public void onClose(WebSocket socket, int code, String reason, boolean remote) {
        synchronized (this) {
            Connection conn = connectionBySocket.remove(socket);
            if (conn != null) {
                terminate(conn);
            }
        }
        logger.info(
                "Connection closed: {}, Reason: {}",
                socket.getRemoteSocketAddress(),
                reason);
    }

    /**
     * Handles errors that occur during WebSocket communication.
     * 
     * @param conn the WebSocket connection that caused the error.
     * @param ex   the exception that occurred.
     */
    @Override
    public void onError(WebSocket conn, Exception ex) {
        logger.error(ex.getMessage(), ex);
        this.exception = ex;
    }

    /**
     * Starts the WebSocket server and show all ipv4 addresses.
     */
    @Override
    public void onStart() {
        logger.info("Server started on port {}", this.getAddress().getPort());
        try {
            // Get all network interfaces on the system
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = networkInterfaces.nextElement();
                // Skip loopback and non-active interfaces
                if (!networkInterface.isUp()) {
                    continue;
                }
                // Get all IP addresses associated with the interface
                List<String> addresses = new ArrayList<>();
                Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();
                while (inetAddresses.hasMoreElements()) {
                    InetAddress inetAddress = inetAddresses.nextElement();
                    if (inetAddress instanceof Inet4Address) {
                        addresses.add(inetAddress.getHostAddress());
                    }
                }
                // Shot the interface only if theres are available ips
                if (!addresses.isEmpty()) {
                    logger.info("Interface {}: {}", networkInterface.getName(), String.join(", ", addresses));
                }
            }
        } catch (SocketException e) {
            logger.error("Cannot resolve interfaces", e);
        }
    }

    /**
     * Sends a message to a specific client identified by their UUID.
     * 
     * @param uuid    the UUID of the client to whom the message will be sent.
     * @param payload the message to be sent.
     * @throws ProfileNotFound         if the client profile is not found.
     * @throws JsonProcessingException if an error occurs while serializing the
     *                                 message.
     */
    public void send(UUID uuid, IServerMessage payload) throws ProfileNotFound, JsonProcessingException {
        Connection conn = connectionByUUID.get(uuid);
        if (conn == null) {
            throw new ProfileNotFound();
        }
        conn.send(payload);
    }

    /**
     * Sends a message to a specific client identified by their LightProfile.
     * 
     * @param profile the LightProfile of the client to whom the message will be
     *                sent.
     * @param payload the message to be sent.
     * @throws ProfileNotFound         if the client profile is not found.
     * @throws JsonProcessingException if an error occurs while serializing the
     *                                 message.
     */
    public void send(LightProfile profile, IServerMessage payload) throws ProfileNotFound, JsonProcessingException {
        send(profile.getUuid(), payload);
    }

    /**
     * Broadcasts a message to all connected clients.
     * 
     * @param payload the message to be broadcasted.
     */
    public void broadcast(IServerMessage payload) {
        this.broadcastExpect(payload);
    }

    /**
     * Broadcasts a message to all clients except those specified in the list of
     * profiles.
     * 
     * @param payload  the message to be broadcasted.
     * @param profiles the profiles of clients who should not receive the
     *                 broadcasted message.
     */
    public void broadcastExpect(IServerMessage payload, LightProfile... profiles) {
        Set<UUID> uuids = Arrays.stream(profiles).map(LightProfile::getUuid).collect(Collectors.toSet());
        connectionByUUID.values().stream().parallel().forEach(conn -> {
            if (uuids.contains(conn.getProfile().getUuid())) {
                return;
            }
            conn.send(payload);
        });
    }

    /**
     * Broadcasts a message to only specified all clients.
     * 
     * @param payload  the message to be broadcasted.
     * @param profiles the profiles of clients who should receive the
     *                 broadcasted message.
     */
    public void broadcastOnly(IServerMessage payload, LightProfile... profiles) {
        Set<UUID> uuids = Arrays.stream(profiles).map(LightProfile::getUuid).collect(Collectors.toSet());
        connectionByUUID.values().stream().parallel().forEach(conn -> {
            if (!uuids.contains(conn.getProfile().getUuid())) {
                return;
            }
            conn.send(payload);
        });
    }

    /**
     * Terminates the connection with a client and cleans up associated resources.
     * 
     * @param conn the connection to be terminated.
     */
    private void terminateUnsafe(Connection conn) {
        UUID uuid = conn.getProfile().getUuid();
        WebSocket socket = conn.getSocket();
        logger.info(
                "Disconnect {} ({})",
                socket.getRemoteSocketAddress(),
                uuid);
        // Abandon !
        this.broadcastExpect(new HandleDelPlayer(conn.getProfile()), conn.getProfile());
        if (socket.isOpen()) {
            conn.send(new HandleDisconnected());
            socket.close();
        }
        connectionByUUID.remove(uuid);
        connectionBySocket.remove(socket);
    }

    /**
     * Terminates the connection with a client and ensures thread safety.
     * 
     * @param conn the connection to be terminated.
     */
    synchronized private void terminate(Connection conn) {
        terminateUnsafe(conn);
    }

    /**
     * Registers a new client connection to the server.
     * 
     * @param conn the connection to be registered.
     */
    synchronized private void register(Connection conn) {
        UUID uuid = conn.getProfile().getUuid();
        WebSocket socket = conn.getSocket();
        logger.info("Login {} ({})", socket.getRemoteSocketAddress(), uuid);
        Connection previousConnection = connectionByUUID.get(uuid);
        if (previousConnection != null) {
            this.terminateUnsafe(previousConnection);
        }
        connectionByUUID.put(uuid, conn);
        connectionBySocket.put(socket, conn);
    }

    /**
     * Stops the WebSocket server and closes all open connections.
     */
    public void close() {
        try {
            this.stop(10);
            logger.info("Server stopped");
        } catch (InterruptedException e) {
            logger.warn("Cannot close some connections.");
        }
    }

    /**
     * Wait for exception.
     */
    public void waitException(Duration duration) throws Exception {
        Instant start = Instant.now(); // Record the start time
        // Run the loop until the duration is elapsed
        while (Duration.between(start, Instant.now()).compareTo(duration) < 0) {
            if (this.exception != null) {
                throw this.exception;
            }
            try {
                Thread.sleep(50); // Sleep for 50 milliseconds
            } catch (InterruptedException e) {
            }
        }
        if (this.exception != null) {
            throw this.exception;
        }
    }

}
