package fr.utc.connect4.server.data;

import fr.utc.connect4.server.interfaces.IAdminAskDataServer;

public class AdminAskDataServer implements IAdminAskDataServer {

    @SuppressWarnings("unused")
    private final DataServerCore dataCore;

    public AdminAskDataServer(DataServerCore dataCore) {
        this.dataCore = dataCore;
    }

    @Override
    public void close() {
        // Do nothing for the moment
    }

}
