package fr.utc.connect4.server.interfaces;

import javafx.stage.Stage;

public interface IAdminCore {

    public abstract void setAdminAskDataServer(IAdminAskDataServer adminAskDataServer);

    public abstract IDataServerAskAdmin getDataServerAskAdmin();

    public abstract void setAdminAskComServer(IAdminAskComServer adminAskComServer);

    public abstract IComServerAskAdmin getComServerAskAdmin();

    public abstract void start(Stage primaryStage);

}