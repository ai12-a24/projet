package fr.utc.connect4.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The {@code ServerLauncher} class is responsible for starting the server
 * application. It contains the main entry point of the application, where it
 * attempts to run the server and logs any errors encountered during the
 * process.
 */
public class ServerLauncher {

    /**
     * Logger for {@code ServerLauncher} class. This is used to log error messages
     * when an exception occurs during the execution of the main method.
     */
    private static final Logger logger = LogManager.getLogger(ServerLauncher.class);

    /**
     * The main method which serves as the entry point of the application.
     * It invokes the {@link ServerApp#run()} method to start the server.
     * If an exception occurs during the execution, it is logged using the
     * {@link #logger}.
     *
     * @param args Command-line arguments passed to the program. Currently unused.
     */
    @SuppressWarnings("all")
    public static void main(String[] args) {
        try {
            // Attempts to run the server application
            ServerApp.run();
        } catch (Exception err) {
            // Logs any error that occurs during the execution
            logger.error("An error occurred: ", err);
        }
    }
}
