package fr.utc.connect4.server.admin;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.application.Platform;
import javafx.scene.control.TextArea;

/**
 * The {@code LogToTextArea} class reads log content from a file and updates the
 * provided
 * {@link TextArea} with the content. It continuously monitors the file for
 * changes and
 * updates the text area accordingly. This class runs in a separate thread to
 * avoid blocking
 * the main UI thread.
 * <p>
 * This class utilizes Apache Log4j for logging and JavaFX for GUI interaction.
 */
public class LogToTextArea {

    /**
     * Logger instance used for logging events related to this class.
     */
    private static final Logger logger = LogManager.getLogger(LogToTextArea.class);

    /**
     * The {@link TextArea} where the log content will be displayed.
     */
    private final TextArea textArea;

    /**
     * The path to the file from which log content will be read.
     */
    private final String path;

    /**
     * The thread that continuously monitors the log file and updates the text area.
     */
    private final Thread runner;

    /**
     * The previous content of the log file. This is used to detect new content
     * since
     * the last update.
     */
    private String previous;

    /**
     * Constructs a {@code LogToTextArea} instance that monitors the specified log
     * file
     * and updates the provided {@link TextArea}.
     *
     * @param path     The path to the log file.
     * @param textArea The {@link TextArea} to update with the log content.
     */
    protected LogToTextArea(String path, TextArea textArea) {
        this.textArea = textArea;
        this.path = path;
        this.previous = "";
        this.runner = new Thread(() -> {
            while (true) {
                if (Thread.currentThread().isInterrupted()) {
                    logger.info("Stopped LogToTextArea");
                    return;
                }
                try {
                    Thread.sleep(500);
                    synchronized (this) { // Synchronization to avoid conflicts
                        this.updateTextArea();
                    }
                } catch (InterruptedException e) {
                    logger.info("Stopped LogToTextArea");
                }
            }
        });
    }

    /**
     * Reads the log file and updates the {@link TextArea} with any new content. If
     * the
     * content has changed since the last update, the new content is appended to the
     * {@link TextArea}. If the file has been reset, the entire content is
     * displayed.
     */
    synchronized public void updateTextArea() {
        String content;
        try {
            content = Files.readString(Paths.get(this.path));
        } catch (IOException ex) {
            logger.info("File {} does not exist", this.path);
            return;
        }

        final String fixContent = content;
        if (content.startsWith(previous)) {
            final String newContent = fixContent.substring(previous.length());
            if (!"".equals(newContent)) {
                Platform.runLater(() -> {
                    textArea.appendText(newContent);
                });
            }
        } else {
            Platform.runLater(() -> {
                textArea.setText(fixContent);
            });
        }
        previous = content;
    }

    /**
     * Starts the monitoring of the log file and updates the {@link TextArea}.
     * If the log monitoring is already running, it will stop the current thread and
     * restart it.
     */
    synchronized public void start() {
        if (this.isRunning()) {
            this.stop();
        }
        this.updateTextArea();
        this.runner.start();
    }

    /**
     * Stops the monitoring of the log file by interrupting the monitoring thread.
     */
    synchronized public void stop() {
        this.runner.interrupt();
    }

    /**
     * Checks whether the log monitoring thread is currently running.
     *
     * @return {@code true} if the monitoring thread is alive, {@code false}
     *         otherwise.
     */
    public boolean isRunning() {
        return this.runner.isAlive();
    }
}
