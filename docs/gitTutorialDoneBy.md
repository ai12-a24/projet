# Mark here if you have completed the tutorial by changing 'NO' to 'YES

## All Git Manager must ensure that their team members are added to GitLab and that their names are present on this list

| GIT Id                          | Realised |
| ------------------------------- | -------- |
| Axel Goret @goretaxe            | YESSAI   |
| Baptiste Buvron @buvronba       | YES      |
| Cyprien Dujardin @cdujardi      | YES      |
| Dylan Girault @dgirault         | YES      |
| Eliot Dewulf @dewulfel          | YES      |
| Eric Luo @luoeric               | YES      |
| Eva Guignabodet @eguignab       | YES      |
| Felix Incerti @fincerti         | YES      |
| Haiyang Ma @mahaiyan            | YES      |
| Ikram Dama @damaikra            | NO       |
| Jana Eltayeb El Rafei @jeltayeb | YES      |
| Jessica Kamga @kamgajes         | YES      |
| Louis Coulon @coulonlo          | YES      |
| Mateo Mercier @matmerci         | NO       |
| Melissa Moreira @memoreir       | YES      |
| Morgan Westmeyer @mwestmey      | YES      |
| Nam Khanh Nguyen @nguyenna      | YES      |
| Paul-Antoine Armani @armanipa   | YES      |
| Paul-Henri Favrelle @pfavrell   | YES      |
| Quentin Glandier @qglandie      | YES      |
| Soudarsane Tillai @tillaiso     | NO       |
| Victor Martin @martivic         | YES      |
| Rayan Chergui @rchergui         | YES      |
