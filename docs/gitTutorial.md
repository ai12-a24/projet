# Tutorial : How to create an Issue and a merge Request and commit

The goal of this tutorial is for you to create a branch, make a commit in it, and then merge it.

## Step 0 : Install and clone the repository

Follow the guide for [development setup](https://gitlab.utc.fr/ai12-a24/Projet/-/blob/main/docs/development.md).

## Step 1 : Access the Issues section

From the GitLab project page, start by clicking on **Issues** in the sidebar.

![Step 1](https://gitlab.utc.fr/ai12-a24/projet/-/raw/main/docs/gitCreateIssueTutorialPictures/step1.png)

---

## Step 2 : Create a new Issue

Once you're on the **Issues** page, click on **New Issue** to start creating a new issue.

![Step 2](https://gitlab.utc.fr/ai12-a24/projet/-/raw/main/docs/gitCreateIssueTutorialPictures/step2.png)

---

## Step 3 : Fill in the Issue details

On the **New Issue** page, fill in the following fields:

- **Title :** Enter the title of your issue.
- **Assignee :** Select the person who will be responsible for development on this branch.
- **Due date :** Set the due date.
- **Label :** Choose **"feature"** to label the issue.

![Step 3](https://gitlab.utc.fr/ai12-a24/projet/-/raw/main/docs/gitCreateIssueTutorialPictures/step3.png)

### Example

Here is an example of a page with the fields filled in. Once done, click on **Create Issue**.

![Step 4](https://gitlab.utc.fr/ai12-a24/projet/-/raw/main/docs/gitCreateIssueTutorialPictures/step4.png)

---

## Step 4 : Issue created

Congratulations, your issue is now created ! You are ready to create a Merge Request. This is at this step that your branch will be created.

![Step 5](https://gitlab.utc.fr/ai12-a24/projet/-/raw/main/docs/gitCreateIssueTutorialPictures/step5.png)

---

## Step 5 : Create a Merge Request

On the **New Merge Request** page, fill in the following fields:

- **Assignee :** Select the person responsible for development on this branch.
- **Reviewer :** Choose the Quality manager or the Git maintainer of your team.
- **Label :** Select **"feature"**.

![Step 6](https://gitlab.utc.fr/ai12-a24/projet/-/raw/main/docs/gitCreateIssueTutorialPictures/step6.png)

Once the information is completed, click on **Create merge request**.

![Step 7](https://gitlab.utc.fr/ai12-a24/projet/-/raw/main/docs/gitCreateIssueTutorialPictures/step7.png)

---

## Step 6 : Checkout the branch

The Merge Request is now open, you need to click on the **Code** tab and then select **Check out branch**.

![Step 8](https://gitlab.utc.fr/ai12-a24/projet/-/raw/main/docs/gitCreateIssueTutorialPictures/step8.png)

You will see the commands to execute in order to access your branch and push your changes to GitLab.

![Step 9](https://gitlab.utc.fr/ai12-a24/projet/-/raw/main/docs/gitCreateIssueTutorialPictures/step9.png)

⚠️ Consider the risk of losing your unsaved changes when using the `checkout` command.

---

## Step 7 : Commit

Edit the file in `docs/gitTutorialDoneBy.md` and change `NO` to `YES` for your user. If it is not present, add it.

Then you need to Commit your change and push your commit:

```bash
git add -A
git commit -m "Descriptive message of what changed"
git push
```

⚠️ Never use `--force` or you risk to override change of someone else.

---

## Step 8 : Merge your branch to dev

Now that you've made a commit to your branch, you need to access to your merge request.

![Step 10](https://gitlab.utc.fr/ai12-a24/projet/-/raw/main/docs/gitCreateIssueTutorialPictures/step10.png)  

You should see your commit here in the Activity section.  

<img src="https://gitlab.utc.fr/ai12-a24/projet/-/raw/main/docs/gitCreateIssueTutorialPictures/activityOfMergeRequest.png" alt="Commit in activity section" width="60%">  

Your branch is still in draft, as you finished to update your branch, you can click here to mark it as ready.  

<img src="https://gitlab.utc.fr/ai12-a24/projet/-/raw/main/docs/gitCreateIssueTutorialPictures/markAsReady.png" alt="Mark as ready" width="60%">  

Now you will be able to merge your branch by clicking on "Merge".  

<img src="https://gitlab.utc.fr/ai12-a24/projet/-/raw/main/docs/gitCreateIssueTutorialPictures/merge.png" alt="Merge" width="60%">

---

## End of Tutorial
