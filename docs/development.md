# Setup development

## Supported IDEs

- [Visual Studio Code](https://code.visualstudio.com)
- [IntelliJ IDEA](https://www.jetbrains.com/idea/)

## Install by operating system

⚠️ You must install java 22 or greater.

<details>
  <summary>Windows</summary>

### Windows: Install chocolatey, Java and git

⚠️ Open an admin shell with `windows + x -> a`.

Check if java is not already installed wih:

```powershell
java -version
```

This will display your java version, **which must be at least 22**. If this is not the case, uninstall java before continuing.

Output example of a java version greater than 22:

```text
openjdk version "22.0.2" 2024-07-16
OpenJDK Runtime Environment (build 22.0.2+9-70)
OpenJDK 64-Bit Server VM (build 22.0.2+9-70, mixed mode, sharing)
```

Output example if java is not installed:

```text
'java' is not recognized as an internal or external command, operable program or batch file.
```

After checking that your computer does not have java enter:

```powershell
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
choco install -y openjdk
choco install -y git
```

### Troubleshooting Windows: Uninstall java

You can run the script `uninstall-java.ps1` for get commands to run for uninstall your previous java installation.

```powershell
./tools/uninstall-java.ps1
```

### Windows: Download project from git

⚠️ Re-open a shell in your project folder.

```bash
git clone https://gitlab.utc.fr/ai12-a24/projet
cd projet
```

### Windows: First run

```powershell
./mvnw.cmd clean install dependency:sources
```

### Windows: Fast compile and run

```powershell
./mvnw.cmd install -pl core,client -am -T 1C; ./mvnw.cmd exec:java -pl client
./mvnw.cmd install -pl core,server -am -T 1C; ./mvnw.cmd exec:java -pl server
```

### Windows: Create Fat-Jar, MSI Installer and EXE Installer

```powershell
.\tools\bundle-windows.ps1
```

### Windows: Install the project

```powershell
.\build\connect4-server-1.0.0.0.exe
.\build\connect4-client-1.0.0.0.exe
```

</details>

<details>
  <summary>Ubuntu</summary>

### Ubuntu: Update, Install Java, Git and GitFlow

```bash
sudo apt -y update && sudo apt -y install git
wget https://download.oracle.com/java/23/latest/jdk-23_linux-x64_bin.deb
sudo dpkg -i jdk-23_linux-x64_bin.deb
rm jdk-23_linux-x64_bin.deb
sudo  update-java-alternatives --set jdk-23.0.1-oracle-x64
nano /etc/profile
# export JAVA_HOME="$(dirname "$(dirname "$(readlink -f "$(command -v java)")")")"
```

### Ubuntu: Download project from git

⚠️ Re-open a shell in your project folder.

```bash
git clone https://gitlab.utc.fr/ai12-a24/projet
cd projet
```

### Ubuntu: Make Maven Wrapper executable

```bash
chmod +x mvnw
```

### Ubuntu: First run

```bash
./mvnw clean install dependency:sources
```

### Ubuntu: Fast compile and run

```bash
./mvnw install -pl core,client -am -T 1C && ./mvnw exec:java -pl client
./mvnw install -pl core,server -am -T 1C && ./mvnw exec:java -pl server
```

### Ubuntu: Create .deb package

```bash
chmod +x ./tools/bundle-linux.sh
dos2unix tools/bundle-linux.sh
./tools/bundle-linux.sh
```

### Ubuntu: Install the project

```bash
sudo dpkg -i 'build/connect4-server_1.0.0.0_amd64.deb'
sudo dpkg -i 'build/connect4_1.0.0.0_amd64.deb'
```

</details>

<details>
  <summary>Mac</summary>

### Mac: Install Homebrew, Java and GitFlow

```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
echo "export PATH=/opt/homebrew/bin:$PATH" >> ~/.bash_profile && source ~/.bash_profile
brew install java git
```

### Mac: Download project from git

⚠️ Re-open a shell in your project folder.

```bash
git clone https://gitlab.utc.fr/ai12-a24/projet
cd projet
```

### Mac: Make Maven Wrapper executable

```bash
dos2unix ./mvnw
chmod +x mvnw
```

### Mac: First run

```bash
./mvnw clean install dependency:sources
```

### Mac: Fast compile and run

```bash
./mvnw install -pl core,client -am -T 1C && ./mvnw exec:java -pl client
./mvnw install -pl core,server -am -T 1C && ./mvnw exec:java -pl server
```

### Mac: standalone JAR

```bash
./mvnw clean install package -Passemble
```

</details>

## Run standalone JAR

```bash
java -jar client/target/client-1.0.0.0-jar-with-dependencies.jar
java -jar server/target/server-1.0.0.0-jar-with-dependencies.jar
```

## Build PlantUML

```bash
java -jar tools/plantuml-1.2024.7.jar -checkmetadata -o images docs/sequences/client/*.puml docs/sequences/server/*.puml
```

## Development

Regenerate [Maven wrapper](https://maven.apache.org/wrapper/maven-wrapper-plugin/) (only if you know what you are doing).

```bash
mvn wrapper:wrapper -Dtype=script
```

## Meaningful commit with Python (optional)

```bash
# Install commitizen
pip install pipx
pipx ensurepath
pipx install commitizen

# Commit
git add .
cz c
git push
```
