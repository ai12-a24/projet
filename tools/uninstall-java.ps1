# Function to uninstall via the registry
function UninstallJavaFromRegistry {
    $uninstallKeys = @(
        "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall",
        "HKLM:\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall"
    )

    foreach ($key in $uninstallKeys) {
        Get-ChildItem -Path $key -ErrorAction SilentlyContinue | ForEach-Object {
            $displayName = $_.GetValue("DisplayName")
            $uninstallString = $_.GetValue("UninstallString")
            if ($displayName -match "Java") {
                if ($uninstallString) {
                    Write-Host "$uninstallString"
                }
            }
        }
    }
}

# Function to uninstall via winget
function UninstallJavaFromWinget {
    $wingetList = winget list | Out-String
    if ($wingetList -match "Java") {
        $javaName = ($wingetList -split "`n" | Where-Object { $_ -match "Java" }) -replace "\s{2,}", " " | ForEach-Object { ($_ -split " ")[0] }
        Write-Host "winget uninstall --name $javaName --silent"
    }
}

# Function to uninstall via Chocolatey
function UninstallJavaFromChoco {
    $chocoList = choco list --local-only | Out-String
    if ($chocoList -match "jdk") {
        $javaName = ($chocoList -split "`n" | Where-Object { $_ -match "jdk" }) -replace "\s{2,}", " " | ForEach-Object { ($_ -split " ")[0] }
        Write-Host "choco uninstall -y $javaName"
    }
}

# Main function to uninstall Java
function UninstallJava {
    UninstallJavaFromRegistry
    UninstallJavaFromWinget
    UninstallJavaFromChoco

}

UninstallJava
