# Enable debug and exit on error
Write-Host "[INFO] Building fat-jar"
Remove-Item -Path ".\server\target" -Recurse -Force -ErrorAction SilentlyContinue
Remove-Item -Path ".\client\target" -Recurse -Force -ErrorAction SilentlyContinue
Remove-Item -Path ".\core\target" -Recurse -Force -ErrorAction SilentlyContinue
./mvnw.cmd clean install package -Passemble

Write-Host "[INFO] Packaging windows server .msi"
New-Item -ItemType Directory -Path "build/server" -Force | Out-Null
Copy-Item "server/target/server-1.0.0.0-jar-with-dependencies.jar" "build/server/connect4-server.jar"
jpackage --input "build/server" `
         --name "Connect4 Server" `
         --main-jar "connect4-server.jar" `
         --main-class "fr.utc.connect4.server.ServerLauncher" `
         --type "msi" `
         --add-modules "java.base,java.se,jdk.unsupported" `
         --jlink-options '--strip-native-commands --strip-debug --no-man-pages --no-header-files --compress=zip-9' `
         --icon "server/src/main/resources/server/image/icon.ico" `
         --about-url "https://gitlab.utc.fr/ai12-a24/projet" `
         --app-version "1.0.0.0" `
         --description "Server for host Connect4 games." `
         --copyright "Copyright(C) 2024 UTC, All rights reserved" `
         --dest "build" `
         --win-menu `
         --win-per-user-install `
         --win-shortcut `
         --win-shortcut-prompt `
         --win-dir-chooser `
         --vendor UTC `
         --win-help-url "https://gitlab.utc.fr/ai12-a24/projet/-/issues/new"

Write-Host "[INFO] Packaging windows client .msi"
New-Item -ItemType Directory -Path "build/client" -Force | Out-Null
Copy-Item "client/target/client-1.0.0.0-jar-with-dependencies.jar" "build/client/connect4.jar"
jpackage --input "build/client" `
         --name "Connect4" `
         --main-jar "connect4.jar" `
         --main-class "fr.utc.connect4.client.ClientLauncher" `
         --type "msi" `
         --add-modules "java.base,java.se,jdk.unsupported" `
         --jlink-options '--strip-native-commands --strip-debug --no-man-pages --no-header-files --compress=zip-9' `
         --icon "client/src/main/resources/client/image/icon.ico" `
         --about-url "https://gitlab.utc.fr/ai12-a24/projet" `
         --app-version "1.0.0.0" `
         --description "Client for play Connect4." `
         --copyright "Copyright(C) 2024 UTC, All rights reserved" `
         --dest "build" `
         --win-menu `
         --win-per-user-install `
         --win-shortcut `
         --win-shortcut-prompt `
         --win-dir-chooser `
         --vendor UTC `
         --win-help-url "https://gitlab.utc.fr/ai12-a24/projet/-/issues/new"

Remove-Item -Path "build/client" -Recurse -Force -ErrorAction SilentlyContinue
Remove-Item -Path "build/server" -Recurse -Force -ErrorAction SilentlyContinue
Move-Item "build/Connect4 Server-1.0.0.0.msi" "build/connect4-server-windows-amd64.msi" -Force
Move-Item "build/Connect4-1.0.0.0.msi" "build/connect4-windows-amd64.msi" -Force

Write-Host "[INFO] Generating Client zip from .msi"
Remove-Item -Path "build\connect4-server-windows-amd64" -Recurse -Force -ErrorAction SilentlyContinue
Remove-Item -Path "build\connect4-server-windows-amd64.zip" -Recurse -Force -ErrorAction SilentlyContinue
msiexec /x build/connect4-server-windows-amd64.msi /quiet
Start-Sleep 20
build/connect4-server-windows-amd64.msi INSTALLDIR="${pwd}\build\connect4-server-windows-amd64" /quiet
Start-Sleep 60
Compress-Archive -Path "build\connect4-server-windows-amd64\*" -DestinationPath "build\connect4-server-windows-amd64.zip"
msiexec /x build/connect4-server-windows-amd64.msi /quiet
Start-Sleep 20
Remove-Item -Path "build\connect4-server-windows-amd64" -Recurse -Force -ErrorAction SilentlyContinue

Write-Host "[INFO] Generating Server zip from .msi"
Remove-Item -Path "build\connect4-windows-amd64" -Recurse -Force -ErrorAction SilentlyContinue
Remove-Item -Path "build\connect4-windows-amd64.zip" -Recurse -Force -ErrorAction SilentlyContinue
msiexec /x build/connect4-windows-amd64.msi /quiet
Start-Sleep 20
build/connect4-windows-amd64.msi INSTALLDIR="${pwd}\build\connect4-windows-amd64" /quiet
Start-Sleep 60
Compress-Archive -Path "build\connect4-windows-amd64\*" -DestinationPath "build\connect4-windows-amd64.zip"
msiexec /x build/connect4-windows-amd64.msi /quiet
Start-Sleep 20
Remove-Item -Path "build\connect4-windows-amd64" -Recurse -Force -ErrorAction SilentlyContinue

# Copy both jar
Copy-Item -Force "client/target/client-1.0.0.0-jar-with-dependencies.jar" "build/connect4.jar"
Copy-Item -Force "server/target/server-1.0.0.0-jar-with-dependencies.jar" "build/connect4-server.jar"
