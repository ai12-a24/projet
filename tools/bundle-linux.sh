#!/bin/bash

# Exit on error
set -x

echo "[INFO] Building fat-jar"
rm -rf "server/target" 2>/dev/null
rm -rf "client/target" 2>/dev/null
rm -rf "core/target" 2>/dev/null
./mvnw clean install package -Passemble

echo "[INFO] Packaging linux server .deb"
mkdir -p "build/server"
cp "server/target/server-1.0.0.0-jar-with-dependencies.jar" "build/server/connect4-server.jar"
jpackage --input "build/server" \
         --name "Connect4 Server" \
         --main-jar "connect4-server.jar" \
         --main-class "fr.utc.connect4.server.ServerLauncher" \
         --type "deb" \
         --app-version "1.0.0.0" \
         --add-modules "java.base,java.se,jdk.unsupported" \
         --jlink-options '--strip-native-commands --strip-debug --no-man-pages --no-header-files --compress=zip-9' \
         --icon "server/src/main/resources/server/image/icon.png" \
         --description "Server for host Connect4 games." \
         --copyright "Copyright(C) 2024 UTC, All rights reserved" \
         --dest "build" \
         --linux-package-name "connect4-server" \
         --linux-shortcut \
         --vendor "UTC"
mv build/connect4-server_1.0.0.0_amd64.deb build/connect4-server-linux-amd64.deb

echo "[INFO] Fix server .deb for use xz instead of zstd"
mkdir build/tmp
ar x build/connect4-server-linux-amd64.deb --output build/tmp
zstd -d < build/tmp/control.tar.zst | xz > build/tmp/control.tar.xz
zstd -d < build/tmp/data.tar.zst | xz > build/tmp/data.tar.xz
rm build/connect4-server-linux-amd64.deb
ar -m -c -a sdsd build/connect4-server-linux-amd64.deb build/tmp/debian-binary build/tmp/control.tar.xz build/tmp/data.tar.xz
rm -rf build/tmp

echo "[INFO] Packaging linux client .deb"
mkdir -p "build/client"
cp "client/target/client-1.0.0.0-jar-with-dependencies.jar" "build/client/connect4.jar"
jpackage --input "build/client" \
         --name "Connect4" \
         --main-jar "connect4.jar" \
         --main-class "fr.utc.connect4.client.ClientLauncher" \
         --type "deb" \
         --app-version "1.0.0.0" \
         --add-modules "java.base,java.se,jdk.unsupported" \
         --jlink-options '--strip-native-commands --strip-debug --no-man-pages --no-header-files --compress=zip-9' \
         --icon "client/src/main/resources/client/image/icon.png" \
         --description "Client for play Connect4." \
         --copyright "Copyright(C) 2024 UTC, All rights reserved" \
         --dest "build" \
         --linux-package-name "connect4" \
         --linux-shortcut \
         --vendor "UTC"
mv build/connect4_1.0.0.0_amd64.deb build/connect4-linux-amd64.deb

echo "[INFO] Fix client .deb for use xz instead of zstd"
mkdir build/tmp
ar x build/connect4-linux-amd64.deb --output build/tmp
zstd -d < build/tmp/control.tar.zst | xz > build/tmp/control.tar.xz
zstd -d < build/tmp/data.tar.zst | xz > build/tmp/data.tar.xz
rm build/connect4-linux-amd64.deb
ar -m -c -a sdsd build/connect4-linux-amd64.deb build/tmp/debian-binary build/tmp/control.tar.xz build/tmp/data.tar.xz


cd build

echo "[INFO] Create tar.gz for server"
rm -f connect4-server-linux-amd64.tar.gz
ar x 'connect4-server-linux-amd64.deb'
mkdir -p local
tar -xvf 'data.tar.xz' -C local
(cd local/opt/connect4-server && tar -czvf connect4-server-linux-amd64.tar.gz *)
mv local/opt/connect4-server/connect4-server-linux-amd64.tar.gz connect4-server-linux-amd64.tar.gz
rm -rf 'data.tar.xz' 'control.tar.xz' 'debian-binary' local

echo "[INFO] Create tar.gz for client"
rm -f connect4-linux-amd64.tar.gz
ar x 'connect4-linux-amd64.deb'
mkdir -p local
tar -xvf 'data.tar.xz' -C local
(cd local/opt/connect4 && tar -czvf connect4-linux-amd64.tar.gz *)
mv local/opt/connect4/connect4-linux-amd64.tar.gz connect4-linux-amd64.tar.gz
rm -rf 'data.tar.xz' 'control.tar.xz' 'debian-binary' local

cd ..

rm -rf build/tmp
rm -rf build/client
rm -rf build/server

cp "client/target/client-1.0.0.0-jar-with-dependencies.jar" "build/connect4.jar"
cp "server/target/server-1.0.0.0-jar-with-dependencies.jar" "build/connect4-server.jar"
