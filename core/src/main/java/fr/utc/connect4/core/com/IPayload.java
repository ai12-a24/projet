package fr.utc.connect4.core.com;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import fr.utc.connect4.core.data.Game;
import fr.utc.connect4.core.data.LightGame;
import fr.utc.connect4.core.data.LightProfile;
import fr.utc.connect4.core.data.Message;
import fr.utc.connect4.core.data.Move;
import fr.utc.connect4.core.data.PrivateProfile;
import fr.utc.connect4.core.data.PublicProfile;
import fr.utc.connect4.core.data.Result;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
                // Base type
                @JsonSubTypes.Type(value = LightProfile.class, name = "LightProfile"),
                @JsonSubTypes.Type(value = PublicProfile.class, name = "PublicProfile"),
                @JsonSubTypes.Type(value = PrivateProfile.class, name = "PrivateProfile"),
                @JsonSubTypes.Type(value = Message.class, name = "Message"),
                @JsonSubTypes.Type(value = LightGame.class, name = "LightGame"),
                @JsonSubTypes.Type(value = Game.class, name = "Game"),
                @JsonSubTypes.Type(value = Move.class, name = "Move"),
                @JsonSubTypes.Type(value = Result.class, name = "Result"),

                // Client message
                @JsonSubTypes.Type(value = RequestCreateGame.class, name = "RequestCreateGame"),
                @JsonSubTypes.Type(value = RequestListGame.class, name = "RequestListGame"),
                @JsonSubTypes.Type(value = RequestListPlayer.class, name = "RequestListPlayer"),
                @JsonSubTypes.Type(value = RequestNewMove.class, name = "RequestNewMove"),
                @JsonSubTypes.Type(value = SendLogin.class, name = "SendLogin"),
                @JsonSubTypes.Type(value = SendText.class, name = "SendText"),
                @JsonSubTypes.Type(value = RequestJoinGame.class, name = "RequestJoinGame"),
                @JsonSubTypes.Type(value = RequestFetchProfile.class, name = "RequestFetchProfile"),
                @JsonSubTypes.Type(value = RequestDelPlayer.class, name = "RequestDelPlayer"),

                // Server Message
                @JsonSubTypes.Type(value = HandleAddPlayer.class, name = "HandleAddPlayer"),
                @JsonSubTypes.Type(value = HandleDelPlayer.class, name = "HandleDelPlayer"),
                @JsonSubTypes.Type(value = HandleDisconnected.class, name = "HandleDisconnected"),
                @JsonSubTypes.Type(value = HandleCreateGame.class, name = "HandleCreateGame"),
                @JsonSubTypes.Type(value = HandleLaunchGame.class, name = "HandleLaunchGame"),
                @JsonSubTypes.Type(value = HandleUpdateGame.class, name = "HandleUpdateGame"),
                @JsonSubTypes.Type(value = HandleUpdateMove.class, name = "HandleUpdateMove"),
                @JsonSubTypes.Type(value = ResponseListGame.class, name = "ResponseListGame"),
                @JsonSubTypes.Type(value = ResponseListPlayer.class, name = "ResponseListPlayer"),
                @JsonSubTypes.Type(value = ResponsePlayerJoinsFullGame.class, name = "ResponsePlayerJoinsFullGame"),
                @JsonSubTypes.Type(value = ResponseNewMove.class, name = "ResponseNewMove"),
                @JsonSubTypes.Type(value = ResponseFetchProfile.class, name = "ResponseFetchProfile"),
                @JsonSubTypes.Type(value = HandleGameEnd.class, name = "HandleGameEnd"),
})
public abstract interface IPayload {

}
