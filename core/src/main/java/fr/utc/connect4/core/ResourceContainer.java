package fr.utc.connect4.core;

import java.util.Objects;

/**
 * Abstract class representing a container for a resource of type {@code T}.
 * <p>
 * This class provides a mechanism to lazily load and retrieve a resource using
 * a specified path.
 * Subclasses must implement the {@link #load()} method to define how the
 * resource is loaded.
 * 
 * @param <T> the type of the resource contained by this
 *            {@code ResourceContainer}.
 */
public abstract class ResourceContainer<T> {

    /**
     * The path associated with the resource.
     */
    private final String path;

    /**
     * The resource of type {@code T}. Lazily initialized upon the first call to
     * {@link #get()}.
     */
    private T resource;

    /**
     * Constructs a {@code ResourceContainer} with the specified resource path.
     *
     * @param path the path associated with the resource; must not be {@code null}.
     */
    protected ResourceContainer(String path) {
        this.path = path;
    }

    /**
     * Loads the resource of type {@code T}.
     * <p>
     * This method must be implemented by subclasses to define how the resource is
     * retrieved.
     *
     * @return the loaded resource of type {@code T}.
     */
    protected abstract T load();

    /**
     * Retrieves the resource contained in this {@code ResourceContainer}.
     * <p>
     * If the resource has not yet been loaded, this method will attempt to load it
     * using the {@link #load()} method. If loading fails or returns {@code null},
     * a {@link NullPointerException} is thrown.
     *
     * @return the resource of type {@code T}.
     * @throws NullPointerException if loading the resource fails or returns
     *                              {@code null}.
     */
    public T get() {
        if (resource == null) {
            try {
                resource = load();
            } catch (Exception err) {
                throw new NullPointerException("Error during loading: " + this.path);
            }
            if (resource == null) {
                throw new NullPointerException("Null was returned: " + this.path);
            }
        }
        return resource;
    }

    /**
     * Compares this {@code ResourceContainer} to another object for equality.
     *
     * @param o the object to compare to.
     * @return {@code true} if the specified object is equal to this
     *         {@code ResourceContainer};
     *         {@code false} otherwise.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ResourceContainer<?> that = (ResourceContainer<?>) o;
        return Objects.equals(path, that.path)
                && Objects.equals(resource, that.resource);
    }

    /**
     * Computes the hash code for this {@code ResourceContainer}.
     *
     * @return the hash code of this {@code ResourceContainer}.
     */
    @Override
    public int hashCode() {
        return path.hashCode();
    }

    /**
     * Returns the path associated with this {@code ResourceContainer}.
     *
     * @return the resource path.
     */
    public String getPath() {
        return path;
    }
}
