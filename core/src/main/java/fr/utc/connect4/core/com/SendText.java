package fr.utc.connect4.core.com;

import java.util.Objects;

public record SendText(String content) implements IClientMessage {
    public SendText {
        Objects.requireNonNull(content, "Content cannot be null");
    }
}
