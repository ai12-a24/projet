package fr.utc.connect4.core.data;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

import fr.utc.connect4.core.com.IPayload;

public class LightGame implements IPayload {

    private LightProfile playerCreator;
    private LightProfile player2;

    private UUID uuid;
    private GridFormat grid;
    private Color colorCreator;
    private boolean chatEnabled;

    private GameResult gameResult;

    public LightGame() {
        // Necessary for serialization
    }

    public LightGame(
            @JsonProperty("playerCreator") LightProfile pPlayerCreator,
            @JsonProperty("player2") LightProfile pPlayer2,
            @JsonProperty("uuid") UUID pUuid,
            @JsonProperty("grid") GridFormat pGridFormat,
            @JsonProperty("colorCreator") Color colorCreator,
            @JsonProperty("chatEnabled") boolean chatEnabled) {

        this.playerCreator = pPlayerCreator != null ? pPlayerCreator : new LightProfile();
        this.player2 = pPlayer2;
    
        this.uuid = pUuid != null ? pUuid : UUID.randomUUID();
        this.grid = pGridFormat != null ? pGridFormat : GridFormat.FORMAT_7X6;
    
        this.colorCreator = colorCreator != null ? colorCreator : Color.EMPTY;
        this.chatEnabled = chatEnabled;
    
        this.gameResult = GameResult.NO_RESULT;
    }

    public void setPlayer2(LightProfile player2) {
        this.player2 = player2;
    }

    /**
     * 
     * @return Returns the status of the game
     */
    public GameResult isFinished() {
        return gameResult;
    }

    /**
     * 
     * @param gameResult statut of the game
     * 
     */
    protected void setGameResult(GameResult gameResult) {
        this.gameResult = gameResult;
    }

    /**
     * 
     * @return Get the UUID of the game
     */
    public UUID getUuid() {
        return uuid;
    }

    /**
     * @return Get the game board format
     */
    public GridFormat getGrid() {
        return grid;
    }

    /**
     *
     * @return Get the color of the creator
     */
    public Color getColorCreator() {
        return colorCreator;
    }

    /**
     *
     * @return Get chatEnabled
     */
    public boolean isChatEnabled() {
        return chatEnabled;
    }

    /**
     * 
     * @return Get the profile of the creator of the game
     */
    public LightProfile getPlayerCreator() {
        return playerCreator;
    }

    /**
     * 
     * @return Get the profile of the second player of the game
     */
    public LightProfile getPlayer2() {
        return player2;
    }

    /**
     * 
     * @return Get the profile of the player who is not the creator of the game
     */
    public boolean equals(LightGame g) {
        return g.getUuid().equals(this.getUuid());
    }
}
