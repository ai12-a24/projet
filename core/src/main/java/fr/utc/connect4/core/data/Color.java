package fr.utc.connect4.core.data;

public enum Color {
    RED("Red"),
    YELLOW("Yellow"),
    EMPTY("Empty");

    private final String result;

    Color(String result) {
        this.result = result;
    }

    /**
     * 
     * @return Get the String equivalent of the enumerator
     */
    public String getResult() {
        return result;
    }
}
