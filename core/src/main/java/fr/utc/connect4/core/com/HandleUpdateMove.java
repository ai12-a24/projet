package fr.utc.connect4.core.com;

import fr.utc.connect4.core.data.Game;

import java.util.Objects;

public record HandleUpdateMove(Game game) implements IServerMessage {
    public HandleUpdateMove {
        Objects.requireNonNull(game, "Game cannot be null");
    }
}
