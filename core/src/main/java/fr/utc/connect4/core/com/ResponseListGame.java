package fr.utc.connect4.core.com;

import java.util.List;
import java.util.Objects;

import fr.utc.connect4.core.data.LightGame;

public record ResponseListGame(List<LightGame> games) implements IServerMessage {
    public ResponseListGame {
        Objects.requireNonNull(games, "Games cannot be null");
    }
}
