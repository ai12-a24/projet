package fr.utc.connect4.core.data;

public enum GameResult {
    WIN("Win"),
    LOSE("Lose"),
    DRAW("Draw"),
    NO_RESULT("No Result");

    private final String result;

    GameResult(String result) {
        this.result = result;
    }

    /**
     * 
     * @return Get the String equivalent of the enumerator
     */
    public String getResult() {
        return result;
    }
}
