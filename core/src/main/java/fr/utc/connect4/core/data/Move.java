package fr.utc.connect4.core.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import fr.utc.connect4.core.com.IPayload;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Move implements IPayload {

    private int moveNumber;
    private int column;
    private LightProfile player;
    private Color color;

    public Move() {
        // Nécessaire pour la sérialisation
    }

    public Move(int pMoveNumber, int pColumn, LightProfile pPlayer, Color pColor) {
        this.moveNumber = pMoveNumber;
        this.color = pColor;
        this.column = pColumn;
        this.player = pPlayer;
    }

    /**
     * 
     * @return Find out which column the move was made on
     */
    public int getColumn() {
        return column;
    }

    /**
     * 
     * @return Get the index of the action in an action list
     */
    public int getMoveNumber() {
        return moveNumber;
    }

    /**
     * 
     * @return Get the profile related to the move
     */
    public LightProfile getProfile() {
        return player;
    }

    /**
     * 
     * @return Get the color of the move
     */
    public Color getColor() {
        return color;
    }
}
