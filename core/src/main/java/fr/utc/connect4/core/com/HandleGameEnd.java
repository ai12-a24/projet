package fr.utc.connect4.core.com;

import fr.utc.connect4.core.data.GameResult;

public record HandleGameEnd(GameResult result) implements IServerMessage {

}