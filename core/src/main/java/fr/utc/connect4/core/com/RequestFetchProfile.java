package fr.utc.connect4.core.com;

import java.util.Objects;
import java.util.UUID;

public record RequestFetchProfile(UUID player)
        implements IClientMessage {
    public RequestFetchProfile {
        Objects.requireNonNull(player, "Player uuid cannot be null");
    }
}
