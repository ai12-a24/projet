package fr.utc.connect4.core.com;

import fr.utc.connect4.core.data.Game;
import fr.utc.connect4.core.data.LightProfile;

import java.util.Objects;
import java.util.UUID;

/**
 * Response from the server to the client when a player try to join a full game
 */
public record ResponsePlayerJoinsFullGame(UUID game) implements IServerMessage {
    public ResponsePlayerJoinsFullGame {
        Objects.requireNonNull(game, "Game cannot be null");
    }
}
