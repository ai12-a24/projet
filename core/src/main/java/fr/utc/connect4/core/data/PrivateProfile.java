package fr.utc.connect4.core.data;

import java.time.LocalDate;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true) // Ignore les champs JSON non mappés
public class PrivateProfile extends PublicProfile {

    private String hash;

    @JsonCreator
    public PrivateProfile(
            @JsonProperty("username") String username,
            @JsonProperty("uuid") UUID uuid,
            @JsonProperty("dateOfBirth") LocalDate dateOfBirth,
            @JsonProperty("first_name") String first_name,
            @JsonProperty("last_name") String last_name,
            @JsonProperty("hash") String hash) {
        super(username, uuid, dateOfBirth, first_name, last_name);
        this.hash = hash;
    }

    /**
     * 
     * @return Get the hashed password of the profile
     */
    public String getHash() {
        return hash;
    }

    /**
     * 
     * @return Set the hashed password of the profile
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * 
     * @return Get a secure profile simplification
     */
    public PublicProfile getPublicProfile() {
        return new PublicProfile(getUsername(), getUuid(), getDateOfBirth(), getFirstName(), getLastName());
    }

    /**
     * 
     * @return Returns a lightened class of the profile (LightProfile)
     */
    public LightProfile getLightProfile() {
        return new LightProfile(getUuid(), getUsername());
    }

    @Override
    public String toString() {
        return "PrivateProfile{" +
                "hash='" + hash + '\'' +
                ", dateOfBirth=" + getDateOfBirth() +
                ", first_name='" + getFirstName() + '\'' +
                ", last_name='" + getLastName() + '\'' +
                ", results=" + getResults() +
                '}';
    }
}