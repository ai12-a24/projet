package fr.utc.connect4.core.com;

import fr.utc.connect4.core.data.Color;
import fr.utc.connect4.core.data.Game;
import fr.utc.connect4.core.data.GridFormat;
import fr.utc.connect4.core.data.LightProfile;

import java.util.Objects;
import java.util.UUID;

public record RequestJoinGame(UUID game)
        implements IClientMessage {
    public RequestJoinGame {
        Objects.requireNonNull(game, "Game uuid cannot be null");
    }
}
