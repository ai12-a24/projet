package fr.utc.connect4.core.data;

import java.time.LocalDate;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PublicProfile extends LightProfile {
    private LocalDate dateOfBirth;
    private String firstName;
    private String lastName;
    private ArrayList<Result> results;

    public PublicProfile() {
        super();
    }

    @JsonCreator
    public PublicProfile(
            @JsonProperty("username") String username,
            @JsonProperty("uuid") UUID uuid,
            @JsonProperty("dateOfBirth") LocalDate dateOfBirth,
            @JsonProperty("first_name") String firstName,
            @JsonProperty("last_name") String lastName) {
        super(uuid, username);
        this.dateOfBirth = dateOfBirth;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * 
     * @return Get the profile's date of birth
     */
    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * 
     * @return Get the first name of the profile
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * 
     * @return Get the last name of the profile
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * 
     * @return Get the list of player's game results
     */
    public ArrayList<Result> getResults() {
        return results;
    }

    /**
     * Stores a date of birth for the profile
     * 
     * @param dateOfBirth New date of birth to save
     */
    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * Stores a first name for the profile
     * 
     * @param firstName New first name to save
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Stores a last name for the profile
     * 
     * @param lastName New last name to save
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * 
     * @return Returns a lightened class of the profile (LightProfile)
     */
    public LightProfile getLightProfile() {
        return new LightProfile(getUuid(), getUsername());
    }

}
