package fr.utc.connect4.core.data;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import fr.utc.connect4.core.com.IPayload;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LightProfile implements Serializable, IPayload {

    private final UUID uuid;

    private final String username;

    public LightProfile() {
        this.uuid = null;
        this.username = null;
        
    }
    @JsonCreator
    public LightProfile(
            @JsonProperty("uuid") UUID uuid,
            @JsonProperty("username") String username) {
        this.uuid = Objects.requireNonNull(uuid, "UUID of LightProfile was null");
        this.username = Objects.requireNonNull(username, "username of LightProfile was null");
    }

    @Override
    public String toString() {
        return "LightProfile(" + this.uuid + ", " + this.username + ")";
    }

    /**
     * 
     * @return Get the UUID of the profile
     */
    public UUID getUuid() {
        return uuid;
    }

    /**
     * 
     * @return Get the player's username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * 
     * @param o The profile to compare with
     * @return True if the two profiles are the same
     */
    @Override
    public boolean equals(Object o) {
        // TODO(Data/Morgan) : A VERIFIER SI LE EQUALS FONCTIONNE

        LightProfile myProfile = (LightProfile) o;
        return this.uuid.equals(myProfile.getUuid());
    }

    /**
     *
     * @param p1 The profile to compare with
     * @param p2 the second parameter to test
     * @return True if the two profiles have the same uuid
     */
    public static boolean equals(LightProfile p1, LightProfile p2) {

        return p1.getUuid().equals(p2.getUuid());

    }

}
