package fr.utc.connect4.core.data;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import fr.utc.connect4.core.com.IPayload;

@JsonIgnoreProperties(ignoreUnknown = true) // Ignore les champs JSON non mappés
public class Result implements Serializable, IPayload {
    private GameResult gridResult;
    private GridFormat gridFormat;

    public Result() {
        // Nécessaire pour la sérialisation
    }

    public Result(@JsonProperty("gridResult") GameResult gridResult,
            @JsonProperty("gridFormat") GridFormat gridFormat) {
        this.gridResult = gridResult;
        this.gridFormat = gridFormat;
    }

    public GridFormat getGridFormat() {
        return gridFormat;
    }

    public GameResult getGridResult() {
        return gridResult;
    }
}
