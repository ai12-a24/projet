package fr.utc.connect4.core.com;

import java.util.Objects;

import fr.utc.connect4.core.data.Game;

public record HandleLaunchGame(Game game) implements IServerMessage {
    public HandleLaunchGame {
        Objects.requireNonNull(game, "Game cannot be null");
    }
}
