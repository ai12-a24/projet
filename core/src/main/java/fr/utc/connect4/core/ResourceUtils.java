package fr.utc.connect4.core;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javafx.scene.text.Font;

/**
 * Utility class for resource management within the application.
 * Provides methods to access resources, load fonts, and retrieve file lists
 * from directories.
 * Handles both file system and JAR resource paths.
 */
public class ResourceUtils {

    /**
     * Indicates whether the application is running in debug mode.
     * The value is determined based on system properties containing "debug" set to
     * "true".
     */
    public static final boolean DEBUG = inDebugMode();

    /**
     * Determines if the application is running in debug mode.
     *
     * @return {@code true} if a system property contains "debug" set to "true",
     *         otherwise {@code false}.
     */
    private static boolean inDebugMode() {
        for (String name : System.getProperties().stringPropertyNames()) {
            if (name.toLowerCase().contains("debug") && "true".equalsIgnoreCase(System.getProperty(name))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Loads a core font from the specified path with the given size.
     *
     * @param size the font size to load.
     */
    public static void loadCoreFont(double size) {
        Font.loadFont(getResourceURL("/core/font/arial.ttf").toExternalForm(), size);
    }

    /**
     * Retrieves the URL of a resource file.
     *
     * @param file the path to the resource file.
     * @return the {@link URL} of the specified resource.
     * @throws IllegalArgumentException if the resource file cannot be found.
     */
    public static URL getResourceURL(String file) throws IllegalArgumentException {
        URL url = ResourceUtils.class.getResource(file);
        if (url == null) {
            throw new IllegalArgumentException("Can't find file " + file);
        }
        return url;
    }

    /**
     * Retrieves an input stream for a resource file.
     *
     * @param file the path to the resource file.
     * @return the {@link InputStream} of the specified resource.
     * @throws IllegalArgumentException if the resource file path is invalid.
     */
    public static InputStream getResourceAsStream(String file) {
        InputStream in = ResourceUtils.class.getResourceAsStream(file);
        if (in == null) {
            throw new IllegalArgumentException("Invalid path " + file);
        }
        return in;
    }

    /**
     * Retrieves the URI of a resource file.
     *
     * @param file the path to the resource file.
     * @return the {@link URI} of the specified resource.
     * @throws IllegalArgumentException if the resource file URL is invalid.
     */
    public static URI getResourceURI(String file) {
        URL url = getResourceURL(file);
        try {
            return url.toURI();
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Invalid URL : " + url);
        }
    }

    /**
     * Retrieves the string representation of a resource URI.
     *
     * @param file the path to the resource file.
     * @return the string representation of the resource URI.
     */
    public static String getResourceString(String file) {
        return getResourceURI(file).toString();
    }

    /**
     * Retrieves a list of resource file paths from a specified directory.
     *
     * @param directory the path to the resource directory.
     * @return a list of file paths within the specified directory.
     * @throws IOException if an I/O error occurs or no files are found in the
     *                     directory.
     */
    public static List<String> getResourceListDirectory(String directory) throws IOException {
        URI uri = getResourceURI(directory);
        List<String> paths = "jar".equals(uri.getScheme())
                ? getResourceFilesInJar(directory)
                : getResourceFilesInSrc(directory);
        if (paths.isEmpty()) {
            throw new FileNotFoundException("No sub items at " + directory);
        }
        return paths;
    }

    /**
     * Retrieves a list of resource file paths within a JAR file from the specified
     * directory.
     *
     * @param directory the path to the resource directory.
     * @return a list of file paths within the specified directory in the JAR.
     * @throws IOException if an I/O error occurs.
     */
    private static List<String> getResourceFilesInJar(String directory) throws IOException {
        List<String> filenames = new ArrayList<>();
        URI uri = getResourceURI(directory);
        try (FileSystem fileSystem = FileSystems.newFileSystem(uri, Collections.emptyMap())) {
            Path directoryPath = fileSystem.getPath(directory);
            try (Stream<Path> walk = Files.walk(directoryPath, 1)) {
                Iterator<Path> walkIterator = walk.iterator();
                walkIterator.next(); // Skip directory path
                while (walkIterator.hasNext()) {
                    String path = pathUnify(walkIterator.next());
                    filenames.add(path);
                }
            }
        }
        return filenames;
    }

    /**
     * Retrieves a list of resource file paths from a source directory.
     *
     * @param directory the path to the resource directory.
     * @return a list of file paths within the specified source directory.
     * @throws IOException if an I/O error occurs.
     */
    private static List<String> getResourceFilesInSrc(String directory) throws IOException {
        try(Stream<Path> paths = Files.list(Paths.get(getResourceURI(directory)))){
            return paths.map(p -> directory + "/" + p.getFileName())
            .collect(Collectors.toList());
        }
                
    }

    /**
     * Normalizes a file path to a unified format.
     *
     * @param path the file path to normalize.
     * @return the normalized file path as a string.
     */
    private static String pathUnify(String path) {
        return path
                .replace('\\', '/')
                .replaceAll("[\\\\/]$", "")
                .replaceAll("^file:/", "");
    }

    /**
     * Normalizes a file path to a unified format.
     *
     * @param path the {@link Path} object to normalize.
     * @return the normalized file path as a string.
     */
    private static String pathUnify(Path path) {
        return pathUnify(path.toString());
    }
}
