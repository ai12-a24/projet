package fr.utc.connect4.core.com;

import java.util.Objects;

import fr.utc.connect4.core.data.LightProfile;

public record HandleDelPlayer(LightProfile profile) implements IServerMessage {
    public HandleDelPlayer {
        Objects.requireNonNull(profile, "Profile cannot be null");
    }

}
