package fr.utc.connect4.core;

import javafx.scene.image.Image;

/**
 * The {@code ResourceImage} class represents a container for managing and
 * loading image resources. It extends {@code ResourceContainer<Image>} and
 * provides functionality for loading images from specified resource paths using
 * JavaFX's {@code Image} class.
 *
 * <p>
 * This class utilizes the {@code ResourceUtils} utility to resolve the resource
 * URL for the specified path and load the image.
 * </p>
 *
 * @see javafx.scene.image.Image
 * @see fr.utc.connect4.core.ResourceContainer
 * @see fr.utc.connect4.core.ResourceUtils
 */
public class ResourceImage extends ResourceContainer<Image> {

    /**
     * Constructs a new {@code ResourceImage} with the specified resource path.
     *
     * @param path the path to the resource to be loaded; must not be {@code null}.
     */
    public ResourceImage(String path) {
        super(path);
    }

    /**
     * Loads the image resource from the specified path.
     *
     * <p>
     * This method overrides the {@code load()} method in the
     * {@code ResourceContainer} class. It uses {@code ResourceUtils} to resolve the
     * URL for the resource and creates an {@code Image} instance from the URL.
     * </p>
     *
     * @return the loaded {@code Image} object.
     * @throws IllegalArgumentException if the resource path is invalid or the image
     *                                  cannot be loaded.
     */
    @Override
    protected Image load() {
        return new Image(ResourceUtils.getResourceURL(getPath()).toString());
    }
}
