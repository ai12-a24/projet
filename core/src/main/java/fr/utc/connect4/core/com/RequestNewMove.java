package fr.utc.connect4.core.com;

import java.util.Objects;
import java.util.UUID;

import fr.utc.connect4.core.data.Move;

public record RequestNewMove(Move move, UUID idGame) implements IClientMessage {
    public RequestNewMove {
        Objects.requireNonNull(move, "Move cannot be null");
        Objects.requireNonNull(idGame, "UUID game cannot be null");
    }

}
