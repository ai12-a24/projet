package fr.utc.connect4.core;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

/**
 * Represents a sound resource in the Connect4 application.
 * This class extends the {@link ResourceContainer} to manage {@link Media}
 * resources
 * and provides methods to play sounds, including handling background audio
 * playback.
 * 
 * <p>
 * Utilizes the JavaFX {@link MediaPlayer} for audio playback. This includes
 * methods for
 * starting, stopping, and managing audio with volume control.
 * </p>
 * 
 * @see Media
 * @see MediaPlayer
 */
public class ResourceSound extends ResourceContainer<Media> {

    /**
     * Logger instance for logging errors or debug information.
     */
    private static final Logger logger = LogManager.getLogger(ResourceSound.class);

    /**
     * A static {@link MediaPlayer} instance to manage background audio playback.
     */
    private static MediaPlayer backgroundPlayer = null;

    /**
     * Constructs a {@code ResourceSound} object with the specified file path.
     * 
     * @param path the path to the sound resource file.
     */
    public ResourceSound(String path) {
        super(path);
    }

    /**
     * Loads the sound resource as a {@link Media} object.
     * This method is called internally by the {@link ResourceContainer} base class.
     * 
     * @return the loaded {@link Media} object.
     */
    @Override
    protected Media load() {
        return new Media(ResourceUtils.getResourceString(getPath()));
    }

    /**
     * Plays the loaded sound resource once.
     * <p>
     * Creates a new {@link MediaPlayer} instance, sets the volume, and starts
     * playback
     * in a separate thread.
     * </p>
     */
    public void play() {
        Media media = get();
        MediaPlayer player = new MediaPlayer(media);
        player.setVolume(0.1D);
        new Thread(() -> {
            try {
                player.play();
            } catch (Exception e) {
                logger.error("An Error occurred during playing a sound", e);
            }
        }).start();
    }

    /**
     * Plays the loaded sound resource as background audio.
     * <p>
     * If a background track is already playing, it is stopped before starting the
     * new one.
     * This method uses a static {@link MediaPlayer} instance to manage the
     * background audio.
     * </p>
     */
    public void playBackground() {
        Media media = get();
        MediaPlayer player = new MediaPlayer(media);
        player.setVolume(0.1D);
        stopBackground();
        backgroundPlayer = player;
        new Thread(() -> {
            try {
                player.play();
            } catch (Exception e) {
                logger.error("An Error occurred during playing a sound", e);
            }
        }).start();
    }

    /**
     * Stops the currently playing background audio, if any.
     * <p>
     * This method is synchronized to prevent concurrent access to the static
     * {@code backgroundPlayer} instance.
     * </p>
     */
    public static synchronized void stopBackground() {
        if (backgroundPlayer != null) {
            backgroundPlayer.stop();
        }
        backgroundPlayer = null;
    }

}
