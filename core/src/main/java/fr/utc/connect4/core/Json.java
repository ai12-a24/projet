package fr.utc.connect4.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import fr.utc.connect4.core.com.IPayload;

/**
 * Utility class for handling JSON serialization and deserialization using the
 * Jackson library.
 * 
 * <p>
 * This class is designed to simplify JSON operations for objects that implement
 * the {@link IPayload} interface. It provides a singleton instance of
 * {@link ObjectMapper} configured with support for Java 8 date and time types.
 * </p>
 */
public class Json extends ObjectMapper {

    /**
     * Singleton instance of the {@code Json} utility class.
     */
    private static Json instance;

    /**
     * Private constructor to initialize the {@code ObjectMapper} with specific
     * settings.
     * 
     * <p>
     * Registers the {@link JavaTimeModule} to handle Java 8 date and time types and
     * disables writing dates as timestamps.
     * </p>
     */
    private Json() {
        super();
        this.registerModule(new JavaTimeModule());
        this.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    /**
     * Retrieves the singleton instance of the {@code Json} class.
     * 
     * <p>
     * If the instance does not exist, it is created.
     * </p>
     * 
     * @return the singleton instance of {@code Json}.
     */
    private static Json getInstance() {
        if (instance == null) {
            instance = new Json();
        }
        return instance;
    }

    /**
     * Serializes an {@link IPayload} object to its JSON representation.
     * 
     * @param payload the object to serialize.
     * @return the JSON string representation of the object.
     * @throws JsonProcessingException if the object cannot be serialized to JSON.
     */
    public static String dumps(IPayload payload) throws JsonProcessingException {
        return getInstance().writeValueAsString(payload);
    }

    /**
     * Deserializes a JSON string into an object of the specified type.
     * 
     * @param <T>     the type of the object to deserialize.
     * @param message the JSON string to deserialize.
     * @param klass   the class of the object to deserialize into.
     * @return the deserialized object.
     * @throws JsonProcessingException if the JSON string cannot be deserialized
     *                                 into the specified type.
     */
    public static <T> T loads(String message, Class<T> klass) throws JsonProcessingException {
        return getInstance().readValue(message, klass);
    }
}
