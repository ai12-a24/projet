package fr.utc.connect4.core.data;

public enum GridFormat {
    FORMAT_7X6("7x6", 6, 7),
    FORMAT_6X5("6x5", 5, 6),
    FORMAT_8X7("8x7", 7, 8),
    FORMAT_10X7("10x7", 7, 10);

    private final String result;
    private final int column;
    private final int row;

    GridFormat(String result, int row, int column) {
        this.result = result;
        this.column = column;
        this.row = row;
    }

    /**
     * 
     * @return Get the String equivalent of the enumerator.
     */
    public String getResult() {
        return result;
    }

    /**
     * 
     * @return Get the number of columns on the board
     */
    public int getColumn() {
        return column;
    }

    /**
     * 
     * @return Get the number of lines on the board
     */
    public int getRow() {
        return row;
    }

}
