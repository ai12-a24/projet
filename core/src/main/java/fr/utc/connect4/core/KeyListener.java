package fr.utc.connect4.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.function.Consumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.scene.Parent;
import javafx.scene.input.KeyCode;

/**
 * The KeyListener class handles key press and release events in a JavaFX
 * application. It allows registration of event handlers for specific keys and
 * supports querying whether a key is currently pressed. This class also manages
 * a clean-up mechanism to handle key release events when clearing the pressed
 * keys set.
 *
 * <p>
 * This class relies on JavaFX's {@link javafx.scene.input.KeyCode} for key
 * identification and makes use of functional interfaces for event handling.
 * </p>
 *
 * <p>
 * Example usage:
 * 
 * <pre>
 * Parent root = ...;
 * KeyListener keyListener = new KeyListener(root);
 * keyListener.listenPressed(KeyCode.A, () -> logger.info("Key A pressed"));
 * keyListener.listenReleased(KeyCode.A, () -> logger.info("Key A released"));
 * </pre>
 * </p>
 *
 * @see javafx.scene.input.KeyCode
 * @see javafx.scene.Parent
 */
public class KeyListener {

    /**
     * Logger instance for logging errors or debug information.
     */
    private static final Logger logger = LogManager.getLogger(KeyListener.class);

    /**
     * Map that associates {@link KeyCode} with a list of handlers for key press
     * events.
     */
    private final HashMap<KeyCode, ArrayList<Runnable>> handlersPressed;

    /**
     * Map that associates {@link KeyCode} with a list of handlers for key release
     * events.
     */
    private final HashMap<KeyCode, ArrayList<Runnable>> handlersReleased;

    /**
     * Set of currently pressed keys represented by {@link KeyCode}.
     */
    private HashSet<KeyCode> keysPressed;

    /**
     * Constructs a new {@code KeyListener} instance and initializes key event
     * handlers.
     *
     * @param root the {@link Parent} node to attach key event listeners.
     */
    public KeyListener(Parent root) {
        this.keysPressed = new HashSet<>();
        this.handlersPressed = new HashMap<>();
        this.handlersReleased = new HashMap<>();
        root.setOnKeyPressed(event -> {
            KeyCode code = event.getCode();
            if (!keysPressed.contains(code)) {
                keysPressed.add(code);
                callHandlers(code, handlersPressed);
            }
        });

        root.setOnKeyReleased(event -> {
            KeyCode code = event.getCode();
            if (keysPressed.remove(code)) {
                callHandlers(code, handlersReleased);
            }
        });
    }

    /**
     * Clears all currently pressed keys and invokes their associated release
     * handlers.
     */
    public void cleanKeys() {
        HashSet<KeyCode> keys = this.keysPressed;
        this.keysPressed = new HashSet<>();
        for (KeyCode key : keys) {
            callHandlers(key, handlersReleased);
        }
    }

    /**
     * Checks if the specified key is currently pressed.
     *
     * @param code the {@link KeyCode} to check.
     * @return {@code true} if the key is pressed, {@code false} otherwise.
     */
    public boolean isPressed(KeyCode code) {
        return keysPressed.contains(code);
    }

    /**
     * Invokes all handlers associated with the specified key and event type.
     *
     * @param code     the {@link KeyCode} for which handlers should be invoked.
     * @param handlers the map of event handlers by {@link KeyCode}.
     */
    private void callHandlers(KeyCode code, HashMap<KeyCode, ArrayList<Runnable>> handlers) {
        ArrayList<Runnable> handlersKey = handlers.get(code);
        if (handlersKey != null) {
            for (Runnable handler : handlersKey) {
                try {
                    handler.run();
                } catch (Exception err) {
                    logger.error("Exception occurred during key handling", err);
                }
            }
        }
    }

    /**
     * Registers a handler that is triggered on both press and release events of the
     * specified key.
     *
     * @param code    the {@link KeyCode} to listen to.
     * @param handler a {@link Consumer} that accepts {@code true} on key press and
     *                {@code false} on key release.
     */
    public void listen(KeyCode code, Consumer<Boolean> handler) {
        listenPressed(code, () -> handler.accept(true));
        listenReleased(code, () -> handler.accept(false));
    }

    /**
     * Registers a handler that is triggered when the specified key is pressed.
     *
     * @param code    the {@link KeyCode} to listen to.
     * @param handler a {@link Runnable} to execute when the key is pressed.
     */
    public void listenPressed(KeyCode code, Runnable handler) {
        handlersPressed.computeIfAbsent(code, _ -> new ArrayList<>()).add(handler);
    }

    /**
     * Registers a handler that is triggered when the specified key is released.
     *
     * @param code    the {@link KeyCode} to listen to.
     * @param handler a {@link Runnable} to execute when the key is released.
     */
    public void listenReleased(KeyCode code, Runnable handler) {
        handlersReleased.computeIfAbsent(code, _ -> new ArrayList<>()).add(handler);
    }
}