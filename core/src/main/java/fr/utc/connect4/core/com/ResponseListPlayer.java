package fr.utc.connect4.core.com;

import java.util.List;
import java.util.Objects;

import fr.utc.connect4.core.data.LightProfile;

public record ResponseListPlayer(List<LightProfile> profiles) implements IServerMessage {
    public ResponseListPlayer {
        Objects.requireNonNull(profiles, "Profiles cannot be null");
    }
}
