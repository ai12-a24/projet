package fr.utc.connect4.core.com;

import fr.utc.connect4.core.data.LightProfile;

import java.util.Objects;

public record RequestDelPlayer(LightProfile oldProfile) implements IClientMessage {
    public RequestDelPlayer {
        Objects.requireNonNull(oldProfile, "Profile cannot be null");
    }
}
