package fr.utc.connect4.core.data;

import java.security.Timestamp;
import java.sql.Time;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import fr.utc.connect4.core.com.IPayload;
import javafx.scene.effect.Light;

public class Message implements IPayload {

    private UUID uuid;
    private String text;
    private LightProfile sender;
    private String time;

    public Message() {
        this.uuid = UUID.randomUUID();

        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        String formattedTime = now.format(formatter);

        this.time = formattedTime;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setSender(LightProfile sender) {
        this.sender = sender;
    }

    public LightProfile getSender() {
        return sender;
    }

    public String getText() {
        return text;
    }

    public String getTime() {
        return time;
    }
}
