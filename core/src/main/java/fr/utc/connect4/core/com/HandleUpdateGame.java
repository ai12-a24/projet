package fr.utc.connect4.core.com;

import java.util.Objects;

import fr.utc.connect4.core.data.LightGame;

public record HandleUpdateGame(LightGame game) implements IServerMessage {
    public HandleUpdateGame {
        Objects.requireNonNull(game, "Game cannot be null");
    }
}
