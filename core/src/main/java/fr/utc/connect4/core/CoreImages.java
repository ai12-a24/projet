package fr.utc.connect4.core;

/**
 * The {@code CoreImages} class provides a centralized repository of
 * predefined {@link ResourceImage} instances for core application imagery.
 * 
 * <p>
 * These images are commonly used across the application to represent
 * various states or provide user interface elements such as icons and error
 * indicators.
 * </p>
 * 
 * <p>
 * This class contains only static members and is not meant to be instantiated.
 * </p>
 */
public class CoreImages {

    /**
     * Represents an error image resource used to indicate an error state.
     * 
     * <p>
     * This image is located at {@code /core/image/error.png}.
     * </p>
     * 
     * @see ResourceImage
     */
    public static final ResourceImage ERROR = new ResourceImage("/core/image/error.png");

    /**
     * Represents an icon image resource commonly used for application branding
     * or user interface elements.
     * 
     * <p>
     * This image is located at {@code /core/image/icon.png}.
     * </p>
     * 
     * @see ResourceImage
     */
    public static final ResourceImage ICON = new ResourceImage("/core/image/icon.png");
}
