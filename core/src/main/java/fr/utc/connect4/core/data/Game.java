package fr.utc.connect4.core.data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Game extends LightGame {

    /**
     * ArrayList chosen because of utilities fonction
     */
    private List<LightProfile> listSpectators;
    private List<Move> listMove;

    private ArrayList<Message> listMessage;

    private Color colorj2;
    private List<List<Color>> board;

    @JsonCreator
    public Game(
            @JsonProperty("lightGame") LightGame lightGame,
            @JsonProperty("playerCreator") LightProfile pPlayerCreator,
            @JsonProperty("player2") LightProfile pPlayer2,
            @JsonProperty("uuid") UUID pUuid,
            @JsonProperty("grid") GridFormat pGridFormat,
            @JsonProperty("colorCreator") Color pColorCreator,
            @JsonProperty("colorj2") Color pColorj2,
            @JsonProperty("chatEnabled") boolean chatEnabled,
            @JsonProperty("spectators") List<LightProfile> spectators,
            @JsonProperty("moves") List<Move> moves,
            @JsonProperty("messages") List<Message> messages,
            @JsonProperty("board") List<List<Color>> board) {

        super(pPlayerCreator, pPlayer2, pUuid, pGridFormat, pColorCreator, chatEnabled);

        this.listSpectators = spectators != null ? spectators : new ArrayList<>();
        this.listMove = moves != null ? moves : new ArrayList<>();
        this.listMessage = messages != null ? (ArrayList<Message>) messages : new ArrayList<>();

        this.board = board != null ? board : createBoard(pGridFormat);
        this.colorj2 = pColorj2;
    }
    // TODO CONSTUCTOR WITH ALL ARGS

    /**
     * Create a game board according to a format
     * 
     * @param grid Game board format
     * @return Table representing the game board
     */
    private List<List<Color>> createBoard(GridFormat grid) {

        int maxRow = grid.getRow();
        int maxColumn = grid.getColumn();

        List<List<Color>> lRow = new ArrayList<>(maxColumn);

        for (int i = 0; i < maxColumn; i++) {
            List<Color> lColumn = new ArrayList<>(maxRow);
            for (int j = 0; j < maxRow; j++) {
                lColumn.add(Color.EMPTY);
            }
            lRow.add(lColumn);
        }

        return lRow;

    }

    /**
     * 
     * Return if the game is over and which player is the winner
     *
     * @return The current result of the game
     */
    public void calculResult() {

        if (isFinished() != GameResult.NO_RESULT) {
            return;
        }

        int i;
        int j;

        final int col = getGrid().getColumn();
        final int row = getGrid().getRow();

        boolean flag = false;

        // checks horizontal win
        for (i = 0; i < col; i++) {
            for (j = 0; j < row - 3; j++) {
                if (board.get(i).get(j) == Color.EMPTY) {
                    flag = true;
                    continue;
                }

                if (board.get(i).get(j) == board.get(i).get(j + 1)
                        && board.get(i).get(j) == board.get(i).get(j + 2)
                        && board.get(i).get(j) == board.get(i).get(j + 3)) {

                    if (board.get(i).get(j) == this.getColorCreator()) {
                        setGameResult(GameResult.WIN);
                        return;
                    }
                    setGameResult(GameResult.LOSE);
                    return;
                }
            }
        }

        // checks vertical win
        for (i = 0; i < col - 3; i++) {
            for (j = 0; j < row; j++) {
                if (board.get(i).get(j) == Color.EMPTY) {
                    flag = true;
                    continue;
                }

                if (board.get(i).get(j) == board.get(i + 1).get(j)
                        && board.get(i).get(j) == board.get(i + 2).get(j)
                        && board.get(i).get(j) == board.get(i + 3).get(j)) {

                    if (board.get(i).get(j) == this.getColorCreator()) {
                        setGameResult(GameResult.WIN);
                        return;
                    }
                    setGameResult(GameResult.LOSE);
                    return;
                }
            }
        }

        // checks right-up diagonal win
        for (i = 0; i < col - 3; i++) {
            for (j = 0; j < row - 3; j++) {
                if (board.get(i).get(j) == Color.EMPTY) {
                    flag = true;
                    continue;
                }

                if (board.get(i).get(j) == board.get(i + 1).get(j + 1)
                        && board.get(i).get(j) == board.get(i + 2).get(j + 2)
                        && board.get(i).get(j) == board.get(i + 3).get(j + 3)) {

                    if (board.get(i).get(j) == this.getColorCreator()) {
                        setGameResult(GameResult.WIN);
                        return;
                    }
                    setGameResult(GameResult.LOSE);
                    return;
                }
            }
        }

        // checks right-down diagonal win
        for (i = 0; i < col - 3; i++) {
            for (j = 3; j < row; j++) {

                if (board.get(i).get(j) == Color.EMPTY) {
                    flag = true;
                    continue;
                }

                if (board.get(i).get(j) == board.get(i + 1).get(j - 1)
                        && board.get(i).get(j) == board.get(i + 2).get(j - 2)
                        && board.get(i).get(j) == board.get(i + 3).get(j - 3)) {

                    if (board.get(i).get(j) == this.getColorCreator()) {
                        setGameResult(GameResult.WIN);
                        return;
                    }
                    setGameResult(GameResult.LOSE);
                    return;
                }
            }
        }

        if (flag) {
            setGameResult(GameResult.NO_RESULT);
        } else {
            setGameResult(GameResult.DRAW);
        }
    }

    // GET

    /**
     * 
     * @return Returns a lightened class of the game (LightGame)
     */
    public LightGame getLightGame() {
        LightGame lg = new LightGame(this.getPlayerCreator(), this.getPlayer2(), this.getUuid(), this.getGrid(),
                this.getColorCreator(), this.isChatEnabled());

        lg.setGameResult(isFinished());
        return lg;
    }

    /**
     * 
     * @return Returns the second player's color
     */
    public Color getColorJ2() {
        return colorj2;
    }

    public void setColorJ2(Color colorj2) {
        this.colorj2 = colorj2;
    }

    /**
     * 
     * @return Get the list of moves in the game
     */
    public List<Move> getMoves() {
        return listMove;
    }

    /**
     * 
     * @return Gets the list of messages sent in the game
     */
    public List<Message> getMessages() {
        return listMessage;
    }

    /**
     * 
     * @return Get the list of spectators in the game.
     */
    public List<LightProfile> getSpectators() {
        return listSpectators;
    }

    /**
     * 
     * @return Get the game board
     */
    public List<List<Color>> getBoard() {
        return board;
    }

    // ADD

    /**
     * Add an action to the list then check if a player has won the game
     * 
     * @param move Action to store
     * @exception IndexOutOfBoundsException The action cannot be stored/is outside
     *                                      the boundaries of the game board
     * @exception IllegalStateException     The game is defined as finished
     */
    public void addMove(Move move) {

        if (isFinished() != GameResult.NO_RESULT) {
            throw new IllegalStateException("The game is already finished and can't be continued");
        }
        if (this.listMove.isEmpty()) {
            if (move.getColor() != this.getColorCreator()) {
                throw new IllegalStateException("It's not your turn");
            }
        } else {
            Color playerTurnColor = this.listMove.getLast().getColor() == Color.RED ? Color.YELLOW : Color.RED;

            if (move.getColor() != playerTurnColor) {
                throw new IllegalStateException("It's not your turn");
            }
        }

        List<Color> l = board.get(move.getColumn());
        int i = 0;
        while (i < l.size() && l.get(i) != Color.EMPTY) { // Move up the column until you find an unoccupied square OR
                                                          // Until reaching the end of the column
            i++;
        }

        if (i != l.size()) { // If an available box was found
            l.set(i, move.getColor());
        } else { // If no available box was found
            throw new IndexOutOfBoundsException("The column " + move.getColumn() + " is already full");
        }

        listMove.add(move);

    }

    /**
     * Add a spectator
     * 
     * @param profile Spectator to add
     */
    public void addSpectator(LightProfile profile) {
        listSpectators.add(profile);
    }

    /**
     * Add a message
     * 
     * @param message Message to be added
     */
    public void addMessage(Message message) {
        listMessage.add(message);
    }

    /**
     * Remove a spectator
     * 
     * @param profile Spectator to remove
     */
    public void removeSpectator(LightProfile profile) {
        listSpectators.remove(profile);
    }

    /**
     * Remove a player
     * 
     * @param profile Player to remove
     */
    public void removePlayer(LightProfile profile) {
        if (getPlayer2().equals(profile)) {
            setGameResult(GameResult.WIN);
        } else if (getPlayerCreator().equals(profile)) {
            setGameResult(GameResult.LOSE);
        }
    }
}
