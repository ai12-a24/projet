package fr.utc.connect4.core.com;

import java.util.Objects;

import fr.utc.connect4.core.data.PublicProfile;

public record SendLogin(PublicProfile login) implements IClientMessage {
    public SendLogin {
        Objects.requireNonNull(login, "Profiles cannot be null");
    }
}
