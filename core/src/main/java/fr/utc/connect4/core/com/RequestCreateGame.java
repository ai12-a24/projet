package fr.utc.connect4.core.com;

import java.util.Objects;

import fr.utc.connect4.core.data.LightGame;

public record RequestCreateGame(LightGame game)
        implements IClientMessage {
    public RequestCreateGame {
        Objects.requireNonNull(game, "Game cannot be null");
    }
}
