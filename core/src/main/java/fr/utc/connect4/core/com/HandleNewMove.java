package fr.utc.connect4.core.com;
import fr.utc.connect4.core.data.Move;

public record HandleNewMove(Move move) implements IServerMessage { 

}
