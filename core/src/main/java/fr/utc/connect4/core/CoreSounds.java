package fr.utc.connect4.core;

/**
 * The {@code CoreSounds} class serves as a container for core sound resources
 * used in the application. It provides predefined {@link ResourceSound} objects
 * for commonly used audio effects.
 * 
 * <p>
 * This class contains only static members and is intended to be used as a
 * utility class. As such, it should not be instantiated.
 * </p>
 */
public class CoreSounds {

    /**
     * A {@link ResourceSound} representing the sound effect for a water drop.
     * This sound is located at the resource path "/core/audio/water_drop.mp3".
     */
    public static final ResourceSound DEMO = new ResourceSound("/core/audio/water_drop.mp3");

}