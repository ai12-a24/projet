package fr.utc.connect4.core.com;

import fr.utc.connect4.core.data.PublicProfile;

import java.util.List;
import java.util.Objects;

public record ResponseFetchProfile(PublicProfile profile) implements IServerMessage {
    public ResponseFetchProfile {
        Objects.requireNonNull(profile, "Profile cannot be null");
    }
}
