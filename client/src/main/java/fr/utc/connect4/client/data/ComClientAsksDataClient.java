package fr.utc.connect4.client.data;

import java.util.Iterator;
import java.util.List;

import fr.utc.connect4.client.interfaces.IComClientAsksDataClient;
import fr.utc.connect4.core.data.Game;
import fr.utc.connect4.core.data.LightGame;
import fr.utc.connect4.core.data.LightProfile;
import fr.utc.connect4.core.data.Move;

public class ComClientAsksDataClient implements IComClientAsksDataClient {
    DataClientCore dataCore;

    public ComClientAsksDataClient(DataClientCore dataCore) {
        this.dataCore = dataCore;
    }

    @Override
    public void addUserToList(LightProfile user) {
        dataCore.addUser(user);
        dataCore.getDataClientAsksMain().updateDisplay(dataCore.getUsersOnline());
    }

    @Override
    public void removeUserToList(LightProfile user) {
        dataCore.removeUser(user);
        dataCore.getDataClientAsksMain().updateDisplay(dataCore.getUsersOnline());
    }

    @Override
    public void updateGamesList(List<LightGame> games) {
        dataCore.setGames(games);
        dataCore.getDataClientAsksMain().updateGamesDisplay(games);
    }

    @Override
    public void addAllUsersToList(List<LightProfile> users) {
        dataCore.addAllUsers(users);
        dataCore.getDataClientAsksMain().updateDisplay(dataCore.getUsersOnline());
    }

    @Override
    public void addNewGame(LightGame game) {
        dataCore.getGames().add(game);
        this.dataCore.getDataClientAsksMain().updateGamesDisplay(dataCore.getGames());
    }

    @Override
    public void addPlayedMoveToCurrentGame(Move move) {
        dataCore.addMove(move);
    }

    @Override
    public void updateGameAddPlayer2(LightGame game) {
        List<LightGame> myGameList = this.dataCore.getGames();

        boolean notFound = true;

        Iterator<LightGame> iterator = myGameList.iterator();
        while (iterator.hasNext() && notFound) {
            LightGame g = iterator.next();
            if (g.equals(game)) {
                iterator.remove();
                myGameList.add(game);
                notFound = false;
            }

        }
        this.dataCore.getDataClientAsksMain().updateGamesDisplay(myGameList);
    }

    @Override
    public void startGame(Game game) {
        this.dataCore.setGame(game);
        this.dataCore.getDataClientAsksGame().startGame(game);
    }

    /**
     * Update the game with the new move
     *
     * @param game
     */
    public void updateMove(Game game) {
        dataCore.setGame(game);
    }

}
