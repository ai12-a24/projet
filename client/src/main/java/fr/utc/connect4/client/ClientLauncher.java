package fr.utc.connect4.client;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The {@code ClientLauncher} class is responsible for launching the client
 * application by invoking the {@code ClientApp.run()} method. It also handles
 * any exceptions that may occur during the application's startup process and
 * logs the errors using Apache Log4j.
 */
public class ClientLauncher {

    /**
     * Logger instance used for logging application events, warnings, and errors.
     */
    private static final Logger logger = LogManager.getLogger(ClientLauncher.class);

    /**
     * The main method serves as the entry point for the application. It attempts to
     * launch the {@code ClientApp.run()} method and logs any exceptions that occur.
     * 
     * @param args Command-line arguments passed to the application at runtime.
     *             This parameter is unused in the current implementation.
     */
    @SuppressWarnings("all")
    public static void main(String[] args) {
        try {
            // Attempt to start the client application
            ClientApp.run();
        } catch (Exception err) {
            // Log any exceptions that occur during the startup
            logger.error("An error occurred: ", err);
        }
    }
}
