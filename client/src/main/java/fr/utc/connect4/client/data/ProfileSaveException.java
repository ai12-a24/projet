package fr.utc.connect4.client.data;

public class ProfileSaveException extends Exception {
    public ProfileSaveException(String message) {
        super(message);
    }

    public ProfileSaveException(String message, Throwable cause) {
        super(message, cause);
    }
}
