package fr.utc.connect4.client.main;

import fr.utc.connect4.client.data.ProfileSaveException;
import fr.utc.connect4.core.data.PrivateProfile;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Optional;
import static fr.utc.connect4.client.main.LoginViewController.hashPassword;

/**
 * Controller class for editing user profiles.
 */
public class EditProfileController {
    @FXML
    private TextField pseudoField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private TextField firstNameField;
    @FXML
    private TextField lastNameField;
    @FXML
    private TextField birthDateField;
    @FXML
    private ComboBox<String> avatarComboBox;

    private MainCore mainCore;
    private PrivateProfile currentProfile;
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    /**
     * Sets the main core instance and loads the current profile.
     *
     * @param mainCore the main core instance
     */
    public void setMainCore(MainCore mainCore) {
        this.mainCore = mainCore;
        loadCurrentProfile();
    }

    /**
     * Initializes the controller. Sets up avatar options and adds a listener for
     * date format validation.
     */
    @FXML
    public void initialize() {
        // Initialize avatar options
        avatarComboBox.getItems().addAll("Mario", "Luigi", "Peach", "Yoshi", "Toad");

        // Add listener for date format validation
        birthDateField.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                if (!newValue.isEmpty()) {
                    LocalDate.parse(newValue, formatter);
                    birthDateField.setStyle("");
                }
            } catch (DateTimeParseException e) {
                birthDateField.setStyle("-fx-border-color: red;");
            }
        });
    }

    /**
     * Loads the current profile data into the form fields.
     */
    private void loadCurrentProfile() {
        currentProfile = mainCore.getMainAsksDataClient().getMyPrivateProfile();

        // Populate fields with current data
        pseudoField.setText(currentProfile.getUsername());
        firstNameField.setText(currentProfile.getFirstName());
        lastNameField.setText(currentProfile.getLastName());
        birthDateField.setText(currentProfile.getDateOfBirth().format(formatter));
        avatarComboBox.setValue("Mario"); // TODO get from profile
    }

    /**
     * Handles the import profile action. Currently not implemented.
     */
    @FXML
    private void importProfile() {
        // TODO: Implement profile import functionality
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Import Profile");
        alert.setHeaderText(null);
        alert.setContentText("Profile import functionality not yet implemented.");
        alert.showAndWait();
    }

    /**
     * Handles the save changes action. Validates the input data and updates the
     * profile.
     */
    @FXML
    private void saveChanges() {
        try {
            // Validate date format
            LocalDate birthDate = LocalDate.parse(birthDateField.getText(), formatter);

            // Create updated profile
            PrivateProfile updatedProfile = new PrivateProfile(
                    pseudoField.getText(),
                    currentProfile.getUuid(),
                    birthDate,
                    firstNameField.getText(),
                    lastNameField.getText(),
                    passwordField.getText().isEmpty() ? currentProfile.getHash()
                            : hashPassword(passwordField.getText()));

            // Update profile through MainCore
            mainCore.getMainAsksDataClient().setUserData(updatedProfile);

            // Show success message and confirmation dialog
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Profil modifié");
            alert.setHeaderText("Profil modifié avec succès");
            alert.setContentText("Veuillez vous reconnecter pour appliquer les modifications.");

            // Wait for user to acknowledge
            Optional<ButtonType> result = alert.showAndWait();

            if (result.isPresent()) {
                // Close the edit profile window
                Stage editProfileStage = (Stage) pseudoField.getScene().getWindow();
                editProfileStage.close();

                // Get the main stage and close it (logout)
                Stage mainStage = mainCore.getMainStage();
                mainCore.getMainAsksComClient().logout(this.currentProfile);
            }

        } catch (DateTimeParseException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Invalid Date Format");
            alert.setHeaderText(null);
            alert.setContentText("Please enter the date in DD/MM/YYYY format.");
            alert.showAndWait();
        } catch (ProfileSaveException e) {
            throw new RuntimeException(e);
        }
    }
}