package fr.utc.connect4.client.interfaces;

import java.util.UUID;

import fr.utc.connect4.client.game.GameController;

public interface IGameCore {

    public void setGameAsksMain(IGameAsksMain gameAsksMain);

    public IMainAsksGame getMainAsksGame();

    public IGameAsksMain getGameAsksMain();

    public void setGameAsksDataClient(IGameAsksDataClient gameAsksDataClient);

    public IDataClientAsksGame getDataClientAsksGame();

    public IGameAsksDataClient getGameAsksDataClient();

    public void setGameAsksComClient(IGameAsksComClient gameAsksComClient);

    public IComClientAsksGame getComClientAsksGame();

    public GameController getGameController();

    public IGameAsksComClient getGameAsksComClient();

}
