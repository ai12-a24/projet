package fr.utc.connect4.client.main;

import fr.utc.connect4.client.interfaces.IMainAsksDataClient;
import fr.utc.connect4.core.data.PrivateProfile;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.UUID;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.nio.charset.StandardCharsets;

/**
 * Controller class for the Create Profile view.
 * Handles user interactions and profile creation logic.
 */
public class CreateProfileController implements Initializable {
    @FXML 
    private TextField usernameField;
    @FXML 
    private PasswordField passwordField;
    @FXML 
    private TextField firstNameField;
    @FXML 
    private TextField lastNameField;
    @FXML 
    private DatePicker birthDatePicker;
    @FXML 
    private ComboBox<String> avatarComboBox;

    @FXML 
    private Button importButton;
    @FXML 
    private Button createButton;

    public void setupHandlers() {
        importButton.setOnAction(event -> handleImportProfile());
        createButton.setOnAction(event -> handleCreateProfile());
    }

    private CreateProfileCore core;

    public CreateProfileController(CreateProfileCore core) {
        this.core = core;
    }

    /**
     * Initializes the controller class.
     * Sets up initial values and configurations for UI components.
     *
     * @param url The location used to resolve relative paths for the root object, or null if the location is not known.
     * @param rb The resources used to localize the root object, or null if the root object was not localized.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Initialize avatar options
        avatarComboBox.getItems().addAll(
                "Mario",
                "Luigi",
                "Yoshi",
                "Toad",
                "Peach"
        );
        avatarComboBox.setValue("Mario");

        // Set up date picker restrictions
        birthDatePicker.setValue(LocalDate.now());
    }

    /**
     * Hashes the given password using SHA-256.
     *
     * @param password The password to be hashed.
     * @return The hashed password as a hexadecimal string.
     */
    private String hashPassword(String password) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] encodedHash = digest.digest(
                    password.getBytes(StandardCharsets.UTF_8));

            StringBuilder hexString = new StringBuilder();
            for (byte b : encodedHash) {
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Failed to hash password", e);
        }
    }

    /**
     * Handles the import profile action.
     * Currently unimplemented.
     */
    @FXML
    private void handleImportProfile() {
        // Handle profile import
        showAlert(Alert.AlertType.INFORMATION, "Unimplemented", "Profile import is not yet implemented");
        throw new UnsupportedOperationException("Unimplemented");
    }

    /**
     * Handles the create profile action.
     * Creates a new profile with the provided user information.
     */
    @FXML
    private void handleCreateProfile() {
        try {
            // Validate inputs
            if (!validateInputs()) {
                return;
            }
            String passwordHash = hashPassword(passwordField.getText());
            PrivateProfile newProfile = new PrivateProfile(
                    usernameField.getText(),
                    UUID.randomUUID(),
                    birthDatePicker.getValue(),
                    firstNameField.getText(),
                    lastNameField.getText(),
                    passwordHash
            );

            // Save profile
            core.createProfile(newProfile);

             showAlert(Alert.AlertType.INFORMATION, "Profile Created",
                    "Profile successfully created for " + newProfile.getUsername());

            // Close the dialog
             Stage stage = (Stage) usernameField.getScene().getWindow();
             stage.close();

        } catch (Exception e) {
            showAlert(Alert.AlertType.ERROR, "Error",
                    "Failed to create profile: " + e.getMessage());
        }
    }

    /**
     * Validates the user inputs for profile creation.
     *
     * @return true if all inputs are valid, false otherwise.
     */
    private boolean validateInputs() {
        if (usernameField.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "Validation Error", "Username is required");
            return false;
        }
        if (passwordField.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "Validation Error", "Password is required");
            return false;
        }
        if (birthDatePicker.getValue() == null || birthDatePicker.getValue().isAfter(LocalDate.now())) {
            showAlert(Alert.AlertType.ERROR, "Validation Error", "Please enter a valid birth date");
            return false;
        }
        if (firstNameField.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "Validation Error", "First name is required");
            return false;
        }
        if (lastNameField.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "Validation Error", "Last name is required");
            return false;
        }
        return true;
    }

    /**
     * Displays an alert dialog with the specified type, title, and content.
     *
     * @param type The type of alert.
     * @param title The title of the alert.
     * @param content The content of the alert.
     */
    private void showAlert(Alert.AlertType type, String title, String content) {
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(content);
        alert.showAndWait();
    }
}