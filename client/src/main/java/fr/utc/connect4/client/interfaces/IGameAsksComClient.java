package fr.utc.connect4.client.interfaces;
import fr.utc.connect4.core.data.Move;
import java.util.UUID;

public interface IGameAsksComClient {
    void sendMove(Move move, UUID gameID);
}