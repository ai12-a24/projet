package fr.utc.connect4.client.game;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.utc.connect4.core.KeyListener;
import fr.utc.connect4.core.data.Game;
import fr.utc.connect4.core.data.GameResult;
import fr.utc.connect4.core.data.GridFormat;
import fr.utc.connect4.core.data.LightGame;
import fr.utc.connect4.core.data.LightProfile;
import fr.utc.connect4.core.data.Message;
import fr.utc.connect4.core.data.Move;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

/**
 * The {@code GameController} class manages the game logic and user interface
 * interactions for a Connect4 game. It is responsible for initializing the
 * game,
 * handling player actions, updating the game grid, managing chat messages, and
 * controlling game state transitions.
 */
public class GameController implements Initializable {

    /**
     * Logger instance for logging messages within the class.
     */
    private static final Logger logger = LogManager.getLogger(GameController.class);

    private GameCore gameCore; // Reference to GameCore for game logic

    /**
     * Constructs a new GameController instance.
     * 
     * @param gameCore the game core instance to handle game logic
     */
    public GameController(GameCore gameCore) {
        this.gameCore = gameCore; // Conserve une référence à GameCore
    }

    private GridPane gameGrid; // Game grid for displaying the board
    private boolean isGameStarted = false; // Flag to check if the game has started

    @FXML
    private BorderPane borderPane; // Main layout of the game

    @FXML
    private StackPane gamePane; // StackPane for layering game elements

    @FXML
    private GridPane gameGridPane; // GridPane to display the game grid

    @FXML
    private Label playerTurnLabel;

    @FXML
    private Label timerLabel;

    @FXML
    private Label playerNameLabel; // Label to display the player's name

    @FXML
    private Label playerLabel; // Label to display the player's name

    @FXML
    private Label userLabel; // Label to display the player's name

    @FXML
    private Label playerColorLabel; // Label to display the player's color

    @FXML
    private TextArea waitingTextArea; // TextArea for displaying waiting messages

    @FXML
    private TextField chatTextField; // TextField for entering chat messages

    @FXML
    private Button sendButton; // Button to send a message in the chat

    @FXML
    private Button btnQuitter; // Button to quit the game

    @FXML
    private VBox usersVBox; // VBox to display players and spectators

    @FXML
    private HBox topBox; // Référence au HBox

    private LightProfile player1; // Reference to player 1
    private LightProfile player2; // Reference to player 2

    private Game currentGame;

    private LightProfile currentUser;

    /**
     * Initializes the controller. This method is called after the FXML
     * components are loaded. It sets up the key listener and button actions.
     *
     * @param location  the location used to resolve relative paths
     * @param resources the resources used to localize the root object
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        new KeyListener(borderPane);

        // Quit button event
        btnQuitter.setOnAction(event -> showQuitConfirmation(currentGame));

        // Add action to the send button
        sendButton.setOnAction(event -> sendMessage());
    }

    /**
     * Initializes the game based on the parameters provided by the creator.
     * It sets up the grid and displays a waiting message.
     */
    @FXML
    public void initGame(LightGame lightGame) {
        logger.info("Initiating the game for a creator...");
        logger.info("Creator: {}", lightGame.getPlayerCreator().getUsername());
        logger.info("Grid dimensions: {}", lightGame.getGrid());
        logger.info("Creator's color: {}", lightGame.getColorCreator().toString());

        // Create the board
        gameGrid = createGameGrid(lightGame.getGrid());
        gamePane.getChildren().clear();
        gamePane.getChildren().add(gameGrid);

        // Add to the list of games here
        showWaitingMessage();
    }

    /**
     * Initializes the game based on the parameters provided by the creator.
     * It sets up the grid and displays a waiting message.
     */
    @FXML
    public void initGameForPlayer2(LightGame lightGame) {
        logger.info("A second player joined this game: ");
        logger.info("Grid dimensions: {}" + lightGame.getGrid());

        // Create the board
        gameGrid = createGameGrid(lightGame.getGrid());
        gamePane.getChildren().clear();
        gamePane.getChildren().add(gameGrid);

    }

    /**
     * Starts the game by initializing both players and setting up the game
     * state. The first player is the creator, and the second player joins
     * the game. The colors of the players are assigned, and the game is
     * transitioned into a started state.
     *
     * @param game
     * @return the actual game instance
     */
    @FXML

    public void startGame(Game game) {
        currentGame = game;
        // Initialize player1 and player2
        player1 = game.getPlayerCreator();
        player2 = game.getPlayer2();

        logger.info("Starting the game...");
        logger.info("Player 1: {}", player1);
        logger.info("Player 2: {}", player2);

        // Create the board
        gameGrid = createGameGrid(game.getGrid());
        gamePane.getChildren().clear();
        gamePane.getChildren().add(gameGrid);

        clearWaitingMessage();

        // Show players and spectators in the panel
        displayUserList(game);

        // Change the game state
        isGameStarted = true;

        sendMessageToUsers(player1.getUsername() + " doit commencer à jouer");
        updatePlayerState(currentGame);
    }

    /**
     * Creates the game grid based on the specified grid format.
     *
     * @param gridFormat the grid format (e.g., 7x6, 6x5)
     * @return the {@code GridPane} representing the game board
     */

    public GridPane createGameGrid(GridFormat gridFormat) {
        int rows = 0;
        int cols = 0;

        switch (gridFormat) {
            case FORMAT_7X6 -> {
                rows = 6;
                cols = 7;
            }
            case FORMAT_6X5 -> {
                rows = 5;
                cols = 6;
            }
            case FORMAT_8X7 -> {
                rows = 7;
                cols = 8;
            }
            case FORMAT_10X7 -> {
                rows = 7;
                cols = 10;
            }
        }

        GridPane grid = new GridPane();

        grid.setHgap(1.0);
        grid.setVgap(1.0);
        grid.setStyle("-fx-background-color: #424772;");

        // Column and Row Constraints
        for (int i = 0; i < cols; i++) {
            ColumnConstraints column = new ColumnConstraints();
            column.setHgrow(Priority.SOMETIMES);
            column.setMinWidth(10.0);
            column.setPrefWidth(100.0);
            grid.getColumnConstraints().add(column);
        }

        for (int i = 0; i < rows; i++) {
            RowConstraints row = new RowConstraints();
            row.setVgrow(Priority.SOMETIMES);
            row.setMinHeight(10.0);
            row.setPrefHeight(30.0);
            grid.getRowConstraints().add(row);
        }

        // Add Circles
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                Circle circle = new Circle(23.0);
                circle.setFill(Color.WHITE);
                circle.setStroke(null);

                int currentCol = col; // To use in the lambda
                circle.setOnMouseClicked(_ -> playMove(currentCol));
                // de data/ com

                GridPane.setHalignment(circle, HPos.CENTER);
                grid.add(circle, col, row);
            }
        }

        Rectangle clip = new Rectangle();
        clip.setArcWidth(100);
        clip.setArcHeight(100);
        clip.widthProperty().bind(grid.widthProperty());
        clip.heightProperty().bind(grid.heightProperty());
        grid.setClip(clip);

        return grid;
    }

    /**
     * Places a piece in the specified column for the current player.
     * The piece is dropped from the bottom row upwards until an empty space is
     * found.
     *
     * @param column the column in which to place the piece
     */
    public void playMove(int column) {
        if (!isGameStarted) {
            logger.info("Le jeu n'a pas encore commencé !");
            return;
        }

        // Traverse the column, from bottom to top, to place the piece
        for (int row = gameGrid.getRowConstraints().size() - 1; row >= 0; row--) {
            Circle circle = placeCircleAt(column, row);
            if (circle != null && circle.getFill() == Color.WHITE) {

                LightProfile myLightProfile = this.gameCore.getGameAsksDataClient().getPublicProfile()
                        .getLightProfile();
                currentUser = myLightProfile;
                fr.utc.connect4.core.data.Color currentPlayerColor;
                Game currentGame = this.gameCore.getGameAsksDataClient().getCurrentGame();
                if (currentGame.getPlayerCreator().equals(myLightProfile)) {
                    currentPlayerColor = currentGame.getColorCreator();
                } else {
                    currentPlayerColor = currentGame.getColorJ2();
                }
                Move moveToSend = new Move(0, column, myLightProfile, currentPlayerColor);
                this.gameCore.getGameAsksComClient().sendMove(moveToSend, currentGame.getUuid());
                break;
            }
        }
    }

    /**
     * Places a circle at the specified position in the grid.
     *
     * @param column the column in which to place the piece
     * @param row    the row in which to place the piece
     * @return the circle object placed in the grid
     */
    public Circle placeCircleAt(int column, int row) {
        for (javafx.scene.Node node : gameGrid.getChildren()) {
            if (GridPane.getColumnIndex(node) == column && GridPane.getRowIndex(node) == row) {
                if (node instanceof Circle circle) {
                    return circle;
                }
            }
        }
        return null;

    }

    /**
     * Displays a message in the waiting text area indicating that the game
     * is waiting for another player to join.
     */
    public void showWaitingMessage() {
        waitingTextArea.setStyle("-fx-font-weight: bold;");
        waitingTextArea.setText("En attente d'un autre joueur...");
    }

    /**
     * Displays a message in the waiting text area indicating that the game
     * is waiting for another player to join.
     */
    public void showWaitingMessageStart() {
        waitingTextArea.setStyle("-fx-font-weight: bold;");
        waitingTextArea.setText("En attente du lancement de la partie...");
    }

    /**
     * Clears the waiting message and displays a message indicating that the
     * game is starting.
     */
    public void clearWaitingMessage() {
        waitingTextArea.clear();
        waitingTextArea.setStyle("-fx-font-weight: bold;");
        waitingTextArea.setText("Début du jeu !");
    }

    /**
     * Displays the list of users (players and spectators) in the UI.
     *
     * @param game the game instance containing players
     */
    public void displayUserList(fr.utc.connect4.core.data.Game game) {
        currentGame = game;
        // Initialize player1 and player2
        player1 = game.getPlayerCreator();
        player2 = game.getPlayer2();

        usersVBox.getChildren().clear();

        // Convert player colors to CSS-compatible hexadecimal strings
        String colorPlayer1CSS = GameUtils.convertToJavaFXColor(game.getColorCreator()).toString().substring(2, 8);
        String colorPlayer2CSS = GameUtils.convertToJavaFXColor(
                game.getColorCreator() == fr.utc.connect4.core.data.Color.RED
                        ? fr.utc.connect4.core.data.Color.YELLOW
                        : fr.utc.connect4.core.data.Color.RED)
                .toString().substring(2, 8);

        // Create labels for players with color
        Label player1Label = new Label(player1.getUsername());
        player1Label.setStyle("-fx-text-fill: #" + colorPlayer1CSS + "; -fx-font-size: 14px; -fx-font-weight: bold;");

        Label player2Label = new Label(player2.getUsername());
        player2Label.setStyle("-fx-text-fill: #" + colorPlayer2CSS + "; -fx-font-size: 14px; -fx-font-weight: bold;");

        // Add labels for players to the VBox
        usersVBox.getChildren().addAll(player1Label, player2Label);

        // Add labels for spectators (no color)
        for (LightProfile spectator : game.getSpectators()) {
            Label spectatorLabel = new Label(spectator.getUsername());
            spectatorLabel.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
            usersVBox.getChildren().add(spectatorLabel);
        }

    }

    /**
     * Updates the game grid after each move, checking for wins or draws.
     *
     * @param game the game instance containing players
     */

    public void updateDisplayGame(Game game) {
        updatePlayerState(game);
        currentGame = game;

        // Get the board from the Game object
        List<List<fr.utc.connect4.core.data.Color>> OriginalBoard = game.getBoard();
        List<List<fr.utc.connect4.core.data.Color>> board = new ArrayList<>();

        for (int i = 0; i < OriginalBoard.size(); i++) {
            List<fr.utc.connect4.core.data.Color> row = new ArrayList<>();
            for (int j = OriginalBoard.get(i).size() - 1; j >= 0; j--) {
                fr.utc.connect4.core.data.Color color = OriginalBoard.get(i).get(j);
                row.add(color);
            }

            board.add(row);
        }

        // Collections.reverse(board);

        // Iterate through all nodes in the GridPane
        for (javafx.scene.Node node : gameGrid.getChildren()) {
            if (node instanceof Circle) {
                // Get the Circle's column and row
                Integer col = GridPane.getColumnIndex(node);
                Integer row = GridPane.getRowIndex(node);

                // Ensure we have valid positions
                if (col != null && row != null && row <= board.size() && col <= board.get(row).size()) {
                    // Get the corresponding color from the board
                    fr.utc.connect4.core.data.Color boardColor = board.get(col).get(row);

                    // Convert to JavaFX color and update the Circle's fill
                    ((Circle) node).setFill(GameUtils.convertToJavaFXColor(boardColor));
                }
            }
        }

        updatePlayerState(currentGame);
        displayUserList(currentGame);
        displayMessageToChat(currentGame);

    }

    /**
     * Updates the game state
     *
     * @param game the game instance containing players
     */
    private void updatePlayerState(Game game) {
        currentGame = game;
        player1 = currentGame.getPlayerCreator();
        player2 = currentGame.getPlayer2();
        currentUser = this.gameCore.getGameAsksDataClient().getPublicProfile().getLightProfile();

        topBox.getChildren().clear();

        // Définition des styles de base pour les labels
        String greenStyle = "-fx-background-color: green; -fx-text-fill: white; -fx-background-radius: 10px; -fx-padding: 5px;";
        String redStyle = "-fx-background-color: red; -fx-text-fill: white; -fx-background-radius: 10px; -fx-padding: 5px;";
        String userLabelStyle = "-fx-text-fill: black; -fx-font-size: 14px; -fx-background-radius: 10px; -fx-padding: 5px;";

        // Définition des messages pour chaque cas
        String turnMessageForPlayer = "C'est à ton tour de jouer let's gooo";
        String notYourTurnMessage = "Ce n'est pas ton tour";

        // (tour 0)
        if (currentGame.getMoves().isEmpty()) {
            // Le créateur (player1) est vert et player2 est rouge
            if (currentUser.getUuid().equals(player1.getUuid())) {
                playerLabel = new Label(currentUser.getUsername());
                playerLabel.setStyle(greenStyle);
                userLabel = new Label(turnMessageForPlayer);
                userLabel.setStyle(userLabelStyle);
            } else if (currentUser.getUuid().equals(player2.getUuid())) {
                playerLabel = new Label(currentUser.getUsername());
                playerLabel.setStyle(redStyle);
                userLabel = new Label(notYourTurnMessage);
                userLabel.setStyle(userLabelStyle);
            }
        }
        // Si des mouvements ont eu lieu
        else {
            // Vérifie qui a joué en dernier
            if (currentGame.getMoves().getLast().getColor().equals(currentGame.getColorCreator())) {
                // Si c'est au tour de player2
                if (currentUser.getUuid().equals(player2.getUuid())) {
                    playerLabel = new Label(currentUser.getUsername());
                    playerLabel.setStyle(greenStyle);
                    userLabel = new Label(turnMessageForPlayer);
                    userLabel.setStyle(userLabelStyle);
                } else {
                    playerLabel = new Label(currentUser.getUsername());
                    playerLabel.setStyle(redStyle);
                    userLabel = new Label(notYourTurnMessage);
                    userLabel.setStyle(userLabelStyle);
                }
            } else {
                // Si c'est au tour de player1
                if (currentUser.getUuid().equals(player1.getUuid())) {
                    playerLabel = new Label(currentUser.getUsername());
                    playerLabel.setStyle(greenStyle);
                    userLabel = new Label(turnMessageForPlayer);
                    userLabel.setStyle(userLabelStyle);
                } else {
                    playerLabel = new Label(currentUser.getUsername());
                    playerLabel.setStyle(redStyle);
                    userLabel = new Label(notYourTurnMessage);
                    userLabel.setStyle(userLabelStyle);
                }
            }
        }

        topBox.getChildren().addAll(playerLabel, userLabel);
    }

    /**
     * Sends a message entered in the chat field to the waiting text area.
     */
    public void sendMessage() {
        String text = chatTextField.getText();
        if (!text.isEmpty()) {
            Message message = new Message();
            message.setText(text);
            // ici envoyer à data pour add dans la liste des messages du game et display
            chatTextField.clear();
        }
    }

    /**
     * Display the message of a game
     *
     * @param game the game instance containing players
     */

    public void displayMessageToChat(Game game) {
        if (game == null || game.getMessages() == null) {
            return;
        }
        List<Message> messages = game.getMessages();
        for (Message message : messages) {
            String formattedMessage = message.getSender() + " : \t" + message.getText() + "\t" + message.getTime();
            waitingTextArea.appendText(formattedMessage);
        }
    }

    /**
     * Sends a message to all users in the game chat.
     * 
     * @param message the message to be sent to users
     */
    void sendMessageToUsers(String message) {
        message = message.trim();
        if (!message.isEmpty()) {
            waitingTextArea.setStyle("-fx-font-weight: bold;");
            waitingTextArea.appendText("\n" + message);
        }
    }

    /**
     * Shows a confirmation dialog when the user attempts to quit the game.
     */
    public void showQuitConfirmation(Game game) {
        currentGame = game;
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Quitter la partie ?");
        alert.setHeaderText("Êtes-vous sûr de vouloir quitter la partie ?");
        alert.setContentText("Cette action est irréversible et provoquera la victoire de votre adversaire.");

        ButtonType buttonRetour = new ButtonType("Retour au jeu");
        ButtonType buttonQuitter = new ButtonType("Quitter la partie");

        alert.getButtonTypes().setAll(buttonRetour, buttonQuitter);

        alert.showAndWait().ifPresent(response -> {
            if (response == buttonQuitter) {
                if (game != null) {
                    // Si le jeu a commencé, afficher le résultat
                    displayResult(game.isFinished());
                } else {
                    // Si le jeu n'a pas commencé, fermer simplement la fenêtre
                    Stage stage = (Stage) btnQuitter.getScene().getWindow();
                    this.gameCore.getGameAsksMain().showMainUi(stage);
                }
            }
        });
    }

    // function display result
    public void displayResult(GameResult gameResult) {
        ButtonType gameToMain = new ButtonType("Retour à l'écran titre");

        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Résultat de la partie");
        switch (gameResult) {
            case GameResult.DRAW:
                alert.setHeaderText("Égalité");
                break;
            case GameResult.WIN:
                alert.setHeaderText("Vous avez gagné !");
                break;
            case GameResult.LOSE:
                alert.setHeaderText("Vous avez perdu");
                break;
            default:
                break;
        }
        alert.setContentText("Fin de partie");

        alert.getButtonTypes().setAll(gameToMain);

        alert.showAndWait().ifPresent(response -> {
            if (response == gameToMain) {

                // fermer simplement la fenêtre
                Stage stage = (Stage) btnQuitter.getScene().getWindow();
                this.gameCore.getGameAsksMain().showMainUi(stage);

            }
        });
    }

}
