package fr.utc.connect4.client.interfaces;

import javafx.stage.Stage;

public interface IMainCore {

    public void setMainAsksGame(IMainAsksGame mainAsksGame);

    public IGameAsksMain getGameAsksMain();

    public void setMainAsksDataClient(IMainAsksDataClient mainAsksDataClient);

    public IDataClientAsksMain getDataClientAsksMain();

    public void setMainAsksComClient(IMainAsksComClient mainAsksComClient);

    public IComClientAsksMain getComClientAsksMain();

    public abstract void start(Stage primaryStage);

}
