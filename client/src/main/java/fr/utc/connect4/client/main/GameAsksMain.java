package fr.utc.connect4.client.main;

import java.io.IOException;

import fr.utc.connect4.client.interfaces.IGameAsksMain;
import javafx.stage.Stage;

public class GameAsksMain implements IGameAsksMain {

    private final MainCore mainCore;

    public GameAsksMain(MainCore mainCore) {
        this.mainCore = mainCore;
    }

    @Override
    public void showMainUi(Stage primaryStage) {
        try {
            this.mainCore.loadMainScene(primaryStage);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
