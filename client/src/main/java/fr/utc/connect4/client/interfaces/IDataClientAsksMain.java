package fr.utc.connect4.client.interfaces;

import java.util.List;

import fr.utc.connect4.core.data.LightGame;
import fr.utc.connect4.core.data.LightProfile;

public interface IDataClientAsksMain {
    /**
     * Updates the display with a new list of user profiles.
     * 
     * @param updatedUserList List of user profiles to display
     */
    void updateDisplay(List<LightProfile> updatedUserList);

    /**
     * Updates the games display with current game information.
     * 
     * @param games List of games to display
     */
    void updateGamesDisplay(List<LightGame> games);
}
