package fr.utc.connect4.client.main;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.utc.connect4.core.data.PublicProfile;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class LoginViewController {

    /**
     * Logger instance for logging messages within the class.
     */
    private static final Logger logger = LogManager.getLogger(LoginViewController.class);

    @FXML
    private TextField usernameField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private TextField serverIPField;

    @FXML
    private TextField serverPortField;

    @FXML
    private Button loginButton;

    @FXML
    private Button createAccountButton;

    private MainCore mainCore;

    public void setMainCore(MainCore mainCore) {
        this.mainCore = mainCore;
    }

    @FXML
    public void initialize() {
        loginButton.setOnAction(_ -> handleLogin());
        createAccountButton.setOnAction(_ -> handleCreateAccount());
    }

    /**
     * click on login
     * login to server with the specified login/password
     */
    private void handleLogin() {
        String username = usernameField.getText();
        String password = passwordField.getText();
        String serverIP = serverIPField.getText();
        String serverPort = serverPortField.getText();

        logger.info("Trying to login :");
        logger.info("Username: {}", username);
        logger.info("Password {}", password);
        logger.info("IP: {}", serverIP);
        logger.info("Port: {}", serverPort);

        try {
            PublicProfile profile = this.mainCore.getMainAsksDataClient().loadUserDataProfile(username,
                    hashPassword(password));
            if (profile != null) {
                this.mainCore.connectToServer(serverIP, Integer.parseInt(serverPort), profile);
            } else {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Profile invalide");
                alert.setContentText("Le profile n'a pas pu être chargé");
                alert.showAndWait();

            }
        } catch (Exception e) {
            logger.error("An error occurred during logging");

            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Connection échouée");
            alert.setContentText(
                    "La connection n'a pas pu aboutir : "
                            + e.getMessage()
                            + "\nVeuillez consulter les logs pour plus des détails");
            alert.showAndWait();
        }
    }

    /**
     * click on "Creer un compte"
     * Display the creation profile window
     */
    private void handleCreateAccount() {
        logger.info("New account creation started...");
        this.mainCore.getCreateProfileCore().displayUserProfileCreationWindow(this.mainCore.getMainStage());
    }

    /**
     * Hashes the given password using SHA-256.
     * SAME FUNCTION AS IN CreateProfileController.java
     * 
     * @param password The password to be hashed.
     * @return The hashed password as a hexadecimal string.
     */
    static public String hashPassword(String password) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] encodedHash = digest.digest(
                    password.getBytes(StandardCharsets.UTF_8));

            StringBuilder hexString = new StringBuilder();
            for (byte b : encodedHash) {
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Failed to hash password", e);
        }
    }

}
