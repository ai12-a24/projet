package fr.utc.connect4.client.com;

import fr.utc.connect4.client.interfaces.IDataClientAsksComClient;

/**
 * This class provide interface between DataClientCore and ComClientCore.
 * 
 * @see IDataClientAsksComClient
 * @see ComClientCore
 */
public class DataClientAsksComClient implements IDataClientAsksComClient {

    /**
     * The {@code ComClientCore} object that handles core communication
     * functionality.
     */
    @SuppressWarnings("unused")
    private final ComClientCore comClientCore;

    /**
     * Constructs a new {@code DataClientAsksComClient} instance with the specified
     * {@link ComClientCore} object.
     * 
     * @param comClientCore the {@code ComClientCore} object to be used for
     *                      managing core communication logic.
     */
    public DataClientAsksComClient(ComClientCore comClientCore) {
        this.comClientCore = comClientCore;
    }

}
