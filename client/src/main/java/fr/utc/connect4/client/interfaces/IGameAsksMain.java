package fr.utc.connect4.client.interfaces;

import javafx.stage.Stage;

/**
 * Interface for handling game-related communications from the IHM.
 */
public interface IGameAsksMain {

    void showMainUi(Stage primaryStage);
}