package fr.utc.connect4.client.main;

import java.util.List;

import fr.utc.connect4.client.interfaces.IDataClientAsksMain;
import fr.utc.connect4.core.data.LightGame;
import fr.utc.connect4.core.data.LightProfile;

public class DataClientAsksMain implements IDataClientAsksMain {

    private final MainCore mainCore;

    public DataClientAsksMain(MainCore mainCore) {
        this.mainCore = mainCore;
    }

    @Override
    public void updateDisplay(List<LightProfile> updatedUserList) {
        this.mainCore.getMainController().getUpdatedUserList(updatedUserList);
    }

    @Override
    public void updateGamesDisplay(List<LightGame> games) {
        this.mainCore.getMainController().updateGamesList(games);
    }

}
