package fr.utc.connect4.client.com;

import java.net.URISyntaxException;
import java.util.UUID;

import fr.utc.connect4.client.interfaces.IMainAsksComClient;
import fr.utc.connect4.core.com.*;
import fr.utc.connect4.core.data.Color;
import fr.utc.connect4.core.data.GridFormat;
import fr.utc.connect4.core.data.LightProfile;
import fr.utc.connect4.core.data.LightGame;
import fr.utc.connect4.core.data.PublicProfile;

/**
 * This class provide interface between MainCore and ComClientCore.
 */
public class MainAsksComClient implements IMainAsksComClient {

    /**
     * The {@link ComClientCore} instance used to send requests and handle
     * connections. This is a private field that is initialized via the constructor.
     */
    private final ComClientCore comClientCore;

    /**
     * Constructs a {@link MainAsksComClient} with the specified
     * {@link ComClientCore} instance.
     * 
     * @param comClientCore The {@link ComClientCore} instance responsible for
     *                      handling communication.
     */
    public MainAsksComClient(ComClientCore comClientCore) {
        this.comClientCore = comClientCore;
    }

    /**
     * Sends a request to create a game with the specified grid format and color for
     * the creator.
     * 
     * @param format       The {@link GridFormat} that defines the dimensions and
     *                     configuration of the game grid.
     * @param colorCreator The {@link Color} representing the creator's color in the
     *                     game.
     */
    @Override
    public void requestCreateGame(LightGame game) {
        this.comClientCore.send(new RequestCreateGame(game));
    }

    /**
     * Sends a request to retrieve a list of available games.
     */
    @Override
    public void requestGames() {
        this.comClientCore.send(new RequestListGame());
    }

    /**
     * Sends a request to retrieve a list of users currently online.
     */
    @Override
    public void requestUsersOnline() {
        this.comClientCore.send(new RequestListPlayer());
    }

    /**
     * Logs the user out from the current session.
     */
    @Override
    public void logout(PublicProfile profile) {
        LightProfile oldLight = new LightProfile(profile.getUuid(), profile.getUsername());
        this.comClientCore.send(new RequestDelPlayer(oldLight));
        this.comClientCore.logout();
    }

    /**
     * Establishes a connection to the server with the specified profile, hostname,
     * and port.
     * 
     * @param profile  The {@link PublicProfile} containing the user's profile
     *                 details.
     * @param hostname The hostname of the server to connect to.
     * @param port     The port number of the server.
     * 
     * @throws URISyntaxException   If the URI for the connection is invalid.
     * @throws InterruptedException If the thread is interrupted during the
     *                              connection process.
     */
    @Override
    public void connect(PublicProfile profile, String hostname, int port)
            throws Exception {
        this.comClientCore.connect(profile, hostname, port);
    }

    @Override
    public void joinGame(UUID uuidGame) {
        this.comClientCore.send(new RequestJoinGame(uuidGame));
    }

    @Override
    public void fetchPlayerProfile(UUID uuid) {
        this.comClientCore.send(new RequestFetchProfile(uuid));
    }

}
