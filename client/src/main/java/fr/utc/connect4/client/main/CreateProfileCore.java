package fr.utc.connect4.client.main;

import java.io.IOException;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.utc.connect4.client.data.ProfileSaveException;
import fr.utc.connect4.client.interfaces.IMainAsksDataClient;
import fr.utc.connect4.client.interfaces.IMainCore;
import fr.utc.connect4.core.ResourceUtils;
import fr.utc.connect4.core.data.PrivateProfile;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * This class handles the creation and display of the user profile creation
 * window.
 */
public class CreateProfileCore {
    private static final Logger logger = LogManager.getLogger(CreateProfileCore.class);
    private final IMainCore mainCore;
    private final IMainAsksDataClient dataClient;

    /**
     * Constructor for CreateProfileCore.
     *
     * @param mainCore   The main core interface to be used by this class.
     * @param dataClient The data client interface to be used by this class.
     */
    public CreateProfileCore(IMainCore mainCore, IMainAsksDataClient dataClient) {
        this.mainCore = mainCore;
        this.dataClient = dataClient;
    }

    /**
     * Displays the user profile creation window.
     *
     * @param parentStage The parent stage to which this dialog is modal.
     */
    public void displayUserProfileCreationWindow(Stage parentStage) {
        try {
            // Load the FXML
            URL fxml = ResourceUtils.getResourceURL("/client/view/CreateProfile.fxml");
            FXMLLoader loader = new FXMLLoader(fxml);

            // Create the controller with dependencies
            CreateProfileController controller = new CreateProfileController(this);
            loader.setController(controller);

            VBox root = loader.load();

            controller.setupHandlers();

            Stage dialogStage = new Stage();
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(parentStage);
            dialogStage.setTitle("Connect4 - Créer un compte");
            dialogStage.getIcons().add(MainImages.ICON.get());

            Scene scene = new Scene(root);
            scene.getStylesheets().add(ResourceUtils.getResourceURL("/client/style/stylesheet.css").toExternalForm());

            dialogStage.setScene(scene);
            dialogStage.setResizable(false);

            dialogStage.showAndWait();

        } catch (IOException e) {
            logger.error("Failed to load create profile view", e);
        }
    }

    /**
     * Creates a new profile using the given profile data.
     * 
     * @param profile The profile to be created.
     */
    public void createProfile(PrivateProfile profile) {
        try {
            // Save profile using data client
            dataClient.setUserData(profile);

            // Log success
            logger.info("Profile created successfully for user: " + profile.getUsername());

        } catch (ProfileSaveException e) {
            logger.error("Failed to create profile", e);
        }
    }
}