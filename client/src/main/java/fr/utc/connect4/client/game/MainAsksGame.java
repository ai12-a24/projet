package fr.utc.connect4.client.game;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.utc.connect4.client.interfaces.IMainAsksGame;
import fr.utc.connect4.core.ResourceUtils;
import fr.utc.connect4.core.data.Color;
import fr.utc.connect4.core.data.Game;
import fr.utc.connect4.core.data.LightGame;
import fr.utc.connect4.core.data.LightProfile;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * The {@code MainAsksGame} class implements the {@code IMainAsksGame} interface
 * and is responsible for managing the game creation, player joining, and
 * spectator functionality within the Connect4 game.
 */
public class MainAsksGame implements IMainAsksGame {
    /** Logger instance for tracking application events and errors */
    private static final Logger logger = LogManager.getLogger(MainAsksGame.class);
    /** Core game logic handler */
    private final GameCore gameCore;

    /**
     * Constructs a {@code MainAsksGame} object with the given {@code GameCore}.
     *
     * @param gameCore the {@code GameCore} instance that manages game logic
     */
    public MainAsksGame(GameCore gameCore) {
        this.gameCore = gameCore;
    }

    /**
     * Creates a new game with the specified parameters and sets up the game view.
     *
     * @param primaryStage  the primary stage for the game window
     * @param playerCreator the player who is creating the game
     * @param grid          the grid format for the game
     * @param color         the color chosen by the player
     * @param chat          whether chat functionality is enabled
     * @return the {@code InitGame} object containing game setup information
     */
    @Override
    public void displayGameForCreator(Stage primaryStage, LightGame lightGame) {

        GameController gameController = this.gameCore.getGameController();

        try {
            // Load the game view from the FXML file
            URL fxmlGame = ResourceUtils.getResourceURL("/client/view/GAME.fxml");
            FXMLLoader loaderGame = new FXMLLoader(fxmlGame);

            // Link the controller to the FXML loader
            loaderGame.setController(gameController);

            // Load the content of the game view
            BorderPane gameView = loaderGame.load();

            // Replace the current scene with the game scene

            Scene scene = new Scene(gameView, primaryStage.getWidth(), primaryStage.getHeight());
            primaryStage.setScene(scene);

            // Initialize the game logic
            gameController.initGame(lightGame);

        } catch (Exception err) {
            logger.error("An error occurred while launching the game: ", err);
        }

    }

    /**
     * Creates a new game with the specified parameters and sets up the game view.
     *
     * @param primaryStage  the primary stage for the game window
     * @param playerCreator the player who is creating the game
     * @param grid          the grid format for the game
     * @param color         the color chosen by the player
     * @param chat          whether chat functionality is enabled
     * @return the {@code InitGame} object containing game setup information
     */
    @Override
    public void displayGameForPlayer2(Stage primaryStage, LightGame lightGame) {

        GameController gameController = this.gameCore.getGameController();

        try {
            // Load the game view from the FXML file
            URL fxmlGame = ResourceUtils.getResourceURL("/client/view/GAME.fxml");
            FXMLLoader loaderGame = new FXMLLoader(fxmlGame);

            // Link the controller to the FXML loader
            loaderGame.setController(gameController);

            // Load the content of the game view
            BorderPane gameView = loaderGame.load();

            // Replace the current scene with the game scene
            Scene scene = new Scene(gameView, primaryStage.getWidth(), primaryStage.getHeight());
            primaryStage.setScene(scene);

            // Initialize the game logic
            gameController.initGameForPlayer2(lightGame);

        } catch (Exception err) {
            logger.error("An error occurred while launching the game: ", err);
        }

    }

    /**
     * Joins the game as a player with the specified {@code LightProfile}.
     *
     * @param initGame the initial game setup
     * @param player2  the player joining the game
     * @return the {@code Game} object representing the actual game
     */
    @Override
    public void joinGameAsPlayer(Game game) {

        GameController gameController = this.gameCore.getGameController();
        gameController.startGame(game);

    }

    /**
     * Joins the game as a spectator with the specified {@code LightProfile}.
     *
     * @param game      the game to join as a spectator
     * @param spectator the spectator joining the game
     */
    @Override
    public void joinGameAsSpectator(Game game) {
        List<LightProfile> spectators = game.getSpectators();

        // Retrieve the game controller from the core game
        GameController gameController = gameCore.getGameController();

        // Display the updated user list
        gameController.displayUserList(game);

        // Send a message indicating the new spectator joined
        String message = spectators.getLast().getUsername() + " nous a rejoint";
        gameController.sendMessageToUsers(message);

    }

}
