package fr.utc.connect4.client.interfaces;

import fr.utc.connect4.core.data.PublicProfile;

public interface IComClientAsksMain {

    /**
     * The client was disconnected.
     */
    void disconnected();

    /**
     * Return the profile of the player.
     * @param profile
     */
    void showProfile(PublicProfile profile);
}
