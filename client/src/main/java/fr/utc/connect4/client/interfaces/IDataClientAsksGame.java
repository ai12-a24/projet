package fr.utc.connect4.client.interfaces;

import java.util.List;

import fr.utc.connect4.core.data.Game;
import javafx.scene.layout.GridPane;

public interface IDataClientAsksGame {

    public List<Integer> sendMove(int column, int row);

    public void updateDisplayGame(Game game);

    public void displayResult(Game game);

    public void startGame(Game game);

}
