package fr.utc.connect4.client.com;

import java.net.URISyntaxException;
import java.time.Duration;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.utc.connect4.client.interfaces.IComClientAsksDataClient;
import fr.utc.connect4.client.interfaces.IComClientAsksGame;
import fr.utc.connect4.client.interfaces.IComClientAsksMain;
import fr.utc.connect4.client.interfaces.IComClientCore;
import fr.utc.connect4.client.interfaces.IDataClientAsksComClient;
import fr.utc.connect4.client.interfaces.IGameAsksComClient;
import fr.utc.connect4.client.interfaces.IMainAsksComClient;
import fr.utc.connect4.core.com.HandleAddPlayer;
import fr.utc.connect4.core.com.HandleCreateGame;
import fr.utc.connect4.core.com.HandleDelPlayer;
import fr.utc.connect4.core.com.HandleDisconnected;
import fr.utc.connect4.core.com.HandleGameEnd;
import fr.utc.connect4.core.com.HandleLaunchGame;
import fr.utc.connect4.core.com.HandleUpdateGame;
import fr.utc.connect4.core.com.HandleUpdateMove;
import fr.utc.connect4.core.com.IClientMessage;
import fr.utc.connect4.core.com.IServerMessage;
import fr.utc.connect4.core.com.ResponseFetchProfile;
import fr.utc.connect4.core.com.ResponseListGame;
import fr.utc.connect4.core.com.ResponseListPlayer;
import fr.utc.connect4.core.com.ResponsePlayerJoinsFullGame;
import fr.utc.connect4.core.data.PublicProfile;

/**
 * Represents the core communication on client for managing WebSocket
 * connections and handling messages between the client and the server.
 * This class implements the {@link IComClientCore} interface and is responsible
 * for connecting to the server, sending and receiving messages, and managing
 * the states of the game, players, and other relevant data.
 */
public class ComClientCore implements IComClientCore {

    /**
     * Logger instance for logging messages within the class.
     */
    private static final Logger logger = LogManager.getLogger(ComClientCore.class);

    /**
     * WebSocket client handler for managing WebSocket connections and
     * communication.
     */
    private WebSocketClientHandler webSocketClient;

    /**
     * Interface provided to DataClientCore.
     */
    private final IDataClientAsksComClient dataClientAsksComClient;

    /**
     * Interface provided to MainCore.
     */
    private final IMainAsksComClient mainAsksComClient;

    /**
     * Interface provided to GameCore.
     */
    private final IGameAsksComClient gameAsksComClient;

    /**
     * Interface used for communicate with MainCore.
     */
    private IComClientAsksMain comClientAsksMain;

    /**
     * Interface used for communicate with GameCore.
     */
    @SuppressWarnings("unused")
    private IComClientAsksGame comClientAsksGame;

    /**
     * Interface used for communicate with DataClient.
     */
    private IComClientAsksDataClient comClientAsksDataClient;

    /**
     * Constructs a new {@link ComClientCore} instance, initializing client
     * handlers.
     */
    public ComClientCore() {
        this.dataClientAsksComClient = new DataClientAsksComClient(this);
        this.mainAsksComClient = new MainAsksComClient(this);
        this.gameAsksComClient = new GameAsksComClient(this);
    }

    /**
     * Establishes a connection to the server using the provided profile, hostname,
     * and port. This method initiates the WebSocket client connection.
     *
     * @param profile  The profile of the user connecting.
     * @param hostname The hostname of the server to connect to.
     * @param port     The port number of the server.
     * @throws URISyntaxException   if the URI of the WebSocket connection is
     *                              invalid.
     * @throws InterruptedException if the connection process is interrupted.
     */
    public void connect(PublicProfile profile, String hostname, int port)
            throws Exception {
        this.logout();
        webSocketClient = new WebSocketClientHandler(hostname, port, profile, this::handle);
        webSocketClient.waitException(Duration.ofMillis(500));
    }

    /**
     * Logs out by closing the WebSocket connection and nullifying the client
     * handler.
     */
    public void logout() {
        if (webSocketClient != null) {
            webSocketClient.close();
            webSocketClient = null;
        }
    }

    /**
     * Sends a message to the server using the WebSocket connection.
     *
     * @param payload The message to be sent.
     * @throws NullPointerException if the WebSocket client is not connected.
     */
    public void send(IClientMessage payload) {
        Objects.requireNonNull(webSocketClient, "Cannot send message before connection.");
        webSocketClient.send(payload);
    }

    /**
     * Handles incoming server messages and routes them to the appropriate handlers
     * based on the message type.
     *
     * @param message The server message to handle.
     */
    public void handle(IServerMessage message) {
        switch (message) {
            case HandleCreateGame createGame -> {
                comClientAsksDataClient.addNewGame(createGame.game());
            }
            case HandleAddPlayer addPlayer -> {
                comClientAsksDataClient.addUserToList(addPlayer.profile());
            }
            case HandleDisconnected _ -> {
                comClientAsksMain.disconnected();
            }
            case ResponseListPlayer listPlayer -> {
                comClientAsksDataClient.addAllUsersToList(listPlayer.profiles());
            }
            case ResponseListGame listGame -> {
                comClientAsksDataClient.updateGamesList(listGame.games());
            }
            case HandleDelPlayer delPlayer -> {
                comClientAsksDataClient.removeUserToList(delPlayer.profile());

            }
            case ResponsePlayerJoinsFullGame playerJoinsGame -> {
                comClientAsksGame.rejectPlayerJoinGame(playerJoinsGame.game());
            }
            case HandleLaunchGame launchGame -> {
                comClientAsksDataClient.startGame(launchGame.game());
            }
            case HandleUpdateGame updateGame -> {
                comClientAsksDataClient.updateGameAddPlayer2(updateGame.game());
            }
            case ResponseFetchProfile profile -> {
                comClientAsksMain.showProfile(profile.profile());
            }
            case HandleUpdateMove updateMove -> {
                comClientAsksDataClient.updateMove(updateMove.game());
            }
            case HandleGameEnd gameEnd -> {
                comClientAsksGame.gameResult(gameEnd.result());
            }
            default -> logger.error("Unknown message type: {}", message.getClass().getName());
        }
    }

    @Override
    public void setComClientAsksMain(IComClientAsksMain comClientAsksMain) {
        this.comClientAsksMain = comClientAsksMain;
    }

    @Override
    public IMainAsksComClient getMainAsksComClient() {
        return this.mainAsksComClient;
    }

    @Override
    public void setComClientAsksGame(IComClientAsksGame comClientAsksGame) {
        this.comClientAsksGame = comClientAsksGame;
    }

    @Override
    public IGameAsksComClient getGameAsksComClient() {
        return this.gameAsksComClient;
    }

    @Override
    public void setComClientAsksDataClient(IComClientAsksDataClient comClientAsksDataClient) {
        this.comClientAsksDataClient = comClientAsksDataClient;
    }

    @Override
    public IDataClientAsksComClient getDataClientAsksComClient() {
        return this.dataClientAsksComClient;
    }

    /**
     * Retrieves the WebSocket client handler.
     *
     * @return The WebSocket client handler.
     */
    public WebSocketClientHandler getWebSocketClient() {
        return webSocketClient;
    }
}
