package fr.utc.connect4.client.interfaces;

import fr.utc.connect4.core.data.Game;
import fr.utc.connect4.core.data.PublicProfile;

public interface IGameAsksDataClient {

    // Méthode pour effacer les données du jeu
    void clearDataGame();

    public PublicProfile getPublicProfile();

    public Game getCurrentGame();
}
