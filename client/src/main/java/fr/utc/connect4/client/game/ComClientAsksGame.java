package fr.utc.connect4.client.game;

import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.utc.connect4.client.interfaces.IComClientAsksGame;
import fr.utc.connect4.core.data.GameResult;

/**
 * Handles communication between client and game components for game-related
 * requests.
 * This class implements IComClientAsksGame to manage game interactions from the
 * client side.
 */
public class ComClientAsksGame implements IComClientAsksGame {

    private static final Logger logger = LogManager.getLogger(ComClientAsksGame.class);

    private final GameCore gameCore;

    /**
     * Constructs a new ComClientAsksGame instance.
     * 
     * @param gameCore the game core instance to handle game logic
     */
    public ComClientAsksGame(GameCore gameCore) {
        this.gameCore = gameCore;
    }

    /**
     * Rejects a player's attempt to join a game.
     * 
     * @param uuidGame the UUID of the game the player attempted to join
     */
    @Override
    public void rejectPlayerJoinGame(UUID uuidGame) {
        logger.warn("IHMGame: rejectPlayerJoinGame - Not implemented yet");
    }

    @Override
    public void gameResult(GameResult gameResult) {
        gameCore.gameResult(gameResult);
    }
}
