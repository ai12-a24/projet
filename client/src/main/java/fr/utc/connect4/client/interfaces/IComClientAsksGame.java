package fr.utc.connect4.client.interfaces;

import java.util.UUID;

import fr.utc.connect4.core.data.GameResult;

public interface IComClientAsksGame {

    /**
     * This method is called when a player joined a game that is already full.
     *
     * @param uuidGame Game that is full.
     */
    void rejectPlayerJoinGame(UUID uuidGame);

    void gameResult(GameResult gameResult);

}
