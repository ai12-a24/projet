package fr.utc.connect4.client.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.SecureRandom;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import fr.utc.connect4.core.data.PrivateProfile;
import fr.utc.connect4.core.data.PublicProfile;
import fr.utc.connect4.core.data.Result;
import javafx.util.Pair;

public class UserDataFileHandler {
    private PrivateProfile privateProfile;

    public UserDataFileHandler() {
        privateProfile = null;
    }

    // Load user data profile from file
    // Throws ProfileLoadException if any of the loading steps fails
    public PrivateProfile loadUserDataProfile(String username, String hash) throws ProfileLoadException {

        File folder = new File("profiles");
        if (!folder.exists()) {
            throw new ProfileLoadException("Profiles folder does not exist");
        }

        File file = new File(folder, username);
        if (!file.exists()) {
            throw new ProfileLoadException("Profile file not found for username: " + username);
        }

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            @SuppressWarnings("unchecked")
            Pair<SealedObject, byte[]> encryptedProfileAndIv = (Pair<SealedObject, byte[]>) ois.readObject();

            PrivateProfile profile = decrypt(encryptedProfileAndIv.getKey(), encryptedProfileAndIv.getValue(), hash);

            this.privateProfile = profile;
            return profile;
        } catch (IOException e) {
            throw new ProfileLoadException("IO error while loading profile", e);
        } catch (ClassNotFoundException e) {
            throw new ProfileLoadException("Class not found while deserializing profile", e);
        } catch (Exception e) {
            throw new ProfileLoadException("Error while decrypting profile", e);
        }
    }

    public void addUserGameResult(Result result) {
        if (privateProfile == null) {
            System.out.println("Error while adding game result: no loaded profile");
            return;
        }
        privateProfile.getResults().add(result);
    }

    public void setUserData(final PrivateProfile newPrivateProfile) throws ProfileSaveException {
        validateProfile(newPrivateProfile);
        writeToLocalData(newPrivateProfile);
        this.privateProfile = newPrivateProfile;
    }

    // Save user data profile to file
    // Throws ProfileSaveException if any of the saving steps fails
    private void writeToLocalData(final PrivateProfile newPrivateProfile) throws ProfileSaveException {
        if (newPrivateProfile == null) {
            throw new ProfileSaveException("No loaded profile to save");
        }

        File folder = new File("profiles");
        if (!folder.exists()) {
            try {
                if (!folder.mkdir()) {
                    throw new ProfileSaveException("Failed to create profiles directory");
                }
            } catch (SecurityException e) {
                throw new ProfileSaveException("Security error creating profiles directory", e);
            }
        } else {
            // Remove old profile file
            File oldFile = new File(folder,
                    this.privateProfile != null ? this.privateProfile.getUsername() : newPrivateProfile.getUsername());

            if (oldFile.exists()) {
                if (!oldFile.delete()) {
                    throw new ProfileSaveException("Failed to delete existing profile file");
                }
            }
        }

        // Create new profile file
        File file = new File(folder, newPrivateProfile.getUsername());
        try {
            Pair<SealedObject, byte[]> encryptedProfileAndIv = encrypt(newPrivateProfile, newPrivateProfile.getHash());

            try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
                oos.writeObject(encryptedProfileAndIv);
            }
        } catch (Exception e) {
            throw new ProfileSaveException("Error while saving profile", e);
        }
    }

    private Pair<SealedObject, byte[]> encrypt(PrivateProfile profile, String hash) throws Exception {
        SecureRandom random = new SecureRandom();
        byte[] iv = new byte[12];
        random.nextBytes(iv);

        SecretKey myAesKey = deriveKeyFromHash(hash, iv);

        Cipher aesCipher = Cipher.getInstance("AES/GCM/NoPadding");

        GCMParameterSpec gcmSpec = new GCMParameterSpec(128, iv); // 128-bit authentication tag
        aesCipher.init(Cipher.ENCRYPT_MODE, myAesKey, gcmSpec);

        return new Pair<>(new SealedObject(profile, aesCipher), iv);
    }

    private PrivateProfile decrypt(SealedObject sealedProfile, byte[] iv, String hash) throws Exception {
        SecretKey myAesKey = deriveKeyFromHash(hash, iv);

        Cipher aesCipher;
        aesCipher = Cipher.getInstance("AES/GCM/NoPadding");

        GCMParameterSpec gcmSpec = new GCMParameterSpec(128, iv); // 128-bit authentication tag
        aesCipher.init(Cipher.DECRYPT_MODE, myAesKey, gcmSpec);

        return (PrivateProfile) sealedProfile.getObject(aesCipher);
    }

    private SecretKey deriveKeyFromHash(String hash, byte[] salt) throws Exception {
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        KeySpec spec = new PBEKeySpec(hash.toCharArray(), salt, 64, 256);
        return new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
    }

    private void validateProfile(final PrivateProfile newPrivateProfile) {
        // Add other checks?
        if (newPrivateProfile == null) {
            throw new IllegalStateException("No loaded profile to save");
        }

        if (newPrivateProfile.getUsername() == null) {
            throw new IllegalStateException("Profile username is null");
        }

        if (newPrivateProfile.getUuid() == null) {
            throw new IllegalStateException("Profile UUID is null");
        }

        if (newPrivateProfile.getHash() == null) {
            throw new IllegalStateException("Profile hash is null");
        }
    }

    public PrivateProfile getMyPrivateProfile() {
        return privateProfile;
    }

    public PublicProfile getMyPublicProfile() {
        return privateProfile.getPublicProfile();
    }

}