package fr.utc.connect4.client.main;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.utc.connect4.client.com.ComClientCore;
import fr.utc.connect4.core.data.LightGame;
import fr.utc.connect4.core.data.LightProfile;
import fr.utc.connect4.core.data.PublicProfile;
import fr.utc.connect4.core.data.Result;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MainController {
    /**
     * Logger instance for logging messages within the class.
     */
    private static final Logger logger = LogManager.getLogger(ComClientCore.class);

    @FXML
    private VBox playersList;
    @FXML
    private Label profileNameLabel;
    @FXML
    private Label profileFirstNameLabel;
    @FXML
    private Label profilePseudoLabel;
    @FXML
    private Label profileBirthDateLabel;

    @FXML
    private VBox gameSectionsContainer;

    private PublicProfile distantProfile = null;

    private List<LightGame> currentGames = new ArrayList<>();

    private PublicProfile currentUser; // Structure for the connected user
    private List<LightProfile> activePlayers; // List of active players

    private MainCore mainCore;

    public void setMainCore(MainCore mainCore) {
        this.mainCore = mainCore;
    }

    // Define the formatter for dd/MM/yyyy
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    private double xOffset = 0;
    private double yOffset = 0;

    @FXML
    public void initialize() {

    }

    public void afterStart() {
        // Initialize user data
        // create a date with the format "MM/dd/yyyy"
        // currentUser = new PublicProfile("JeanD", UUID.randomUUID(),
        // LocalDate.of(1990, 1, 1), "Jean", "Dupont");

        this.currentUser = mainCore.getPublicProfileMain();

        // Initialize active players (20 explicitly defined players)
        activePlayers = new ArrayList<>();

        // Display user information and active players
        displayCurrentUser();
        displayOnlinePlayers();
    }

    public void getUpdatedUserList(List<LightProfile> updatedUserList) {
        activePlayers = updatedUserList;
        displayOnlinePlayers();
    }

    private void displayCurrentUser() {
        profileNameLabel.setText("Name: " + currentUser.getLastName());
        profileFirstNameLabel.setText("First Name: " + currentUser.getFirstName());
        profilePseudoLabel.setText("Pseudo: " + currentUser.getUsername());
        profileBirthDateLabel.setText("Date of Birth: " + currentUser.getDateOfBirth().format(formatter));
    }

    private void displayOnlinePlayers() {
        playersList.getChildren().clear();

        for (LightProfile player : activePlayers) {
            HBox playerRow = new HBox(10);
            playerRow.setStyle("-fx-padding: 5; -fx-alignment: center;");

            // Smiley to represent the avatar
            Label smiley = new Label("\uD83D\uDE00"); // Smiley Unicode
            smiley.setStyle("-fx-font-size: 20px;");
            smiley.setMinWidth(40); // Set a width to align the smileys

            // Player's pseudo
            Label pseudoLabel = new Label(player.getUsername());
            pseudoLabel.setStyle("-fx-font-size: 14px;");
            pseudoLabel.setMinWidth(200); // Set a width to align the pseudos

            // "View Profile" button
            Button profileButton = new Button("View Profile");
            profileButton.setMinWidth(120); // Set a width to align the buttons
            profileButton.setOnAction(event -> openPlayerProfile(player));

            // Add all elements in a row
            playerRow.getChildren().addAll(smiley, pseudoLabel, profileButton);
            playersList.getChildren().add(playerRow);
        }
    }

    private void openPlayerProfile(LightProfile player) {
        // recuperating the profile
        mainCore.getMainAsksComClient().fetchPlayerProfile(player.getUuid());

        // create a stage
        Stage loadingStage = new Stage();
        loadingStage.initModality(Modality.APPLICATION_MODAL);
        loadingStage.setTitle("Chargement du profil...");

        VBox loadingRoot = new VBox(10);
        loadingRoot.setAlignment(Pos.CENTER);
        loadingRoot.setStyle("-fx-padding: 20; -fx-background-color: #f4f4f4;");

        ProgressIndicator progressIndicator = new ProgressIndicator();
        Label loadingLabel = new Label("Chargement en cours...");

        loadingRoot.getChildren().addAll(progressIndicator, loadingLabel);
        Scene loadingScene = new Scene(loadingRoot, 300, 150);
        loadingStage.setScene(loadingScene);

        // loading screen
        loadingStage.show();

        // wait until the profile is not null
        CompletableFuture.runAsync(() -> {
            while (distantProfile == null) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }
            }

            // Close stage and display the distant profile
            Platform.runLater(() -> {
                loadingStage.close();
                displayPlayerProfile();
            });
        });
    }

    private void displayPlayerProfile() {
        Stage profileStage = new Stage();
        profileStage.initModality(Modality.APPLICATION_MODAL);
        profileStage.setTitle("Profil de " + distantProfile.getUsername());

        VBox root = new VBox(10);
        root.setStyle("-fx-padding: 20; -fx-background-color: #f4f4f4;");

        // player informations
        VBox infoBox = new VBox(5);
        infoBox.getChildren().addAll(
                new Label("Nom : " + distantProfile.getLastName()),
                new Label("Prénom : " + distantProfile.getFirstName()),
                new Label("Date de naissance : " + distantProfile.getDateOfBirth().toString()));
        infoBox.setStyle("-fx-font-size: 14px;");

        // Titre
        Label title = new Label(distantProfile.getUsername());
        title.setStyle("-fx-font-size: 24px; -fx-font-weight: bold;");

        // Historique des parties (centré et en gras)
        Label historyLabel = new Label("Historique des parties");
        historyLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold; -fx-alignment: center;");
        historyLabel.setAlignment(Pos.CENTER);

        // Result table
        TableView<Result> table = new TableView<>();
        table.setPrefHeight(200);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        TableColumn<Result, String> gridResultCol = new TableColumn<>("Résultat");
        gridResultCol.setCellValueFactory(data -> {
            String result = data.getValue().getGridResult().toString();
            switch (result) {
                case "WIN":
                    return new SimpleStringProperty("Victoire");
                case "LOSE":
                    return new SimpleStringProperty("Défaite");
                case "DRAW":
                    return new SimpleStringProperty("Match nul");
                case "NO_RESULT":
                    return new SimpleStringProperty("Pas de résultat");
                default:
                    return new SimpleStringProperty(result);
            }
        });
        gridResultCol.setCellFactory(column -> new TableCell<Result, String>() {
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty) {
                    setText(null);
                    setStyle("");
                } else {
                    setText(item);
                    setAlignment(Pos.CENTER);
                    if ("Victoire".equals(item)) {
                        setStyle("-fx-background-color: #2E8B57; -fx-text-fill: white;");
                    } else if ("Défaite".equals(item)) {
                        setStyle("-fx-background-color: #8B0000; -fx-text-fill: white;");
                    } else { // For "Match nul" and "Pas de résultat"
                        setStyle("");
                    }
                }
            }
        });

        TableColumn<Result, String> gridFormatCol = new TableColumn<>("Grille");
        gridFormatCol.setCellValueFactory(data -> {
            String format = data.getValue().getGridFormat().toString();
            return new SimpleStringProperty(format.replace("FORMAT_", ""));
        });
        gridFormatCol.setCellFactory(column -> new TableCell<Result, String>() {
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty) {
                    setText(null);
                    setStyle("");
                } else {
                    setText(item);
                    setAlignment(Pos.CENTER);
                    setStyle("-fx-background-color: #D3D3D3; -fx-text-fill: black;");
                }
            }
        });

        table.getColumns().addAll(gridResultCol, gridFormatCol);

        if (distantProfile.getResults() != null) {
            table.getItems().addAll(distantProfile.getResults());
        }

        // Closing button
        Button closeButton = new Button("Fermer");
        closeButton.setOnAction(e -> profileStage.close());

        root.getChildren().addAll(title, infoBox, historyLabel, table, closeButton);
        Scene scene = new Scene(root, 600, 400);
        profileStage.setScene(scene);
        profileStage.showAndWait();
    }

    @FXML
    private void disconnect(ActionEvent event) {
        Stage stage = (Stage) playersList.getScene().getWindow();
        this.mainCore.getMainAsksComClient().logout(this.currentUser);
        stage.close();
    }

    @FXML
    private void quitApplication(ActionEvent event) {
        // Création de la fenêtre (Stage)
        Stage confirmationStage = new Stage();
        confirmationStage.initModality(Modality.APPLICATION_MODAL);
        confirmationStage.setTitle("Confirmation de déconnexion");

        // Contenu de la fenêtre (VBox)
        VBox confirmationBox = new VBox(10);
        confirmationBox.setStyle("-fx-padding: 20; -fx-alignment: center; -fx-spacing: 15;");

        // Message de confirmation
        Label message = new Label("Voulez-vous vraiment vous déconnecter ?");
        message.setStyle("-fx-font-size: 14px;");

        // Boutons Oui et Non
        Button yesButton = new Button("Oui");
        yesButton.setStyle("-fx-background-color: #ff4d4d; -fx-text-fill: white;");
        yesButton.setOnAction(e -> {
            // Action de déconnexion confirmée
            confirmationStage.close();
            Stage stage = (Stage) playersList.getScene().getWindow();
            this.mainCore.getMainAsksComClient().logout(this.currentUser);
            stage.close();
        });

        Button noButton = new Button("Non");
        noButton.setOnAction(e -> confirmationStage.close()); // Fermer la fenêtre

        // Ajouter les éléments au conteneur
        HBox buttonBox = new HBox(10, yesButton, noButton);
        buttonBox.setStyle("-fx-alignment: center;");

        confirmationBox.getChildren().addAll(message, buttonBox);

        // Définir la scène et afficher la fenêtre
        Scene confirmationScene = new Scene(confirmationBox, 300, 150);
        confirmationStage.setScene(confirmationScene);
        confirmationStage.showAndWait();
    }

    @FXML
    private void exportProfile(ActionEvent event) {
        logger.info("Profile exported!");
    }

    @FXML
    private void editProfile(ActionEvent event) {
        try {
            // Load the FXML file
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/client/view/EditProfile.fxml"));
            VBox editProfileRoot = loader.load();

            // Get the controller and set the MainCore
            EditProfileController controller = loader.getController();
            controller.setMainCore(this.mainCore);

            // Create a new window (stage)
            Stage editProfileStage = new Stage();
            editProfileStage.initModality(Modality.APPLICATION_MODAL);
            editProfileStage.setTitle("Edit Profile");
            editProfileStage.setScene(new Scene(editProfileRoot));

            // Show the window and wait for it to close
            editProfileStage.showAndWait();

            // After window closes, refresh the profile display
            displayCurrentUser();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Function that open the create game form when the user click on Create Game
     */
    @FXML
    private void createGameWindow(ActionEvent event) {
        try {
            // Load the FXML file for the game creation window
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/client/view/create_game.fxml"));
            VBox createGameRoot = loader.load();

            CreateGameController controller = loader.getController();
            controller.setMainCore(this.mainCore);

            // Create a new window (stage)
            Stage createGameStage = new Stage();
            createGameStage.initModality(Modality.APPLICATION_MODAL);
            createGameStage.setTitle("Create a Game");
            createGameStage.setScene(new Scene(createGameRoot, 300, 300));

            // Show the new window
            createGameStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void playSound(ActionEvent event) {
        MainSounds.DEMO.play();
    }

    @FXML
    private void onMousePressed(MouseEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        xOffset = stage.getX() - event.getScreenX();
        yOffset = stage.getY() - event.getScreenY();
    }

    @FXML
    private void onMouseDragged(MouseEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setX(event.getScreenX() + xOffset);
        stage.setY(event.getScreenY() + yOffset);
    }

    /**
     * Updates the list of current games and displays them.
     * 
     * @param games the updated list of current games
     */
    public void updateGamesList(List<LightGame> games) {
        this.currentGames = games;
        LightProfile me = this.mainCore.getPublicProfileMain();
        displayGames();
        for (LightGame game : this.currentGames) {
            if (game.getPlayerCreator().getUuid().equals(me.getUuid())) {
                this.mainCore.getMainAsksGame().displayGameForCreator(this.mainCore.getMainStage(),
                        game);
            }
        }
    }

    /**
     * Displays the list of current games.
     */
    private void displayGames() {
        // Clear existing content
        gameSectionsContainer.getChildren().clear();

        // Create scrollable sections
        TitledPane ongoingGamesPane = createCollapsibleSection(
                "Parties en cours",
                createScrollableGameSection(false));

        TitledPane waitingGamesPane = createCollapsibleSection(
                "Parties en attente",
                createScrollableGameSection(true));

        // Add sections to container
        gameSectionsContainer.getChildren().addAll(ongoingGamesPane, waitingGamesPane);
    }

    /**
     * Creates a scrollable section for games.
     * 
     * @param isWaiting true if the section is for waiting games, false otherwise
     * @return the scrollable section
     */
    private ScrollPane createScrollableGameSection(boolean isWaiting) {
        // Create container for games
        VBox gamesContainer = new VBox(5);
        gamesContainer.setStyle("-fx-padding: 5;");

        // Add relevant games
        for (LightGame game : currentGames) {
            if ((game.getPlayer2() == null) == isWaiting) {
                HBox gameRow = createGameRow(game, isWaiting);
                gamesContainer.getChildren().add(gameRow);
            }
        }

        // Create scrollable container
        ScrollPane scrollPane = new ScrollPane(gamesContainer);
        scrollPane.setFitToWidth(true);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED); // Show vertical scrollbar as needed
        VBox.setVgrow(scrollPane, Priority.ALWAYS); // Make it grow to fill available space
        scrollPane.setStyle("-fx-background-color: transparent;");

        // Make the games container fit the width of the scroll pane
        gamesContainer.prefWidthProperty().bind(scrollPane.widthProperty().subtract(20)); // subtract scrollbar width

        return scrollPane;
    }

    /**
     * Creates a collapsible section with a title and content.
     * 
     * @param title   the title of the section
     * @param content the content of the section
     * @return the collapsible section
     */
    private TitledPane createCollapsibleSection(String title, ScrollPane content) {
        TitledPane titledPane = new TitledPane(title, content);
        titledPane.setExpanded(true);
        titledPane.setStyle("-fx-font-weight: bold;");
        return titledPane;
    }

    /**
     * Creates a row for a game.
     * 
     * @param game      the game to be displayed
     * @param isWaiting true if the game is waiting for a player, false otherwise
     * @return the row for the game
     */
    private HBox createGameRow(LightGame game, boolean isWaiting) {
        HBox row = new HBox(10);
        row.setStyle("-fx-padding: 5; -fx-alignment: center-left;");

        // Game information
        String gameText;
        if (isWaiting) {
            gameText = game.getPlayerCreator().getUsername() + " VS ?";
        } else {
            gameText = game.getPlayerCreator().getUsername() + " VS " + game.getPlayer2().getUsername();
        }
        Label gameLabel = new Label(gameText);
        gameLabel.setStyle("-fx-font-size: 14px;");

        // Spacer
        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);

        // Action buttons
        Button spectateButton = new Button("Rejoindre (spectateur)");
        spectateButton.setStyle("""
                    -fx-background-color: #7986CB;
                    -fx-text-fill: white;
                    -fx-padding: 5 15;
                    -fx-background-radius: 5;
                """);
        spectateButton.setOnAction(e -> joinAsSpectator(game));

        row.getChildren().addAll(gameLabel, spacer, spectateButton);

        if (isWaiting) {
            Button joinButton = new Button("Jouer");
            joinButton.setStyle("""
                        -fx-background-color: #FFD700;
                        -fx-padding: 5 15;
                        -fx-background-radius: 5;
                    """);
            joinButton.setOnAction(e -> joinGame(game));
            row.getChildren().add(2, joinButton); // Add before spectate button
        }

        return row;
    }

    /**
     * Join a game as a player.
     * 
     * @param game the game to join
     */
    private void joinGame(LightGame game) {
        logger.info("Clicked on button to join a game as player: {}", game);
        LightProfile loggedInUser = this.mainCore.getPublicProfileMain();
        if (game.getPlayerCreator().getUuid().equals(loggedInUser.getUuid())) {
            logger.info("You cannot join your own game.");
            logger.info("Game creator: {}", game.getPlayerCreator().getUuid());
            logger.info("Logged in user: {}", loggedInUser.getUuid());
        } else {
            this.mainCore.getMainAsksComClient().joinGame(game.getUuid());
            this.mainCore.getMainAsksGame().displayGameForPlayer2(this.mainCore.getMainStage(), game);
        }
    }

    /**
     * Joins a game as a spectator.
     * 
     * @param game the game to join as a spectator
     */
    private void joinAsSpectator(LightGame game) {
        // TODO: Implement spectator functionality
        logger.info("Joining game as spectator: {}", game);
    }

    /**
     * Update the distant profile to display
     *
     * @param distantProfile the public distant profile whe want to check
     */
    public void updateDistantProfile(PublicProfile distantProfile) {
        this.distantProfile = distantProfile;
    }
}