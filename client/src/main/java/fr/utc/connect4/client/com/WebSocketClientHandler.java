package fr.utc.connect4.client.com;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;
import java.time.Instant;
import java.util.function.Consumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import com.fasterxml.jackson.core.JsonProcessingException;

import fr.utc.connect4.core.Json;
import fr.utc.connect4.core.com.IClientMessage;
import fr.utc.connect4.core.com.IServerMessage;
import fr.utc.connect4.core.com.SendLogin;
import fr.utc.connect4.core.data.PublicProfile;
import javafx.application.Platform;

/**
 * WebSocketClientHandler is a specialized implementation of a WebSocket client
 * for handling
 * communication between a client and a server in a Connect4 game application.
 * 
 * It extends the {@link WebSocketClient} class and provides custom methods for
 * handling WebSocket
 * events such as opening a connection, receiving messages, sending messages,
 * and handling errors.
 * This handler facilitates user login and message dispatching through a
 * connection to the server,
 * while utilizing JSON serialization and deserialization for message handling.
 * 
 * The handler processes incoming messages asynchronously on the JavaFX
 * application thread.
 */
public class WebSocketClientHandler extends WebSocketClient {

    /**
     * Logger instance for logging messages within the class.
     */
    private static final Logger logger = LogManager.getLogger(WebSocketClientHandler.class);

    /**
     * The function to call for handle all IServerMessage.
     */
    private final Consumer<IServerMessage> handler;

    /**
     * Current user used for connection.
     */
    private final PublicProfile currentUser;

    /**
     * Last exception raised.
     */
    private Exception exception;

    /**
     * Constructs a new WebSocketClientHandler instance with the specified server
     * address,
     * user profile, and message handler.
     * 
     * @param hostname    The server hostname to connect to.
     * @param port        The server port to connect to.
     * @param currentUser The public profile of the user attempting to connect.
     * @param handler     The consumer that handles incoming server messages.
     * @throws URISyntaxException   if the URI for the WebSocket connection is
     *                              invalid.
     * @throws InterruptedException if the connection is interrupted.
     */
    public WebSocketClientHandler(
            String hostname,
            int port,
            PublicProfile currentUser,
            Consumer<IServerMessage> handler) throws URISyntaxException, InterruptedException {
        super(new URI("ws://" + hostname + ":" + port));
        this.handler = handler;
        this.currentUser = currentUser;
        this.exception = null;
        connectBlocking();
    }

    /**
     * Called when the WebSocket connection is successfully established.
     * Sends a login message with the current user profile to the server.
     * 
     * @param handshakedata Data from the handshake process with the server.
     */
    @Override
    public void onOpen(ServerHandshake handshakedata) {
        logger.info("Connected to Server");
        send(new SendLogin(currentUser));
        logger.info("Login to Server");
    }

    /**
     * Called when a message is received from the server.
     * The message is deserialized from JSON and passed to the handler for
     * processing.
     * The handler is executed on the JavaFX application thread to ensure thread
     * safety.
     * 
     * @param message The raw message received from the server as a string.
     */
    @Override
    public void onMessage(String message) {
        try {
            logger.info("Recv: {}", message);
            IServerMessage receivedMessage = Json.loads(message, IServerMessage.class);
            // Ensure message handling occurs on the JavaFX application thread to prevent
            // errors
            Platform.runLater(() -> {
                handler.accept(receivedMessage);
            });
        } catch (JsonProcessingException e) {
            logger.error("Invalid message", e);
        }
    }

    /**
     * Called when the WebSocket connection is closed.
     * Logs the reason for the closure of the connection.
     * 
     * @param code   The close code sent by the server.
     * @param reason The reason for the connection closure.
     * @param remote Indicates whether the connection was closed remotely.
     */
    @Override
    public void onClose(int code, String reason, boolean remote) {
        logger.info("Connection closed: " + reason);
    }

    /**
     * Called when an error occurs in the WebSocket connection.
     * Logs the exception details.
     * 
     * @param ex The exception that caused the error.
     */
    @Override
    public void onError(Exception ex) {
        logger.error(ex.getMessage(), ex);
        this.exception = ex;
    }

    /**
     * Sends a message to the server after serializing it into JSON format.
     * The message is logged before being sent to the server.
     * 
     * @param message The message to be sent to the server.
     */
    public void send(IClientMessage message) {
        try {
            String jsonMessage = Json.dumps(message);
            logger.info("Send: {}", jsonMessage);
            send(jsonMessage);
        } catch (JsonProcessingException e) {
            logger.error("Cannot serialize to json", e);
        }
    }

    /**
     * Wait for exception.
     */
    public void waitException(Duration duration) throws Exception {
        Instant start = Instant.now(); // Record the start time
        // Run the loop until the duration is elapsed
        while (Duration.between(start, Instant.now()).compareTo(duration) < 0) {
            if (this.exception != null) {
                throw this.exception;
            }
            try {
                Thread.sleep(50); // Sleep for 50 milliseconds
            } catch (InterruptedException e) {
            }
        }
        if (this.exception != null) {
            throw this.exception;
        }
    }
}
