package fr.utc.connect4.client.interfaces;

import java.util.List;
import java.util.UUID;

import fr.utc.connect4.client.data.ProfileSaveException;
import fr.utc.connect4.core.data.Game;
import fr.utc.connect4.core.data.LightGame;
import fr.utc.connect4.core.data.LightProfile;
import fr.utc.connect4.core.data.PrivateProfile;
import fr.utc.connect4.core.data.PublicProfile;

public interface IMainAsksDataClient {

    // Méthode pour obtenir une liste de jeux légers
    List<LightGame> getGames();

    // Méthode pour obtenir la liste des utilisateurs en ligne
    List<LightProfile> getUsersOnline();

    // Méthode pour obtenir une liste de profils légers
    List<LightProfile> getProfilesList();

    // Méthode pour définir les données utilisateur avec un profil privé
    void setUserData(PrivateProfile privateProfile) throws ProfileSaveException;

    // Méthode pour charger le profil utilisateur à partir d'un login et d'un hash
    PublicProfile loadUserDataProfile(String login, String hash);

    // Méthode pour obtenir le profil privé de l'utilisateur
    PrivateProfile getMyPrivateProfile();

    // Méthode pour obtenir le profil public de l'utilisateur
    PublicProfile getMyPublicProfile();

    // Méthode pour obtenir les données d'un jeu à partir de son UUID
    Game getGameData(UUID id);
}
