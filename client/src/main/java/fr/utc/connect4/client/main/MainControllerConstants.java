package fr.utc.connect4.client.main;

import javafx.scene.input.KeyCode;
import static javafx.scene.input.KeyCode.D;
import static javafx.scene.input.KeyCode.ESCAPE;
import static javafx.scene.input.KeyCode.Q;
import static javafx.scene.input.KeyCode.S;
import static javafx.scene.input.KeyCode.Z;

public interface MainControllerConstants {

        double FPS = 60;
        int HEIGHT = 896,
                        WIDTH = 1600;

        KeyCode UP = Z,
                        DOWN = S,
                        LEFT = Q,
                        RIGHT = D,
                        EXIT_GAME = ESCAPE;

}