package fr.utc.connect4.client.interfaces;

import java.util.UUID;

import fr.utc.connect4.core.data.LightGame;
import fr.utc.connect4.core.data.PublicProfile;

public interface IMainAsksComClient {

    void requestCreateGame(LightGame lightGame);

    void requestGames();

    void requestUsersOnline();

    void logout(PublicProfile profile);

    void connect(PublicProfile profile, String hostname, int port) throws Exception;

    /**
     * Sends a request to join a game with the specified UUID and player profile.
     *
     * @param uuidGame The UUID of the game to join.
     */
    void joinGame(UUID uuidGame);

    /**
     * Sends a request to get information about a player with the specified UUID.
     * 
     * @param uuid The UUID of the player to get information about.
     */
    void fetchPlayerProfile(UUID uuid);

}