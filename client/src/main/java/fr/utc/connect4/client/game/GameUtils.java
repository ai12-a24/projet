package fr.utc.connect4.client.game;

import javafx.scene.paint.Color;

/**
 * Utility class providing color conversion methods for the game.
 * Handles conversion between game-specific colors and JavaFX color representations.
 */
public class GameUtils {

    /**
     * Converts a game color to a JavaFX color.
     *
     * @param color the game color to convert
     * @return the corresponding JavaFX color
     */
    public static Color convertToJavaFXColor(fr.utc.connect4.core.data.Color color) {
        if (color == fr.utc.connect4.core.data.Color.RED) {
            return Color.RED;
        } else if (color == fr.utc.connect4.core.data.Color.YELLOW) {
            return Color.YELLOW;
        } else {
            return Color.WHITE;
        }
    }

}
