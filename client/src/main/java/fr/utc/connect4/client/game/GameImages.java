package fr.utc.connect4.client.game;

import fr.utc.connect4.core.ResourceImage;

/**
 * Contains constants for game-related image resources.
 * Provides centralized access to commonly used images in the game.
 */
public class GameImages {
     /** Image resource for error displays */
    public static final ResourceImage ERROR = new ResourceImage("/client/image/error.png"),
             /** Image resource for game icon */
            ICON = new ResourceImage("/client/image/icon.png");

}
