package fr.utc.connect4.client.data;

import fr.utc.connect4.client.interfaces.IGameAsksDataClient;
import fr.utc.connect4.core.data.Game;
import fr.utc.connect4.core.data.PublicProfile;

public class GameAsksDataClient implements IGameAsksDataClient {
    DataClientCore dataCore;

    public GameAsksDataClient(DataClientCore dataCore) {
        this.dataCore = dataCore;
    }

    @Override
    public void clearDataGame() {
        dataCore.clearDataGame();
    }

    @Override
    public PublicProfile getPublicProfile() {
        return this.dataCore.getMyPublicProfile();
    }

    @Override
    public Game getCurrentGame() {
        return this.dataCore.getGame();
    }

}
