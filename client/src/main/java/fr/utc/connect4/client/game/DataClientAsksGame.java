package fr.utc.connect4.client.game;

import java.util.List;

import fr.utc.connect4.client.interfaces.IDataClientAsksGame;
import fr.utc.connect4.core.data.Game;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

/**
 * Handles data communication between client and game components.
 * This class implements IDataClientAsksGame to manage game data operations from
 * the client side.
 */
public class DataClientAsksGame implements IDataClientAsksGame {
    private final GameCore gameCore;

    @FXML
    private Button btnQuitter; // Button to quit the game

    /**
     * Constructs a new DataClientAsksGame instance.
     * 
     * @param gameCore the game core instance to handle game logic
     */
    public DataClientAsksGame(GameCore gameCore) {
        this.gameCore = gameCore;
    }

    /**
     * Sends the move coordinates to the game.
     * 
     * @param column the column number where the move is made
     * @param row    the row number where the move is made
     * @return List containing the column and row coordinates
     */
    @Override
    public List<Integer> sendMove(int column, int row) {
        return List.of(column, row);
    }

    /**
     * Updates the game grid after each move, checking for wins or draws.
     */
    public void updateDisplayGame(Game game) {
        GameController gameController = gameCore.getGameController();
        gameController.updateDisplayGame(game);
    }

    @Override
    public void displayResult(Game game) {
        GameController gameController = gameCore.getGameController();
        gameController.displayResult(game.isFinished());
    }

    @Override
    public void startGame(Game game) {
        gameCore.getGameController().startGame(game);

    }

}
