package fr.utc.connect4.client.interfaces;

public interface IComClientCore {

    public void setComClientAsksMain(IComClientAsksMain comClientAsksMain);

    public IMainAsksComClient getMainAsksComClient();

    public void setComClientAsksGame(IComClientAsksGame comClientAsksGame);

    public IGameAsksComClient getGameAsksComClient();

    public void setComClientAsksDataClient(IComClientAsksDataClient comClientAsksDataClient);

    public IDataClientAsksComClient getDataClientAsksComClient();

}
