package fr.utc.connect4.client.game;

import fr.utc.connect4.client.interfaces.IComClientAsksGame;
import fr.utc.connect4.client.interfaces.IDataClientAsksGame;
import fr.utc.connect4.client.interfaces.IGameAsksComClient;
import fr.utc.connect4.client.interfaces.IGameAsksDataClient;
import fr.utc.connect4.client.interfaces.IGameAsksMain;
import fr.utc.connect4.client.interfaces.IGameCore;
import fr.utc.connect4.client.interfaces.IMainAsksGame;
import fr.utc.connect4.core.data.GameResult;

/**
 * The {@code GameCore} class manages the core game logic and facilitates
 * communication
 * between the main game interface, the data client, and the communication
 * client.
 * It also manages the lifecycle of game controllers associated with specific
 * game sessions.
 */
public class GameCore implements IGameCore {

    /** Interface for communication between main component and game */
    private final IMainAsksGame mainAsksGame;
    /** Interface for data operations between client and game */
    private final IDataClientAsksGame dataClientAsksGame;
    /** Interface for communication operations between client and game */
    private final IComClientAsksGame comClientAsksGame;
    /** Interface for game to main component communication */
    private IGameAsksMain gameAsksMain;
    /** Interface for game to communication client operations */
    private IGameAsksComClient gameAsksComClient;
    /** Interface for game to data client operations */
    private IGameAsksDataClient gameAsksDataClient;
    /** Controller managing game UI and interactions */
    private final GameController gameController;

    /**
     * Constructs a {@code GameCore} instance, initializing the necessary components
     * for game management including communication and data handling.
     */
    public GameCore() {
        this.mainAsksGame = new MainAsksGame(this);
        this.dataClientAsksGame = new DataClientAsksGame(this);
        this.comClientAsksGame = new ComClientAsksGame(this);
        this.gameController = new GameController(this);
    }

    /**
     * Sets the {@code IGameAsksMain} instance responsible for handling the main
     * game logic.
     *
     * @param gameAsksMain the {@code IGameAsksMain} instance
     */
    @Override
    public void setGameAsksMain(IGameAsksMain gameAsksMain) {
        this.gameAsksMain = gameAsksMain;
    }

    /**
     * Returns the {@code IMainAsksGame} instance responsible for interacting with
     * the main game interface.
     *
     * @return the {@code IMainAsksGame} instance
     */
    @Override
    public IMainAsksGame getMainAsksGame() {
        return this.mainAsksGame;
    }

    /**
     * Returns the {@code IMainAsksGame} instance responsible for interacting with
     * the main game interface.
     *
     * @return the {@code IMainAsksGame} instance
     */
    @Override
    public IGameAsksMain getGameAsksMain() {
        return this.gameAsksMain;
    }

    /**
     * Sets the {@code IGameAsksDataClient} instance responsible for handling
     * data-related game operations.
     *
     * @param gameAsksDataClient the {@code IGameAsksDataClient} instance
     */
    @Override
    public void setGameAsksDataClient(IGameAsksDataClient gameAsksDataClient) {
        this.gameAsksDataClient = gameAsksDataClient;
    }

    /**
     * Returns the {@code IDataClientAsksGame} instance responsible for handling
     * client-side data operations.
     *
     * @return the {@code IDataClientAsksGame} instance
     */
    @Override
    public IGameAsksDataClient getGameAsksDataClient() {
        return this.gameAsksDataClient;
    }

    /**
     * Returns the {@code IDataClientAsksGame} instance responsible for handling
     * client-side data operations.Gam
     *
     * @return the {@code IDataClientAsksGame} instance
     */
    @Override
    public IDataClientAsksGame getDataClientAsksGame() {
        return this.dataClientAsksGame;
    }

    /**
     * Sets the {@code IGameAsksComClient} instance responsible for handling
     * communication-related game operations.
     *
     * @param gameAsksComClient the {@code IGameAsksComClient} instance
     */
    @Override
    public void setGameAsksComClient(IGameAsksComClient gameAsksComClient) {
        this.gameAsksComClient = gameAsksComClient;
    }

    @Override
    public IGameAsksComClient getGameAsksComClient() {
        return this.gameAsksComClient;
    }

    /**
     * Returns the {@code IComClientAsksGame} instance responsible for handling game
     * communication operations.
     *
     * @return the {@code IComClientAsksGame} instance
     */
    @Override
    public IComClientAsksGame getComClientAsksGame() {
        return this.comClientAsksGame;
    }

    /**
     * Returns the {@code IComClientAsksGame} instance responsible for handling game
     * communication operations.
     *
     * @return the {@code IComClientAsksGame} instance
     */
    @Override
    public GameController getGameController() {
        return this.gameController;
    }

    public void gameResult(GameResult game) {
        gameController.displayResult(game);
    }

}
