package fr.utc.connect4.client.data;

import java.util.List;
import java.util.UUID;

import fr.utc.connect4.client.interfaces.IMainAsksDataClient;
import fr.utc.connect4.core.data.Game;
import fr.utc.connect4.core.data.GridFormat;
import fr.utc.connect4.core.data.LightGame;
import fr.utc.connect4.core.data.LightProfile;
import fr.utc.connect4.core.data.PrivateProfile;
import fr.utc.connect4.core.data.PublicProfile;

public class MainAsksDataClient implements IMainAsksDataClient {
    private final DataClientCore dataCore;

    public MainAsksDataClient(DataClientCore myDataCore) {
        this.dataCore = myDataCore;
    }

    @Override
    public List<LightGame> getGames() {
        return dataCore.getGames();
    }

    @Override
    public List<LightProfile> getUsersOnline() {
        return dataCore.getUsersOnline();
    }

    @Override
    public List<LightProfile> getProfilesList() {
        return dataCore.getProfilesList();
    }

    @Override
    public void setUserData(PrivateProfile privateProfile) throws ProfileSaveException {
        dataCore.setUserData(privateProfile);
    }   

    @Override
    public PublicProfile loadUserDataProfile(String login, String hash) {
        try {
            return dataCore.loadUserData(login, hash);
        } catch (ProfileLoadException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public PrivateProfile getMyPrivateProfile() {
        return dataCore.getMyPrivateProfile();
    }

    @Override
    public PublicProfile getMyPublicProfile() {
        return dataCore.getMyPublicProfile();
    }

    @Override
    public Game getGameData(UUID id) {
        //return new Game(null, null, id, GridFormat.FORMAT_6X5, null, null, true);
        return null;
    }

}
