package fr.utc.connect4.client.data;

import fr.utc.connect4.core.data.LightProfile;
import fr.utc.connect4.client.interfaces.IComClientAsksDataClient;
import fr.utc.connect4.client.interfaces.IDataClientAsksComClient;
import fr.utc.connect4.client.interfaces.IDataClientAsksGame;
import fr.utc.connect4.client.interfaces.IDataClientAsksMain;
import fr.utc.connect4.client.interfaces.IDataClientCore;
import fr.utc.connect4.client.interfaces.IGameAsksDataClient;
import fr.utc.connect4.client.interfaces.IMainAsksDataClient;
import fr.utc.connect4.core.data.Color;
import fr.utc.connect4.core.data.Game;
import fr.utc.connect4.core.data.GridFormat;
import fr.utc.connect4.core.data.LightGame;
import fr.utc.connect4.core.data.PrivateProfile;
import fr.utc.connect4.core.data.PublicProfile;
import fr.utc.connect4.core.data.Move;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DataClientCore implements IDataClientCore {
    private ArrayList<LightProfile> userList;
    private UserDataFileHandler userDataFileHandler;
    private Game currentGame;
    private ArrayList<LightGame> games;

    private final IMainAsksDataClient mainAsksDataClient;
    private final IGameAsksDataClient gameAsksDataClient;
    private final IComClientAsksDataClient comClientAsksDataClient;
    private IDataClientAsksMain dataClientAsksMain;
    private IDataClientAsksGame dataClientAsksGame;
    private IDataClientAsksComClient dataClientAsksComClient;

    public DataClientCore() {
        this.mainAsksDataClient = new MainAsksDataClient(this);
        this.gameAsksDataClient = new GameAsksDataClient(this);
        this.comClientAsksDataClient = new ComClientAsksDataClient(this);
        this.userDataFileHandler = new UserDataFileHandler();
        this.userList = new ArrayList<>();
        this.currentGame = null;
        this.games = new ArrayList<>();
    }

    public ArrayList<LightGame> getGames() {
        return games;
    }

    public void setGames(List<LightGame> games) {
        this.games = new ArrayList<>(games);
    }

    public ArrayList<LightProfile> getUsersOnline() {
        return userList;
    }

    // Don't know what this method is supposed to do differently from getUsersOnline
    public ArrayList<LightProfile> getProfilesList() {
        return userList;
    }

    public void addUser(LightProfile profile) {
        userList.add(profile);
    }

    public void removeUser(LightProfile profile) {

        userList.removeIf(p -> p.equals(profile));
    }

    public void addAllUsers(List<LightProfile> profiles) {
        userList.addAll(profiles);
    }

    public PrivateProfile getMyPrivateProfile() {
        return userDataFileHandler.getMyPrivateProfile();
    }

    public PublicProfile getMyPublicProfile() {
        return userDataFileHandler.getMyPublicProfile();
    }

    public void setUserData(final PrivateProfile privateProfile) throws ProfileSaveException {
        userDataFileHandler.setUserData(privateProfile);
    }

    public PrivateProfile loadUserData(String login, String hash) throws ProfileLoadException {
        return userDataFileHandler.loadUserDataProfile(login, hash);
    }

    public void clearDataGame() {
        this.currentGame = null;
    }

    public Game getGame() {
        return currentGame;
    }

    public void setGame(Game game) {
        this.currentGame = game;
        this.getDataClientAsksGame().updateDisplayGame(game);
    }

    public void addMove(Move move) {
        this.currentGame.addMove(move);
        // TODODATA : call function from Game to update display
    }

    @Override
    public void setDataClientAsksMain(IDataClientAsksMain dataClientAsksMain) {
        this.dataClientAsksMain = dataClientAsksMain;
    }

    public IDataClientAsksMain getDataClientAsksMain() {
        return this.dataClientAsksMain;
    }

    @Override
    public IMainAsksDataClient getMainAsksDataClient() {
        return this.mainAsksDataClient;
    }

    @Override
    public void setDataClientAsksGame(IDataClientAsksGame dataClientAsksGame) {
        this.dataClientAsksGame = dataClientAsksGame;
    }

    public IDataClientAsksGame getDataClientAsksGame() {
        return this.dataClientAsksGame;
    }

    @Override
    public IGameAsksDataClient getGameAsksDataClient() {
        return this.gameAsksDataClient;
    }

    @Override
    public void setDataClientAsksComClient(IDataClientAsksComClient dataClientAsksComClient) {
        this.dataClientAsksComClient = dataClientAsksComClient;
    }

    public IDataClientAsksComClient getDataClientAsksComClient() {
        return this.dataClientAsksComClient;
    }

    @Override
    public IComClientAsksDataClient getComClientAsksDataClient() {
        return this.comClientAsksDataClient;
    }

}
