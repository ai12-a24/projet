package fr.utc.connect4.client.main;

import java.io.IOException;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.utc.connect4.client.interfaces.IComClientAsksMain;
import fr.utc.connect4.client.interfaces.IDataClientAsksMain;
import fr.utc.connect4.client.interfaces.IGameAsksMain;
import fr.utc.connect4.client.interfaces.IMainAsksComClient;
import fr.utc.connect4.client.interfaces.IMainAsksDataClient;
import fr.utc.connect4.client.interfaces.IMainAsksGame;
import fr.utc.connect4.client.interfaces.IMainCore;
import fr.utc.connect4.core.ResourceUtils;
import fr.utc.connect4.core.data.LightGame;
import fr.utc.connect4.core.data.PublicProfile;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainCore implements IMainCore {

    private static final Logger logger = LogManager.getLogger(MainCore.class);

    private final IDataClientAsksMain dataClientAsksMain;
    private final IGameAsksMain gameAsksMain;
    private final IComClientAsksMain comClientAsksMain;
    private IMainAsksGame mainAsksGame;
    private IMainAsksDataClient mainAsksDataClient;
    private IMainAsksComClient mainAsksComClient;
    private MainController mainController;
    private Stage mainStage;
    private LoginViewController loginViewController;
    private CreateProfileCore createProfileCore;

    public MainCore() {
        this.dataClientAsksMain = new DataClientAsksMain(this);
        this.gameAsksMain = new GameAsksMain(this);
        this.comClientAsksMain = new ComClientAsksMain(this);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            this.mainStage = primaryStage;
            // Create profile window
            this.createProfileCore = new CreateProfileCore(this, this.mainAsksDataClient);
            // createProfileCore.displayUserProfileCreationWindow(primaryStage);
            loadLoginScene(mainStage);

        } catch (IOException e) {
            logger.error("An error occurred: ", e);
        }
    }

    /**
     * Display the login window
     * 
     * @param stage stage
     * @throws IOException in the case of file not readable
     */
    private void loadLoginScene(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/client/view/login_view.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        this.loginViewController = fxmlLoader.getController();
        this.loginViewController.setMainCore(this);
        stage.setTitle("Connect4 - Connexion");
        stage.getIcons().add(MainImages.ICON.get());
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Load and display the main window
     * 
     * @param primaryStage the main stage
     * @throws IOException in case of file not readable
     */
    public void loadMainScene(Stage primaryStage) throws IOException {
        URL fxml = ResourceUtils.getResourceURL("/client/view/MainView.fxml");
        FXMLLoader loader = new FXMLLoader(fxml);
        BorderPane root = loader.load();
        this.mainController = loader.getController();
        this.mainController.setMainCore(this);
        this.mainController.afterStart();

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Connect4");
        scene.getStylesheets().add(ResourceUtils.getResourceURL("/client/style/stylesheet.css").toExternalForm());
        ResourceUtils.loadCoreFont(24);
        primaryStage.getIcons().add(MainImages.ICON.get());

        primaryStage.setMaximized(true);

        primaryStage.centerOnScreen();
        primaryStage.toFront();
        primaryStage.setResizable(true);
        primaryStage.show();
        primaryStage.setOnCloseRequest(_ -> {
            close();
        });
    }

    public void connectToServer(String hostname, int port, PublicProfile profile)
            throws Exception {
        this.mainAsksComClient.connect(profile, hostname, port);
        this.loadMainScene(mainStage);
    }

    public void close() {
        logger.info("Closing client");
        PublicProfile oldPublic = this.getMainAsksDataClient().getMyPublicProfile();
        this.mainAsksComClient.logout(oldPublic);
        Platform.exit();
        logger.info("Closed !");
        System.exit(0);
    }

    @Override
    public void setMainAsksGame(IMainAsksGame mainAsksGame) {
        this.mainAsksGame = mainAsksGame;
    }

    @Override
    public IGameAsksMain getGameAsksMain() {
        return this.gameAsksMain;
    }

    @Override
    public void setMainAsksDataClient(IMainAsksDataClient mainAsksDataClient) {
        this.mainAsksDataClient = mainAsksDataClient;
    }

    @Override
    public IDataClientAsksMain getDataClientAsksMain() {
        return this.dataClientAsksMain;
    }

    @Override
    public void setMainAsksComClient(IMainAsksComClient mainAsksComClient) {
        this.mainAsksComClient = mainAsksComClient;
    }

    @Override
    public IComClientAsksMain getComClientAsksMain() {
        return this.comClientAsksMain;
    }

    public MainController getMainController() {
        return this.mainController;
    }

    public PublicProfile getPublicProfileMain() {
        return this.mainAsksDataClient.getMyPublicProfile();
    }

    public void requestCreateGame(LightGame game) {
        this.mainAsksComClient.requestCreateGame(game);
    }

    public IMainAsksGame getMainAsksGame() {
        return mainAsksGame;
    }

    public IMainAsksComClient getMainAsksComClient() {
        return mainAsksComClient;
    }

    public IMainAsksDataClient getMainAsksDataClient() {
        return mainAsksDataClient;
    }

    public Stage getMainStage() {
        return mainStage;
    }

    public CreateProfileCore getCreateProfileCore() {
        return createProfileCore;
    }
}
