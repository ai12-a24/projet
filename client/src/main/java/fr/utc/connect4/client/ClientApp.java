package fr.utc.connect4.client;

import fr.utc.connect4.client.com.ComClientCore;
import fr.utc.connect4.client.data.DataClientCore;
import fr.utc.connect4.client.game.GameCore;
import fr.utc.connect4.client.interfaces.IComClientCore;
import fr.utc.connect4.client.interfaces.IDataClientCore;
import fr.utc.connect4.client.interfaces.IGameCore;
import fr.utc.connect4.client.interfaces.IMainCore;
import fr.utc.connect4.client.main.MainCore;
import javafx.application.Application;
import javafx.stage.Stage;

public class ClientApp extends Application {

    public static void run() {
        launch();
    }

    @Override
    public void start(Stage primaryStage) {
        // Init modules
        IMainCore mainCore = new MainCore();
        IComClientCore comClientCore = new ComClientCore();
        IDataClientCore dataClientCore = new DataClientCore();
        IGameCore gameCore = new GameCore();

        // Main module interfaces
        mainCore.setMainAsksComClient(comClientCore.getMainAsksComClient());
        mainCore.setMainAsksDataClient(dataClientCore.getMainAsksDataClient());
        mainCore.setMainAsksGame(gameCore.getMainAsksGame());

        // Com Client module interfaces
        comClientCore.setComClientAsksDataClient(dataClientCore.getComClientAsksDataClient());
        comClientCore.setComClientAsksGame(gameCore.getComClientAsksGame());
        comClientCore.setComClientAsksMain(mainCore.getComClientAsksMain());

        // Data Client module interfaces
        dataClientCore.setDataClientAsksComClient(comClientCore.getDataClientAsksComClient());
        dataClientCore.setDataClientAsksGame(gameCore.getDataClientAsksGame());
        dataClientCore.setDataClientAsksMain(mainCore.getDataClientAsksMain());

        // Game module interfaces
        gameCore.setGameAsksComClient(comClientCore.getGameAsksComClient());
        gameCore.setGameAsksDataClient(dataClientCore.getGameAsksDataClient());
        gameCore.setGameAsksMain(mainCore.getGameAsksMain());

        // Main module handle all thread
        mainCore.start(primaryStage);
    }

}