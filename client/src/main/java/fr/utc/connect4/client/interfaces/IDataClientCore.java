package fr.utc.connect4.client.interfaces;

public interface IDataClientCore {

    public void setDataClientAsksMain(IDataClientAsksMain dataClientAsksMain);

    public IMainAsksDataClient getMainAsksDataClient();

    public void setDataClientAsksGame(IDataClientAsksGame dataClientAsksGame);

    public IGameAsksDataClient getGameAsksDataClient();

    public void setDataClientAsksComClient(IDataClientAsksComClient dataClientAsksComClient);

    public IComClientAsksDataClient getComClientAsksDataClient();

}
