package fr.utc.connect4.client.main;

import fr.utc.connect4.client.interfaces.IComClientAsksMain;
import fr.utc.connect4.core.data.PublicProfile;

public class ComClientAsksMain implements IComClientAsksMain {

    private final MainCore mainCore;

    public ComClientAsksMain(MainCore mainCore) {
        this.mainCore = mainCore;
    }

    @Override
    public void disconnected() {
        // TODO: implements this method
    }

    @Override
    public void showProfile(PublicProfile profile) {
        mainCore.getMainController().updateDistantProfile(profile);
    }

}
