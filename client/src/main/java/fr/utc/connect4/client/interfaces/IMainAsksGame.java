package fr.utc.connect4.client.interfaces;

import fr.utc.connect4.core.data.Color;
import fr.utc.connect4.core.data.Game;
import fr.utc.connect4.core.data.LightGame;
import fr.utc.connect4.core.data.LightProfile;
import javafx.stage.Stage;

public interface IMainAsksGame {
    /**
     * Launches and joins a game session
     */

    void displayGameForCreator(Stage primaryStage, LightGame lightGame);

    void displayGameForPlayer2(Stage primaryStage, LightGame lightGame);

    void joinGameAsPlayer(Game game);

    void joinGameAsSpectator(Game game);

}
