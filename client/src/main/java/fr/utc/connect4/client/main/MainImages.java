package fr.utc.connect4.client.main;

import fr.utc.connect4.core.ResourceImage;

public class MainImages {

    public static final ResourceImage ERROR = new ResourceImage("/client/image/error.png"),
            ICON = new ResourceImage("/client/image/icon.png");

}
