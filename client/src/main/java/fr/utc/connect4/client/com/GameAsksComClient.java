package fr.utc.connect4.client.com;

import java.util.UUID;

import fr.utc.connect4.client.interfaces.IGameAsksComClient;
import fr.utc.connect4.core.com.RequestNewMove;
import fr.utc.connect4.core.data.Move;

/**
 * This class provide interface between GameCore and ComClientCore.
 * 
 * @see IGameAsksComClient
 * @see ComClientCore
 */
public class GameAsksComClient implements IGameAsksComClient {

    /**
     * The {@code ComClientCore} instance used by this client to handle the core
     * communication functionality.
     */
    @SuppressWarnings("unused")
    private final ComClientCore comClientCore;

    /**
     * Constructs a {@code GameAsksComClient} object with the specified
     * {@code ComClientCore} instance. The constructor is used to initialize the
     * internal communication core that will be used for game-related operations.
     *
     * @param comClientCore The {@code ComClientCore} instance to be used by this
     *                      client.
     */
    public GameAsksComClient(ComClientCore comClientCore) {
        this.comClientCore = comClientCore;
    }

    /**
     * Sends a game move (coup) to the server. Currently
     *
     * @param coup The game move to be sent to the server as a string.
     */
    @Override
    public void sendMove(Move move, UUID gameID) {
        this.comClientCore.send(new RequestNewMove(move, gameID));
        // throw new UnsupportedOperationException("Unimplemented method 'sendAction'");
    }

}
