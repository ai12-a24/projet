package fr.utc.connect4.client.interfaces;

import java.util.List;

import fr.utc.connect4.core.data.LightGame;
import fr.utc.connect4.core.data.Game;
import fr.utc.connect4.core.data.LightProfile;
import fr.utc.connect4.core.data.Move;

public interface IComClientAsksDataClient {

    // Méthode pour ajouter un utilisateur à la liste
    void addUserToList(LightProfile user);

    // Méthode pour supprimer un utilisateur de la liste
    void removeUserToList(LightProfile user);

    // Méthode pour ajouter plusieurs utilisateurs à la liste
    void addAllUsersToList(List<LightProfile> users);

    // Méthode pour mettre à jour la liste des jeux
    void updateGamesList(List<LightGame> games);

    /**
     * Handle a new created game.
     * 
     * @param game Game to handle.
     */
    void addNewGame(LightGame game);

    // Méthode pour ajouter un mouvement joué
    void addPlayedMoveToCurrentGame(Move move);

    // methode pour annoncer un deuxieme joueur pour la liste des jeux
    void updateGameAddPlayer2(LightGame game);

    // methode pour annoncer aux joueurs de la partie le debut de la partie
    void startGame(Game game);

    void updateMove(Game game);
}
