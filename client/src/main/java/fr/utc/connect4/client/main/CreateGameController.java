package fr.utc.connect4.client.main;

import fr.utc.connect4.core.data.Color;
import fr.utc.connect4.core.data.GridFormat;
import fr.utc.connect4.core.data.LightGame;
import fr.utc.connect4.core.data.LightProfile;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

import java.util.UUID;
import java.util.UUID;

public class CreateGameController {

    private MainCore mainCore;

    @FXML
    private RadioButton radioYes;

    @FXML
    private RadioButton radioNo;

    @FXML
    private ComboBox<String> gridSizeComboBox;

    @FXML
    private ComboBox<String> colorComboBox;

    public void setMainCore(MainCore mainCore) {
        this.mainCore = mainCore;
    }

    @FXML
    public void initialize() {
        // Using Toggle group so we can't check both yes and no
        ToggleGroup chatToggleGroup = new ToggleGroup();
        radioYes.setToggleGroup(chatToggleGroup);
        radioNo.setToggleGroup(chatToggleGroup);
    }

    /**
     * Function to create a game when the user clicks the "Create Game" button on the form.
     */
    @FXML
    private void createGameClick(ActionEvent event) {
        // Retrieve the selected grid size
        String selectedGridSize = gridSizeComboBox.getValue();

        // Find the corresponding enum for the grid size
        GridFormat gridFormat = null;
        for (GridFormat format : GridFormat.values()) {
            if (format.getResult().equals(selectedGridSize)) {
                gridFormat = format;
                break;
            }
        }

        // Retrieve the selected color
        String selectedColor = colorComboBox.getValue();
        Color color = switch (selectedColor) {
            case "Jaune" -> Color.YELLOW;
            case "Rouge" -> Color.RED;
            default -> Color.EMPTY;
        };

        // Check if chat is enabled or not
        boolean isChatEnabled = radioYes.isSelected();

        // Create the LightGame object
        LightProfile gameCreator = this.mainCore.getPublicProfileMain().getLightProfile();
        LightGame lightGame = new LightGame(gameCreator, null, null, gridFormat, color, isChatEnabled);

        // Call from MainAsksComClient the function requestCreateGame
        this.mainCore.requestCreateGame(lightGame);

        // Close the "Create Game" window
        Stage stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
        stage.close();
    }
}
